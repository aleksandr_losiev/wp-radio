/**
 * Audio class
 */

class AudioData {

    constructor() {
        // this.setListeners();
        this.player = new Player();
        // this.stationID = null;
        // this.isActive = false;

        // this.DOMLoad();

    }

    // init() {
    //     let songsStationPage = document.getElementsByTagName('figure');
    //
    //     if (songsStationPage.length) {
    //         this.stationPage(songsStationPage);
    //     }
    // }

    /**
     * Station page
     *
     * @param songs
     */
    // stationPage(songs) {
    //     Array.from(songs).forEach(song => {
    //         let audio = song.querySelector('audio');
    //         let that = this;
    //
    //         audio.onloadedmetadata = function () {
    //             song.querySelector('.audio-time').innerHTML = that.getTime(this.duration);
    //         };
    //
    //     });
    // }
    //
    // setListeners() {
    //     // All stations on archive page
    //     let stations = document.querySelectorAll('article.single-station .station-data-container');
    //     let that = this;
    //
    //     // window.addEventListener('popstate', function(e){
    //     //     if (that.isActive) {
    //     //         let path = window.location.pathname.split('/');
    //     //
    //     //         if (path.includes('stations')) {
    //     //             that.deleteStationPopup();
    //     //         }
    //     //     }
    //     //
    //     // });
    //
    //     if (stations.length) {
    //         Array.from(stations).forEach(function (element) {
    //             element.addEventListener('click', function () {
    //                 that.openStationPopup(this.getAttribute('data-station_id'));
    //                 // that.setStationUrl(this);
    //             }, false);
    //         });
    //     }
    //
    //     this.rangeListeners();
    // }

    /**
     * Generate Popup template for station
     * and this station popup append main container (#popup)
     *
     * @param stationID
     */
    openStationPopup(stationID) {

        ajax.getStationPopupTemplate(stationID).then(response => {
            let container = document.getElementById('page');

            container.insertAdjacentHTML('beforeEnd', response);
            helper.setPreloaderStatus(true);
            this.rangeListeners();
            this.stationPopupButtons();
            this.player.initPlayerRadio();
            this.player.startPlayRadio();
            // this.isActive = true;

            // this.buttons.listener();

            // this.startPlay();
            // console.log(this.getStationData());

            setTimeout(() => {
                this.setStatusStationPopup(true);
            }, 200);
        })
    }

    deleteStationPopup() {
        helper.setPreloaderStatus(true);
        this.player.stopTrack();
        setTimeout(() => {
            let section = document.querySelector('section.station-popup');
            section.parentNode.removeChild(section);
            // this.isActive = false;

            setTimeout(() => {
                helper.setPreloaderStatus(false);
                helper.noScrollBody(false);
            }, 500);
        }, 500);
    }

    stationPopupButtons() {
        let section = document.querySelector('section.station-popup');

        let buttons = {
            close: section.querySelector('button.close-station-popup'),
        };

        buttons.close.addEventListener('click', () => {
            this.deleteStationPopup();
        });
    }

    // setStationUrl(element) {
    //     router.setNewUrl(
    //         {stationID: element.getAttribute('data-station-id')},
    //         element.querySelector('h5').innerHTML,
    //         element.getAttribute('data-link')
    //     );
    // }

    /**
     * Listener input type range and changed style
     */
    rangeListeners() {
        let audioRange = document.querySelectorAll('input[type=range].audio-duration');

        Array.from(audioRange).forEach(function (element) {
            element.addEventListener('input', function () {
                this.setAttribute('value', this.value);
            }, false);
        });
    }

    /**
     * Changing popup status
     *
     * @param status
     */
    // setStatusStationPopup(status) {
    //     console.log('station', status);
    //     let stationPopup = document.querySelector('section.station-popup');
    //
    //     status ? stationPopup.classList.remove('hidden-data') : stationPopup.classList.add('hidden-data');
    // }

    /**
     * Seconds to minutes width seconds
     *
     * @param value
     * @return {string}
     */
    // getTime(value) {
    //
    //     let time = Math.floor(value / 60);
    //
    //     let min = time >= 10 ? time : '0' + time;
    //     let sec = Math.floor(value - time * 60);
    //
    //     return `${min}:${sec}`;
    // }
}

class Player {
    constructor() {

    }

    /**
     * Default value and methods for radio player and music player
     */
    init() {
        this.offsetTime = 0;
        this.activeTrack = null;
        this.newTrackTimeout = null;
        // this.tooltip = new Tooltip();

        this.buttonVolume();
        this.buttonVolumeRange();
        this.buttonClosePopup();
        // this.buttonCompress();
        // this.buttonExpand();
    }

    /**
     * Load methods for music player
     */
    initAudioPlayer() {
        this.activeTrackData = null;
        this.isRepeat = false;
        this.isRandom = false;
        this.audioPlayer = document.getElementById('audio-player');
        this.volume = this.audioPlayer.querySelector('.player-volume-range').value;
        this.tracksData = this.getPlayerTracksData(document.querySelector('.station-songs'));

        this.buttonPlayAudio();
        this.buttonPauseAudio();
        this.buttonRepeat();
        this.buttonRandom();
        this.buttonNextTrack();
        this.buttonPrevTrack();
        this.rangeAudioVolume();
        this.playTrackListener();
        this.buttonVolumePlayer();
        this.rangeAudioTime();
        this.trackStatus();
        this.playTracksListener();
        // this.tracksData.then(result => {
        //     console.log(result);
        // });
    }

    /**
     * Load methods for radio player
     */
    initPlayerRadio() {
        this.stationPopup = document.querySelector('section.station-popup');
        this.player = document.getElementById('player');
        this.updateTime = this.player.querySelector('.audio-duration').getAttribute('data-time-update');
        this.tracksData = this.getPlayerTracksData(this.player);
        this.volumeRangeData = this.player.querySelector('.player-volume-range').value;

        this.init();
        this.buttonStopRadio();
        this.buttonPlayRadio();
        this.buttonNextTrack(true);
    }

    buttonPause() {
        let player = document.getElementById('player');
        let buttons = player.querySelectorAll('button.player-pause');

        buttons.forEach(button => {
           console.log(button);
        });
    }

    buttonStop() {

    }

    buttonCompress() {
        let button = this.player.querySelector('button.compress-station-popup');

        if (button) {
            button.addEventListener('click', () => {
                this.stationPopup.classList.add('compress');
                this.player.querySelector('.buttons-helper').setAttribute('data-compress', 'expand');
                // helper.noScrollBody(false);
            });
        }
    }

    buttonExpand() {
        let button = this.player.querySelector('button.expand-station-popup');

        if (button) {
            button.addEventListener('click', () => {
                this.stationPopup.classList.remove('compress');
                this.player.querySelector('.buttons-helper').setAttribute('data-compress', 'compress');
            });
        }
    }

    buttonClosePopup() {
        let button = this.player.querySelector('button.close-station-popup');

        button.addEventListener('click', () => {
            // this.hidePlayer();
            window.history.back();
        });
    }

    /**
     * Button in player for display volume range
     */
    buttonVolume() {
        let button = this.player.querySelector('button.player-volume');

        button.addEventListener('click', function () {
            this.classList.toggle('open');
        })
    }

    /**
     * Button in Audio player for display volume range
     */
    buttonVolumePlayer() {
        let button = this.audioPlayer.querySelector('button.player-volume');

        button.addEventListener('click', function () {
            this.classList.toggle('open');
        })
    }

    /**
     * Button in player for changed volume
     */
    buttonVolumeRange() {
        let that = this;
        let volume = this.player.querySelector('.player-volume-range');

        volume.addEventListener('input', function () {
            this.setAttribute('value', this.value);
            that.volumeRangeData = this.value;
            that.setVolumeTrack(this.value);
        }, false);
    }

    /**
     * Button in player for stop track (only radio)
     */
    buttonStopRadio() {
        let button = this.player.querySelector('button.player-stop-radio');

        button.addEventListener('click', () => {
            this.stopTrack();
        })
    }

    /**
     * Button in player for start track (only radio)
     */
    buttonPlayRadio() {
        let button = this.player.querySelector('button.player-play-radio');

        button.addEventListener('click', () => {
            this.player.querySelector('.buttons-main').setAttribute('data-status', 'play');
            this.startPlayRadio();
        });
    }

    /**
     * Button Listener to play (only audio player)
     *
     * Changing button to pause
     * Start play current audio track
     */
    buttonPlayAudio() {
        let button = document.getElementById('play-track');

        button.addEventListener('click', ()=>{
            this.audioPlayer.querySelector('.buttons-main').setAttribute('data-status-hidden', 'play');
            this.startPlayAudio();
        })
    }

    /**
     * Button Listener to pause (only audio player)
     *
     * Changing button to play
     * Pause current track
     * Clear timeout to next track
     */
    buttonPauseAudio() {
        let button = document.getElementById('pause-track');

        button.addEventListener('click', ()=>{
            this.audioPlayer.querySelector('.buttons-main').setAttribute('data-status-hidden', 'pause');
            this.activeTrack.pause();

            if (this.newTrackTimeout) {
                clearTimeout(this.newTrackTimeout);
            }
        })
    }

    /**
     * Button Listener Repeat track
     *
     * Active or disable button
     * Clear timeout and generated new with current track
     */
    buttonRepeat() {
        let button = document.getElementById('repeat-track'),
            buttonNext = document.getElementById('next-track'),
            buttonPrev = document.getElementById('prev-track'),
            buttonRandom = document.getElementById('random-track');

        button.addEventListener('click', () => {
            this.isRepeat = !this.isRepeat;
            button.classList.toggle('active');

            let timer = Math.ceil(this.activeTrackData.time) - Math.floor(this.activeTrack.currentTime);
            this.nextAudioTrackTimer(timer);

            if (this.isRepeat) {
                buttonNext.classList.add('disabled');
                buttonPrev.classList.add('disabled');
                buttonRandom.classList.add('disabled');
            } else {
                buttonNext.classList.remove('disabled');
                buttonPrev.classList.remove('disabled');
                buttonRandom.classList.remove('disabled');
            }
        });
    }

    /**
     * Button Listener Random track
     *
     * Active or disable random button
     * Clear timeout and generated new random track
     */
    buttonRandom() {
        let button = document.getElementById('random-track');

        button.addEventListener('click', () => {
            if (!this.isRepeat) {
                this.isRandom = !this.isRandom;
                button.classList.toggle('active');

                let timer = Math.ceil(this.activeTrackData.time) - Math.floor(this.activeTrack.currentTime);
                this.nextAudioTrackTimer(timer);
            }
        })
    }

    /**
     * Button Listener Next track
     *
     * If active repeat then this button not active
     * Change to next track, or change to first track in list
     * Clear timeout and start new track
     */
    buttonNextTrack(isRadio = false) {
        let button = document.getElementById('next-track');

        button.addEventListener('click', () => {
            if (isRadio) {
                this.offsetTime +=  Math.ceil(this.activeTrack.duration - this.activeTrack.currentTime);
                clearTimeout(this.newTrackTimeout);
                this.startPlayRadio();
            } else {
                if (!this.isRepeat) {
                    this.tracksData.then(result => {
                        let tracks = result.sort(function (a, b) {
                            return a.index - b.index;
                        });

                        let index = this.activeTrackData.index === tracks.length - 1 ? 0 : this.activeTrackData.index + 1;

                        this.activeTrack.pause();
                        this.activeTrack = new Audio(tracks[index].songURL);
                        this.activeTrackData = tracks[index];
                        clearTimeout(this.newTrackTimeout);
                        this.startPlayAudio();
                        // start(this.activeTrack);
                    });
                }
            }
        });
    }

    /**
     * Button Listener Previous track
     *
     * If active repeat then this button not active
     * Change to previous track, or change to last track in list
     * Clear timeout and start new track
     */
    buttonPrevTrack() {
        let button = document.getElementById('prev-track');

        button.addEventListener('click', () => {
            if (!this.isRepeat) {
                this.tracksData.then(result => {
                    let tracks = result.sort(function (a, b) {
                        return a.index - b.index;
                    });

                    let index = this.activeTrackData.index === 0 ? tracks.length - 1 : this.activeTrackData.index - 1;

                    this.activeTrack.pause();
                    this.activeTrack = new Audio(tracks[index].songURL);
                    this.activeTrackData = tracks[index];
                    clearTimeout(this.newTrackTimeout);
                    this.startPlayAudio();

                });
            }
        });
    }

    /**
     * Main button to play all tracks in audio player
     */
    playTracksListener() {
        let button = document.querySelector('#single-playlist .description button._button__play');

        if (button) {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                this.startPlayAudio();
            })
        }
    }

    /**
     * Change volume in active track
     *
     * Listen change value and change track on this value
     */
    rangeAudioVolume() {
        let that = this;
        let button = this.audioPlayer.querySelector('.player-volume-range');

        button.addEventListener('input', function () {
            this.setAttribute('value', this.value);
            that.volume = this.value;
            that.setVolumeTrack(this.value);
        }, false);
    }

    /**
     * Listener & changing track time
     */
    rangeAudioTime() {
        let that = this;
        let button = this.audioPlayer.querySelector('.audio-duration');

        button.addEventListener('input', function () {
            let time = that.activeTrackData.time / 300 * this.value;
            let timer = Math.ceil(that.activeTrackData.time - time);

            this.setAttribute('value', this.value);
            that.activeTrack.currentTime = time;

            that.nextAudioTrackTimer(timer);
        }, false);
    }

    /**
     * Change position audio slider & view position current time
     */
    trackStatus() {
        setInterval(() => {
            if (this.activeTrack) {
                let value = 300/this.activeTrackData.time * this.activeTrack.currentTime;
                document.querySelector('input.audio-duration').value = value;
                document.querySelector('input.audio-duration').setAttribute('value', Math.ceil(value));
            }
        }, 1000)
    }

    /**
     * Start play tracks in playlist
     *
     * Check if has active track, if not has active then play first from list
     * Open audio player
     * Add in button ".add-to-playlist" id track
     * Add track description and thumbnail in player
     * Add timer to play next track
     */
    startPlayAudio() {
        let that = this;
        if (!this.activeTrack) {

            this.tracksData.then(result => {
                let tracks = result.sort(function (a, b) {
                    return a.index - b.index;
                });

                this.activeTrack = new Audio(tracks[0].songURL);
                this.activeTrackData = tracks[0];

                start(this.activeTrack);
            });

        } else {
            this.activeTrack.pause();
            start(this.activeTrack);
        }

        function start(activeTrack, startTime = 0) {
            activeTrack.play();
            that.audioPlayer.querySelector('.buttons-main').setAttribute('data-status-hidden', 'play');
            that.audioPlayer.classList.remove('hidden');
            that.audioPlayer.querySelector('.helper-buttons .add-to-playlist').setAttribute('data-song-id', that.activeTrackData.trackID);
            that.setVolumeTrack(that.volume);
            that.setTrackDescription(that.audioPlayer, that.activeTrackData.trackName, that.activeTrackData.artist, null, that.activeTrackData.thumbnail)

            let timer = Math.ceil(that.activeTrackData.time) - Math.floor(activeTrack.currentTime);
            that.nextAudioTrackTimer(timer);
        }

    }

    /**
     *  Play track in playlist
     *
     *  Listen when click on button ".play-track"
     *  Then give this index and changing active track on this track
     */
    playTrackListener() {
        let that = this;
        let buttons = document.querySelectorAll('figure button.play-track');

        buttons.forEach(button => {
            button.addEventListener('click', function(e) {
                e.preventDefault();

                let index = this.getAttribute('data-index');

                that.tracksData.then(result => {
                    let tracks = result.sort(function (a, b) {
                        return a.index - b.index;
                    });

                    if (that.activeTrack) {
                        that.activeTrack.pause();
                        clearTimeout(that.newTrackTimeout);
                    }

                    that.activeTrack = new Audio(tracks[index].songURL);
                    that.activeTrackData = tracks[index];

                    that.startPlayAudio();
                });
            })
        });
    }

    /**
     * Start next track after time
     *
     * Check audio player settings and start track
     * after 'time'. Time sending in seconds.
     *
     * @param timer
     */
    nextAudioTrackTimer(timer) {
        if (this.newTrackTimeout) {
            clearTimeout(this.newTrackTimeout);
        }

        this.newTrackTimeout = setTimeout(() => {
            let type = this.isRandom ? 'random' : 'rotation';
                type = this.isRepeat ? 'repeat' : type;
            this.nextAudioTrack(type);
        }, timer * 1000);
    }

    /**
     * Logistic to next track
     *
     * Next track must be: rotation, random & repeat
     * Sorting tracks and return next track
     *
     * @param type
     */
    nextAudioTrack(type = 'rotation') {
        let that = this;

        this.tracksData.then(result => {
            let tracks = result.sort(function (a, b) {
                return a.index - b.index;
            });

            switch (type) {
                case 'rotation':
                    let index = this.activeTrackData.index === tracks.length - 1 ? 0 : this.activeTrackData.index + 1;

                    this.activeTrack = new Audio(tracks[index].songURL);
                    this.activeTrackData = tracks[index];
                    break;
                case 'random':
                    let random = random(tracks);

                    this.activeTrack = new Audio(tracks[random].songURL);
                    this.activeTrackData = tracks[random];
                    break;
                case 'repeat':
                    // this.activeTrack = new Audio(tracks[this.activeTrackData.index].songURL);
                    // this.activeTrackData = tracks[this.activeTrackData.index];
                    break;
            }

            this.startPlayAudio();

        });

        /**
         * Return random track index
         *
         * @param tracks
         * @return {number}
         */
        function random(tracks) {
            let index = Math.floor(Math.random() * (0 - tracks.length));

            if ((that.activeTrackData.index === index || that.activeTrackData.index === index + 1) && tracks.length > 3) {
                random(tracks);
            }

            return index;
        }
    }

    /**
     * Start play track (using only for radio)
     */
    startPlayRadio() {
        let that = this;
        this.tracksData.then(result => {
            let track = this.getRadioTrack(result, this.updateTime, this.getCurrentTime());
            this.setTrackDescription(this.player, track.song.trackName, track.song.artist, track.song.trackID);
            this.openLastPlayedSongs(track.song.trackID);

            if (this.activeTrack) {
                this.activeTrack.pause();
            }

            this.activeTrack = new Audio(track.song.songURL);
            this.startNewTrack(track.end);
            ajax.incrementViewToType(track.song.trackID, 'post');

            this.activeTrack.onloadedmetadata = function () {
                helper.setPreloaderStatus(false);
                this.currentTime = track.start;
                that.setVolumeTrack(that.volumeRangeData);
                this.play();
            };
        })
    }

    /**
     * Get active track by current station
     * and set description track in panel
     *
     */
    getActiveStationTrack() {
        this.tracksData.then(response => {
            let track = this.getRadioTrack(response, this.updateTime, this.getCurrentTime());
            this.setTrackDescription(this.player, track.song.trackName, track.song.artist, track.song.trackID);

            this.openLastPlayedSongs(track.song.trackID);
        })
    }

    /**
     * Stop active track and clear counter for next track
     */
    stopTrack() {
        this.player.querySelector('.buttons-main').setAttribute('data-status', 'stop');
        this.activeTrack.pause();

        if (this.newTrackTimeout) {
            clearTimeout(this.newTrackTimeout);
        }
    }

    /**
     * Start new track after timer
     *
     * @param timer
     */
    startNewTrack(timer) {
        if (this.newTrackTimeout) {
            clearTimeout(this.newTrackTimeout);
        }

        this.newTrackTimeout = setTimeout(() => {
            this.startPlayRadio();
        }, timer * 1000);

    }

    /**
     * Include in player song description
     *
     * @param element
     * @param track
     * @param artist
     * @param track_id
     * @param thumbnail
     */
    setTrackDescription(element, track, artist, track_id = 0, thumbnail = null) {
        element.querySelector('.song-data h5').innerHTML = artist;
        element.querySelector('.song-data h6').innerHTML = track;

        track_id && element.querySelector('.description').setAttribute('data-active-track-id', track_id);
        thumbnail && element.querySelector('.thumbnail').setAttribute('src', thumbnail);
    }

    /**
     * Change volume for active track
     */
    setVolumeTrack(volume) {
        this.activeTrack.volume = volume / 100;
    }

    /**
     * Get current time in seconds (UNIX)
     *
     * @return {number}
     */
    getCurrentTime() {
        return Math.ceil(Date.now() / 1000) + this.offsetTime;
    }

    /**
     * Open last 5 played tracks
     *
     * @param count {number}
     */
    openLastPlayedSongs(currentTrackID, count = 5) {
        let list = document.querySelectorAll('figure'),
            elem = null;

        list.forEach(track => {
           track.classList.remove('active');

           if (track.getAttribute('data-track-id') === currentTrackID) {
               elem = track;
           }
        });

        if (elem) {
            let _elem = elem.previousElementSibling;

            for (let i = 0; i < count; i++) {
                if (_elem === null) {
                    _elem = list[list.length -1]
                }
                console.log(_elem);
                _elem.classList.add('active');
                _elem = _elem.previousElementSibling;
            }
        }
    }

    /**
     * Get tracks data on player page
     */
    getPlayerTracksData(element) {

        return new Promise(resolve => {
            let songs = element.querySelectorAll('.audio-block .audio-track'),
                songsData = [],
                promises = [];

            Array.from(songs).forEach((song, index) => {
                let songURL = song.getAttribute('src');
                let songTime = song.getAttribute('data-track-duration');

                let data = {
                    index: index,
                    songURL: songURL,
                    trackID: song.getAttribute('data-track-id'),
                    trackName: song.getAttribute('data-track-name'),
                    thumbnail: song.getAttribute('data-track-thumbnail'),
                    artist: song.getAttribute('data-artist'),
                };

                if (songTime !== undefined && songTime) {
                    data.time = songTime;
                    songsData.push(data);
                } else {
                    let audio = new Audio(songURL);
                    promises.push(
                        new Promise(resolveSong => {
                            audio.onloadedmetadata = function () {
                                resolveSong(this.duration);
                            };
                        }).then(result => {
                            data.time = result;
                            songsData.push(data);
                        })
                    );
                }

            });

            Promise.all(promises).then(() => {
                resolve(songsData);
            })

        });
    }

    /**
     * Return current track and rest time by start from @param songs
     *
     * @param songs {array}
     * @param start
     * @param end
     *
     * @return {object}
     */
    getRadioTrack(songs, start, end) {
        let roundTime = 0;
        let timeCounter = 0;
        let currentSong = false;
        let timeDeviation = 1; // In seconds

        songs.forEach((song , i)=> {
            roundTime += Number(song.time);
        });

        let restOfTime = (end - start) - (Math.floor((end - start) / Math.ceil(roundTime)) * Math.ceil(roundTime));

        let songsSorted = songs.sort(function (a, b) {
            return a.index - b.index;
        });

        songsSorted.forEach(song => {
            let songTime = Number(song.time);

            if (!currentSong) {
                if (songTime + timeCounter < restOfTime) {
                    timeCounter += songTime;
                } else {
                    currentSong = {
                        song: song,
                        start: Math.ceil(restOfTime - timeCounter),
                        end: Math.floor(songTime - (restOfTime - timeCounter)) + timeDeviation
                    }
                }
            }
        });

        return currentSong;
    }

    /**
     * Open player
     */
    openPlayer() {
        helper.setPreloaderStatus(true);
        helper.noScrollBody(true);
        setTimeout(() => {
            this.stationPopup.classList.remove('hidden-data');
            setTimeout(() => {
                helper.setPreloaderStatus(false);
            }, 700);
        }, 500);
    }

    /**
     * Hide player and stopped music
     */
    hidePlayer() {
        helper.setPreloaderStatus(true);
        if (this.activeTrack) {
            this.stopTrack();
        }
        setTimeout(() => {
            this.stationPopup.classList.add('hidden-data');
            setTimeout(() => {
                helper.setPreloaderStatus(false);
                helper.noScrollBody(false);
            }, 700);
        }, 500);
    }

    /**
     * Seconds to minutes width seconds
     *
     * @param value
     * @return {string}
     */
    getTime(value) {

        let time = Math.floor(value / 60);

        let min = time >= 10 ? time : '0' + time;
        let sec = Math.floor(value - time * 60);

        sec = sec >=10 ? sec : '0' + sec;

        return `${min}:${sec}`;
    }
}

class StationPlayer {
    constructor() {
        this.player = new Player();
        this.DOMLoad();
    }

    /**
     * Work after Document ready
     *
     * @constructor
     */
    DOMLoad() {
        document.addEventListener('DOMContentLoaded', () => {
            helper.noScrollBody(true);
            this.player.initPlayerRadio();
            this.player.getActiveStationTrack();
        })
    }
}

class AudioPlayer {
    constructor() {
        this.player = new Player();
        this.DOMLoad();
    }

    /**
     * Work after Document ready
     *
     * @constructor
     */
    DOMLoad() {
        document.addEventListener('DOMContentLoaded', () => {
            this.player.initAudioPlayer();
            this.player.tracksData.then( result => {
                result.forEach( song => {
                    document.querySelector(`figure[data-track-id="${song.trackID}"] .audio-time`).innerHTML = this.player.getTime(song.time);
                })
            });
        });
    }
}

class Station {
    constructor() {
        this.stationPlayer = new StationPlayer();
        this.station = document.getElementById('station');
        this.songsStationPage = this.station ? this.station.getElementsByTagName('figure') : [];

        this.DOMLoad();
    }

    /**
     * Work after Document ready
     *
     * @constructor
     */
    DOMLoad() {
        if (this.songsStationPage.length) {
            this.setTimeToTracks(this.songsStationPage);
        }

        if (this.station) {

        //     this.station.querySelector('.station-data button._button__play').addEventListener('click', () => {
        //         this.stationPlayer.player.openPlayer();
        //     })
        }
    }

    /**
     * Add Time to tracks
     *
     * @param songs
     */
    setTimeToTracks(songs) {
        Array.from(songs).forEach(song => {
            let audio = song.querySelector('.audio-data');
            let that = this;

            if (!audio.getAttribute('data-time')) {
                let _audio = new Audio(audio.getAttribute(src));
                _audio.onloadedmetadata = function () {
                    song.querySelector('.audio-time').innerHTML = that.stationPlayer.player.getTime(this.duration);
                };

                helper.updateSongsTime([audio])
            }
        });
    }
}

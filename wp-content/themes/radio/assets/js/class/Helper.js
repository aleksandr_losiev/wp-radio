/**
 * Helper class for js
 *
 * In this class used helpers methods
 */
class Helper {

    DOMLoad() {
        document.addEventListener("DOMContentLoaded", () => {
            this.updateSongsTime();
            this.tracksDuration();
        });
    }
    /**
     * Validate Field
     *
     * Using Validate.js
     * Library: https://validatejs.org
     *
     * @param data (object) / Example: {password: '123456'}
     * @param fields (array) / Example: ['email', 'password']
     * @return {*}
     */
    validateField(data, fields = ['email', 'password', 'confirmPassword']) {
        let constraints = {
            email: {
                presence: true,
                email: true
            },
            password: {
                presence: true,
                format: {
                    pattern: "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$",
                    flags: "i",
                    message: "Minimum eight characters, at least one letter, one number and one special character"
                }
            },
            confirmPassword: {
                presence: true,
                equality: {
                    attribute: "password",
                    message: "The passwords does not match"
                }
            },
            nickname: {
                presence: true,
                format: {
                    pattern: "^[a-z0-9_-]{3,15}$",
                    flags: "i",
                    message: "Match characters and symbols in the list, a-z, 0-9, underscore, hyphen. Length at least 3 characters and maximum length of 15."
                }
            },
            playlistTitle: {
                presence: true,
                format: {
                    pattern: "^[a-z0-9_ -@]{4,20}$",
                    flags: "i",
                    message: "Match characters and symbols in the list, a-z, 0-9, @ _ -. Length at least 4 characters and maximum length of 20."
                }
            },
            date: {
                numericality: {
                    lessThanOrEqualTo: 31,
                    greaterThan: 0,
                    message: "- should be between 01-31."
                },
            },
            month: {
                numericality: {
                    lessThanOrEqualTo: 12,
                    greaterThan: 0,
                    message: "- must be between 01-12."
                },
            },
            year: {
                presence: true,
                format: {
                    pattern: "^(19[0-8][0-9]|199[0-9]|20[01][0-9]|2020)$",
                    flags: "i",
                    message: "- must be between 1900 - 2020."
                }
            }
        };

        let validateBy = {};

        fields.forEach(field => {
            validateBy[field] = constraints[field];
        });

        return validate(data, validateBy);
    }

    /**
     * Open and close tabs
     *
     * Main container must have class 'tabs-container'
     * Navigation by tabs must have class 'tab' and attr 'data-tab'. Example: data-tab="debit-card"
     * Description must have class 'tab-data' and attr 'data-tab-name'. Example: data-tab-name="debit-card"
     * If you want make tab active, then add class 'active' to tab and tab-data
     */
    tabs() {
        let tabsContainers = document.getElementsByClassName('tabs-container');

        Array.from(tabsContainers).forEach(function (element) {
            let tabsButtons = element.querySelectorAll('.tab');

            Array.from(tabsButtons).forEach(function (tab) {
                tab.addEventListener('click', function () {

                    // If click not active tab - open new tab and close old
                    if (!this.classList.contains('active') && !this.classList.contains('disabled')) {
                        let tabsContainer = this.closest('.tabs-container');

                        // Remove active tab
                        Array.from(tabsContainer.querySelectorAll('.tab.active')).forEach(function (item) {
                            item.classList.remove('active');
                        });
                        Array.from(tabsContainer.querySelectorAll('.tab-data.active')).forEach(function (item) {
                            item.classList.remove('active');
                        });

                        // Open new tab
                        let tabData = tabsContainer.querySelectorAll(`.tab-data[data-tab-name=${this.getAttribute('data-tab')}]`);

                        this.classList.add('active');
                        if (tabData.length) {
                            tabData[0].classList.add('active');
                        }

                    }
                });
            });
        });
    }

    /**
     * Mask for payment form after registration
     *
     * @return {{card: InputMask, date: InputMask, cvv: InputMask}}
     */
    paymentFormMask() {

        let card = IMask(document.getElementById('payment-card'),
            {
                mask: '0000 0000 0000 0000',
                lazy: false,
                placeholderChar: 'X'
            });
        let date = IMask(document.getElementById('payment-date'),
            {
                mask: 'MM / YY',
                lazy: false,
                blocks: {
                    MM: {
                        mask: IMask.MaskedRange,
                        from: 1,
                        to: 12,
                        placeholderChar: 'M'
                    },
                    YY: {
                        mask: IMask.MaskedRange,
                        from: 19,
                        to: 99,
                        placeholderChar: 'Y'
                    }
                }
            });
        let cvv = IMask(document.getElementById('payment-ccv'),
            {
                mask: 'CVV',
                lazy: false,
                blocks: {
                    C: {
                        mask: IMask.MaskedRange,
                        from: 1,
                        to: 9,
                        placeholderChar: 'C'
                    },
                    V: {
                        mask: IMask.MaskedRange,
                        from: 0,
                        to: 9,
                        placeholderChar: 'V'
                    },
                }
            });

        return {
            card: card,
            date: date,
            cvv: cvv
        }
    }

    /**
     * Serialize all form data into an array
     *
     * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
     * @param  {Node}   form The form to serialize
     * @return {array} The serialized form data
     */
    serializeArray(form) {
        // Setup our serialized data
        let serialized = [];

        // Loop through each field in the form
        for (let i = 0; i < form.elements.length; i++) {

            let field = form.elements[i];

            // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
            if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;

            // If a multi-select, get all selections
            if (field.type === 'select-multiple') {
                for (let n = 0; n < field.options.length; n++) {
                    if (!field.options[n].selected) continue;
                    serialized.push({
                        name: field.name,
                        value: field.options[n].value
                    });
                }
            }

            // Convert field data to a query string
            else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                serialized.push({
                    name: field.name,
                    value: field.value
                });
            }
        }

        return serialized;
    }

    /**
     * Change status for Preloader
     *
     * @param status
     */
    setPreloaderStatus(status) {
        let preloader = document.getElementById('preloader');

        status ? preloader.classList.remove('hidden') : preloader.classList.add('hidden');
    }

    /**
     * No scroll
     *
     * @param status
     */

    noScrollBody(status) {
        let body = document.getElementsByTagName('body')[0];

        status ? body.classList.add('no-scroll') : body.classList.remove('no-scroll');
    }

    toogleUserMenu() {
        let userMenu = document.querySelector('header .user-menu');

        if (userMenu) {
            userMenu.querySelector('.user-avatar').addEventListener('click', function () {
                userMenu.classList.toggle('hidden');
            })
        }
    }

    lazyScroll() {
        let menuLink = document.querySelectorAll('a[href^="#"].lazy-scroll'),
            headerOffset = 113;

        if (document.querySelector('body').classList.contains('admin-bar')) {
            headerOffset += 15;
        }

        menuLink.forEach(anchor => {
            anchor.addEventListener('click', function (e) {
                e.preventDefault();

                document.querySelector('ul.menu li.active').classList.remove('active');
                this.parentElement.classList.add('active');

                window.scrollTo({
                    top: document.querySelector(this.getAttribute('href')).offsetTop - headerOffset,
                    behavior: "smooth"
                });
            });
        });

        if (menuLink.length) {
            window.onscroll = function () {
                let scrollPosition = document.documentElement.scrollTop,
                    activeMenu = document.querySelector('ul.menu li.active a');

                menuLink.forEach(menuItem => {
                    let menuBlock = document.querySelector(menuItem.getAttribute('href'));

                    let menuBlockScrollData = {
                        top: menuBlock.offsetTop,
                        height: menuBlock.offsetHeight
                    };

                    if ((scrollPosition + 130) > menuBlockScrollData.top && (scrollPosition + 130) < menuBlockScrollData.top + menuBlockScrollData.height) {

                        if (activeMenu && (activeMenu.getAttribute('href').substring(1) !== menuBlock.getAttribute('id'))) {
                            document.querySelector('ul.menu li.active').classList.remove('active');
                            document.querySelector(`ul.menu a[href="#${menuBlock.getAttribute('id')}"]`).parentElement.classList.add('active');
                        } else if (activeMenu === null) {
                            document.querySelector(`ul.menu a[href="#${menuBlock.getAttribute('id')}"]`).parentElement.classList.add('active');
                        }
                    }

                })
            }
        }

    }

    /**
     * Making Pass visible
     */
    openPassInput() {
        document.querySelectorAll('.pass-open').forEach(field => {
            field.parentElement.querySelectorAll('button').forEach(button => {
                button.addEventListener('click', function (e) {
                    e.preventDefault();

                    switch (this.getAttribute('data-status')) {
                        case 'open':
                            this.parentElement.setAttribute('data-pass-status', 'close');
                            this.parentElement.querySelector('input').setAttribute('type', 'text');
                            break;
                        case 'close':
                            this.parentElement.setAttribute('data-pass-status', 'open');
                            this.parentElement.querySelector('input').setAttribute('type', 'password');
                            break;
                    }
                })
            })
        })
    }

    /**
     *  Button - scroll to id element
     */

    buttonScroll() {
        let buttons = document.querySelectorAll('button[data-scroll-to]');

        if (buttons.length) {
            buttons.forEach(button => {
                button.addEventListener('click', function () {
                    window.scrollTo({
                        top: document.getElementById(this.getAttribute('data-scroll-to')).offsetTop - 120,
                        behavior: "smooth"
                    });
                })
            });
        }
    }

    searchStation() {
        let search = document.getElementById('search'),
            stations = [];

        document.querySelectorAll('article.single-station').forEach(station => {
            stations.push({
                id: station.getAttribute('data-id'),
                name: station.getAttribute('data-name'),
                artists: station.getAttribute('data-artists')
            })
        });

        if (search && search.querySelector('input').value) {
            filtered(search.querySelector('input').value);
        }

        if (search && stations) {
            search.querySelector('.search-sidebar').addEventListener('input', function () {

               filtered(this.value);
            });
        }

        function filtered(searchValue) {
            let stationsFiltered = stations.filter(station => {
                return station.name.toLowerCase().indexOf(searchValue.toLowerCase()) > -1
                    || station.artists.toLowerCase().indexOf(searchValue.toLowerCase()) > -1
            });

            stations.forEach(station => {
                let item = document.querySelector(`article.single-station[data-id="${station.id}"]`);

                if (stationsFiltered.find(element => element.id === station.id) !== undefined) {
                    item.classList.remove('hidden');
                } else {
                    item.classList.add('hidden');
                }
            })
        }
    }

    searchMainPage() {
        let form = document.getElementById('main-search-form');

        if (form) {
            form.addEventListener('keypress', function (e) {
                if (e.key === 'Enter') {
                    e.preventDefault();

                    window.location.href = '/channels/?search=' + form.querySelector('#main-search').value;
                }
            })
        }
    }

    search() {
        let search = document.getElementById('search'),
            stations = document.querySelectorAll('article.single-station');

        if (search) {
            search.querySelector('.search-sidebar').addEventListener('input', function () {
                // console.log(this.value);
                if (this.value.length) {
                    ajax.getSearchData(this.value, 'stations').then(result => {
                        result = JSON.parse(result);
                        if (result.status) {
                            let list = search.querySelector('.search-result-data');
                            list.innerHTML = '';

                            result.data.forEach(item => {
                                let data = '<li>';
                                data += `<a href="/${item.taxonomy}/${item.slug}">${item.name}</a>`;
                                data += '</li>';

                                list.insertAdjacentHTML('beforeEnd', data);
                            });

                            console.log(result.data);
                        } else {
                            helper.notifications.add('error', 'Search', result.message);
                        }
                    })
                }
            }, false);
        }
    }

    newSearch() {
        let search = document.getElementById('search');
        let activeTrack = null;

        if (search) {
            let searchInputTimeout = null;
            let popupSearch = document.getElementById('popup-search');
            let searchData = [];
            let buttonsPlay = null;
            let buttonsStop = null;

            search.querySelector('.search-sidebar').addEventListener('input', function () {
                let domChannels = popupSearch.querySelector('.channels .data-list'),
                    domPlaylists = popupSearch.querySelector('.playlists .data-list'),
                    domArtists = popupSearch.querySelector('.artists .data-list'),
                    domSongs = popupSearch.querySelector('.songs .data-list');

                if (this.value.length > 2) {
                    searchInputTimeout && clearTimeout(searchInputTimeout);
                    popup.openPopup('popup-search', ['search', 'with-header'], ['popup-search']);

                    searchInputTimeout = setTimeout(() => {
                        ajax.getSearchData(this.value, ['stations', 'songs', 'playlists', 'artists']).then(result => {
                            result = JSON.parse(result);

                            console.log(result);

                            if (result.status && popupSearch) {

                                searchData = [
                                    {
                                        class: 'channels',
                                        data: result.data.stations,
                                        el: domChannels,
                                        template: getStationTemplate,
                                        count: 12,
                                        offset: 0
                                    },
                                    {
                                        class: 'playlists',
                                        data: result.data.playlists,
                                        el: domPlaylists,
                                        template: getStationTemplate,
                                        count: 12,
                                        offset: 0
                                    },
                                    {
                                        class: 'artists',
                                        data: result.data.artists,
                                        el: domArtists,
                                        template: getSongsTemplate,
                                        count: 20,
                                        offset: 0
                                    },
                                    {
                                        class: 'songs',
                                        data: result.data.songs,
                                        template: getSongsTemplate,
                                        el: domSongs,
                                        count: 20,
                                        offset: 0
                                    }
                                ];

                                searchData.forEach(item => {
                                    if (item.data.length) {
                                        item.el.innerHTML = item.template(item.data);
                                        if (item.data.length > item.count) {
                                            displayButtonMore(item.el);
                                        }
                                    } else {
                                        notResult(item.el);
                                    }
                                });

                                buttonsPlay = popupSearch.querySelectorAll('.play-track');
                                buttonsStop = popupSearch.querySelectorAll('.stop-track');

                                buttonsPlay.forEach(button => {
                                    button.addEventListener('click', function () {
                                        playTrack(button);
                                    })
                                });

                                buttonsStop.forEach(button => {
                                    button.addEventListener('click', function () {
                                        stopTrack(button);
                                    })
                                });
                            }

                        })
                    }, 1500);
                } else {
                    clearList(domChannels);
                    clearList(domPlaylists);
                    clearList(domArtists);
                    clearList(domSongs);

                    popup.closePopup(['search', 'with-header'], ['popup-search'])
                }
            }, false);

            let moreButtons = popupSearch ? popupSearch.querySelectorAll('.more') : null;

            if (moreButtons) {
                moreButtons.forEach(button => {
                    button.addEventListener('click', function () {
                        let element = searchData.find(item => {
                            let bool = item.class === this.getAttribute('data-class');

                            bool && (item.offset += item.count);

                            return bool;
                        });



                        element.el.insertAdjacentHTML('beforeend', element.template(element.data, element.offset, element.count));

                        buttonsPlay.forEach(button => {
                            let newElement = button.cloneNode(true);
                            button.parentNode.replaceChild(newElement, button);
                        });
                        buttonsPlay = popupSearch.querySelectorAll('.play-track');
                        buttonsPlay.forEach(button => {
                            button.addEventListener('click', function () {
                                playTrack(button);
                            })
                        });

                        buttonsStop.forEach(button => {
                            let newElement = button.cloneNode(true);
                            button.parentNode.replaceChild(newElement, button);
                        });
                        buttonsStop = popupSearch.querySelectorAll('.stop-track');
                        buttonsStop.forEach(button => {
                            button.addEventListener('click', function () {
                                stopTrack(button);
                            })
                        });

                        if (element.data.length - element.offset <= element.count) {
                            hideButtonMore(element.el);
                        }
                    })
                });

            }

        }

        function getStationTemplate(list, offset = 0, count = 12) {
            let result = '';

            list.forEach((item, index) => {
                if (offset <= index && (offset + count) > index) {
                    result += `<a href="${item.url}" class="single-station-template">`;
                    result +=   `<img src="${item.thumbnail}">`;
                    result +=   `<h6>${item.name}</h6>`;
                    result += `</a>`;
                }
            });

            return result;
        }

        function getSongsTemplate(list, offset = 0, count = 20) {
            let result = '';

            list.forEach((item, index) => {
                if (offset <= index && (offset + count) > index) {
                    result += `<figure data-track-id="${item.ID}">`;
                    result +=   `<div class="song-actions">`;
                    result +=       `<div class="play-stop" data-status="stop">`;
                    result +=           `<button class="play-track button__clear" data-id="${item.ID}"><i class="fa fa-play" aria-hidden="true"></i></button>`;
                    result +=           `<button class="stop-track button__clear" data-id="${item.ID}"><i class="fa fa-stop" aria-hidden="true"></i></button>`;
                    result +=       `</div>`;
                    result +=   `</div>`;
                    result +=   `<div class="title">${item.song_title} - ${item.song_artist}</div>`;
                    result +=   `<div class="audio-track"
                                    data-title="${item.song_title}"
                                    src="${item.song_url}"
                                    data-artist="${item.song_artist}"
                                    data-track-name="${item.song_title}"
                                    data-track-id="${item.ID}"
                                    data-track-thumbnail="${item.thumbnail}"
                                    data-track-duration="${item.duration}"
                                ></div>`;
                    result += `</figure>`;
                }
            });

            return result;
        }

        function clearList(el) {
            el.innerHTML = '';
        }

        function displayButtonMore(el) {
            let parent = el.parentElement;

            parent.querySelector('.more').classList.add('active');
        }

        function hideButtonMore(el) {
            let parent = el.parentElement;

            parent.querySelector('.more').classList.remove('active');
        }

        function notResult(el) {
            el.innerHTML = '<span>Sorry, no results were found for this request.</span>';
        }
        
        function playTrack(button) {
            let trackID = button.getAttribute('data-id');
            let parent = document.querySelector(`figure[data-track-id="${trackID}"]`);

            if (parent) {
                let trackData = parent.querySelector('.audio-track');

                if (activeTrack) {
                    activeTrack.pause();
                }

                activeTrack = new Audio(trackData.getAttribute('src'));

                activeTrack.onloadedmetadata = function () {
                    document.querySelectorAll('.play-stop').forEach(status => {
                       status.setAttribute('data-status', 'stop');
                    });
                    parent.querySelector('.play-stop').setAttribute('data-status', 'play');
                    this.play();
                };
            }
        }

        function stopTrack(button) {
            let trackID = button.getAttribute('data-id');
            let parent = document.querySelector(`figure[data-track-id="${trackID}"]`);

            if (parent) {
                if (activeTrack) {
                    activeTrack.pause();
                }

                parent.querySelector('.play-stop').setAttribute('data-status', 'stop');
            }
        }

    }

    updateSongsTime(tracks = [], isHasKeys = true) {
        let promises = [],
            _tracks = tracks.length ? tracks : document.querySelectorAll('.tracks-data-helper span');

        if (_tracks.length) {
            _tracks.forEach(track => {

                promises.push(
                    new Promise((resolve => {
                        let audio = new Audio(track.getAttribute('data-track-url'));

                        audio.onloadedmetadata = () => {
                            let temp = {
                                track_id: track.getAttribute('data-track-id'),
                                track_time: audio.duration,
                                isHaveKey: isHasKeys
                            };
                            resolve(temp);
                        };
                    }))
                )

            });

            Promise.all(promises).then(values => {
                ajax.setTracksTime(values).then(response => {
                    console.log(response);
                })
            });
        }
    }

    tracksDuration() {
        let container = document.getElementById('songs-duration-update');

        if (container) {
            container.querySelector('button').addEventListener('click', function() {
                this.classList.add('_button_ajax_load');

                let count = 10;
                let counter = 1;
                let tracks = container.querySelectorAll('.track-data');
                let addedTracks = 0;
                let promises = [];

                if (tracks.length) {
                    for (let i = 0; i < tracks.length; i += count ) {
                        promises[i] = [];
                        let _tracks = Array.from(tracks).slice(i, counter * count);

                        console.log(tracks, _tracks, promises, i,  counter * count);

                        _tracks.forEach(track => {
                            promises[i].push(
                                new Promise((resolve => {
                                    let audio = new Audio(track.getAttribute('data-track-url'));

                                    audio.onloadedmetadata = () => {
                                        let temp = {
                                            track_id: track.getAttribute('data-track-id'),
                                            track_time: audio.duration,
                                            isHaveKey: false
                                        };
                                        resolve(temp);
                                    };
                                }))
                            )
                        });

                        Promise.all(promises[i]).then(values => {
                            ajax.setTracksTime(values).then(response => {
                                if (response === '1') {
                                    addedTracks += values.length;
                                    let description = `${addedTracks} tracks have been successfully added.<br>`;
                                    description += 'Tracks ID:<br>';

                                    values.forEach((value, index)=> {
                                        description += value.track_id + (value.length !== index + 1 ? ', ' : '');
                                    });

                                    helper.notifications.add('success', 'Tracks added', description);

                                    if (addedTracks === tracks.length) {
                                        ajax.buttonSuccess(this);
                                    }
                                }
                            })
                        });

                        ++counter;
                    }
                }
            });
        }
    }

    /**
     * Check file and validate
     *
     * @param file
     * @return {boolean}
     */
    sendFileValidate(file) {
        let maxFileSize = 5242880;

        return ((file.size <= maxFileSize) && ((file.type === 'image/png') || (file.type === 'image/jpeg')));
    }

    /**
     * Load scripts when this need
     */
    init() {
        // this.paymentFormMask();
        this.buttonScroll();
        this.openPassInput();
        this.lazyScroll();
        this.toogleUserMenu();
        this.tabs();
        // this.searchStation();
        this.newSearch();
        this.searchMainPage();
    }

    constructor() {
        this.notifications = new Notifications();
        this.tooltip = new Tooltip();
        this.DOMLoad();
    }

}

class Notifications {
    constructor() {
        this.section = document.getElementById('notifications-popup');
        this.noticeTime = 6000;
    }

    add(type, title, message) {
        let key = `f${(~~(Math.random() * 1e8)).toString(16)}`;
        ajax.getNotificationTemplate(type, title, message, key).then(result => {

            this.section.insertAdjacentHTML('beforeEnd', result);
            this.addTrashListener();

            let notice = this.section.querySelector(`article[data-key=${key}]`);

            // Delay Animation for open notice
            setTimeout(() => {
                notice.setAttribute('style', 'transition: .4s; transform: translateY(0px); opacity: 1;');
            }, 300);

            // Automatic Close notice
            setTimeout(() => {
                this.trashNotice(notice);
            }, this.noticeTime);
        });
    }

    addTrashListener() {
        let buttons = this.section.querySelectorAll('.close-notification');
        let that = this;

        buttons.forEach(button => {
            button.addEventListener('click', function () {
                let notice = this.parentElement;

                that.trashNotice(notice);
            })
        });
    }

    trashNotice(notice) {
        notice.setAttribute('style', 'transition: .4s; transform: translateY(-50px); opacity: 0;');
        setTimeout(() => {
            notice.remove();
        }, 500);
    }
}

class Tooltip {
    constructor() {
        this.openTooltip();
    }

    openTooltip() {
        let that = this;
        let buttons = document.querySelectorAll('button.tooltip');

        buttons.forEach(button => {
            button.addEventListener('click', function (e) {
                let buttonIcon = button.querySelector('i.fa');

                if (e.target !== this && buttonIcon !== e.target) return;

                e.preventDefault();

                if (this.classList.contains('open')) {
                    this.classList.toggle('open');
                } else {
                    that.closeAllTooltips(buttons);
                    this.classList.toggle('open');
                }


            })
        });
    }

    closeAllTooltips(buttons = null) {
        buttons = buttons ? buttons : document.querySelectorAll('button.tooltip');

        buttons.forEach(button => {
            button.classList.remove('open');
        })
    }
}

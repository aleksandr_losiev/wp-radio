/**
 * Ajax class
 *
 *
 */
class Ajax {

    getData(data) {

        return jQuery.post(ajaxUrl.url, data, function (response) {
            return response;
        });
    }

    /**
     * Get user Data
     *
     * @param field ('ID', 'email')
     * @param value
     * @return {Promise<any>}
     */
    getUserDataBy(field, value) {
        return new Promise(resolve => {
            let data = {
                action: 'getUserDataBy',
                field: field,
                value: value
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Authenticate a user, confirming the login credentials are valid.
     *
     * @link https://developer.wordpress.org/reference/functions/wp_authenticate/
     *
     * @param username
     * @param password
     * @return {Promise<any>}
     */
    wpAuthenticate(username, password) {
        return new Promise(resolve => {
            let data = {
                action: 'wpAuthenticate',
                username: username,
                password: password
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Get station popup template
     *
     *  @param stationID
     *  @return {string}
     */
    getStationPopupTemplate(stationID) {
        return new Promise(resolve => {
            let data = {
                action: 'getStationPopupTemplate',
                stationID: stationID
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Set new password for User
     *
     * @param pass
     * @return {Promise<any>}
     */
    setNewPassword(pass) {
        return new Promise(resolve => {
            let data = {
                action: 'setNewPassword',
                pass: pass
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Get Notification template
     *
     * @param type
     * @param title
     * @param message
     * @param key
     * @return {Promise<any>}
     */
    getNotificationTemplate(type, title, message, key = '') {
        return new Promise(resolve => {
            let data = {
                action: 'getNotificationTemplate',
                type: type,
                title: title,
                message: message,
                key: key
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Get Template part
     *
     * @param template
     * @param params
     * @return {Promise<any>}
     */
    /*
    getTemplate(template, params = null) {
        return new Promise(resolve => {
            let data = {
                action: `getTemplate${template}`,
                params: params
            };

            resolve(this.getData(data));
        });
    }
    */

    /**
     * Set new data for current user
     *
     * @param userData
     * @return {Promise<any>}
     */
    setUserData(userData) {
        return new Promise(resolve => {
            let data = {
                action: 'setUserData',
                data: userData
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Get search data by key
     *
     * @param value
     * @param type {array} (stations, songs, playlist, artist)
     * @returns {Promise<any>}
     */
    getSearchData(value, type = ['stations']) {
        return new Promise(resolve => {
            let data = {
                action: 'getSearchData',
                value: value,
                type: type
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Save new user playlist
     *
     * @param title
     * @param description
     * @return {Promise<any>}
     */
    setUserPlaylist(title, description = '') {
        return new Promise(resolve => {
            let data = {
                action: 'setUserPlaylist',
                title: title,
                description: description
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Update User Playlist by params
     *
     * @param title
     * @param term_id
     * @param description
     * @return {Promise<any>}
     */
    updateUserPlaylist(title, term_id, description = '') {
        return new Promise(resolve => {
            let data = {
                action: 'updateUserPlaylist',
                title: title,
                term_id: term_id,
                description: description
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Delete User Playlist
     * @param term_id
     * @return {Promise<any>}
     */
    deleteUserPlaylist(term_id) {
        return new Promise(resolve => {
            let data = {
                action: 'deleteUserPlaylist',
                term_id: term_id,
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Save track to User playlist
     *
     * @param track_id
     * @param term_id
     * @param taxonomy
     * @return {Promise<any>}
     */
    setTrackToPlaylist(track_id, term_id, taxonomy = 'playlist') {
        return new Promise(resolve => {
            let data = {
                action: 'setTrackToPlaylist',
                taxonomy: taxonomy,
                track_id: track_id,
                term_id: term_id,
            };

            resolve(this.getData(data));
        });
    }

    /**
     *  Delete track from User playlist
     *
     * @param track_id
     * @param term_id
     * @param taxonomy
     * @return {Promise<any>}
     */
    deleteTrackFromPlaylist(track_id, term_id, taxonomy = 'playlist') {
        return new Promise(resolve => {
            let data = {
                action: 'deleteTrackFromPlaylist',
                track_id: track_id,
                taxonomy: taxonomy,
                term_id: term_id,
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Save file in uploads
     *
     * @param filesData
     * @return {Promise<any>}
     */
    saveFiles(filesData) {
        return new Promise(resolve => {
            jQuery.ajax({
                url: ajaxUrl.url,
                type: 'POST',
                contentType: false,
                processData: false,
                data: filesData,
                success: function (response) {
                    resolve(response);
                    console.log('File uploaded successfully');
                }
            });
        });
    }

    /**
     * Save new avatar for User
     *
     * @param image_id
     * @return {Promise<any>}
     */
    setUserAvatar(image_id) {
        return new Promise(resolve => {
            let data = {
                action: 'setUserAvatar',
                image_id,
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Save or Update track duration
     *
     * @param postsData
     * @return {Promise<any>}
     */
    setTracksTime(postsData) {
        return new Promise(resolve => {
            let data = {
                action: 'setTracksTime',
                posts: postsData,
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Update User plan
     *
     * @param plan_id
     * @return {Promise<any>}
     */
    updateUserPlan(plan_id) {
        return new Promise(resolve => {
            let data = {
                action: 'updateUserPlan',
                plan_id,
            };

            resolve(this.getData(data));
        });
    }

    incrementViewToType(id, type = 'taxonomy') {
        return new Promise(resolve => {
            let data = {
                action: 'incrementViewToType',
                id,
                type
            };

            resolve(this.getData(data));
        });
    }

    /**
     * Buttons status when load and done request
     *
     * @type {{error(*, *=): void, success(*, *=): void}}
     */
    buttonError(el, delay = 3000) {
            el.classList.remove('_button_ajax_load');
            el.classList.add('_button_ajax_error');

            setTimeout(() => {
                el.classList.remove('_button_ajax_error')
            }, delay)
        }

    buttonSuccess(el, delay = 3000) {
        el.classList.remove('_button_ajax_load');
        el.classList.add('_button_ajax_done');

        setTimeout(() => {
            el.classList.remove('_button_ajax_done')
        }, delay)
    }

}

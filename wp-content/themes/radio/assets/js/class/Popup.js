/**
 * Popup class
 *
 * To add a pop-up, you need to add a template to the '/template-parts/popup' folder and styles in the '/scss/popup' folder.
 * To open the pop-up you need to add the class "open-modal" to the tag and add an attribute with the id of the
 * form name (Example: data-modal-id="sign-in")
 * A new file with styles that have been added must be added to the bottom of the file the 'index.scss' that is "scss/popup"
 */

class Popup {
    constructor() {
        this.body = document.getElementsByTagName('body')[0];
        this.popup = document.getElementById('popup');
        this.addListeners();
    }

    openPopup(id_section, popupClasses = [], bodyClasses = []) {
        this.popup.classList.add('open');
        if (popupClasses) {
            popupClasses.forEach(itemClass => {
                this.popup.classList.add(itemClass);
            })
        }
        this.body.classList.add('modal-is-open');
        if (bodyClasses) {
            bodyClasses.forEach(itemClass => {
                this.body.classList.add(itemClass);
            })
        }
        this.openPopupSection(id_section);
    }

    closePopup(popupClasses = [], bodyClasses = []) {
        this.popup.classList.remove('open');
        if (popupClasses) {
            popupClasses.forEach(itemClass => {
                this.popup.classList.remove(itemClass);
            })
        }
        this.body.classList.remove('modal-is-open');
        if (bodyClasses) {
            bodyClasses.forEach(itemClass => {
                this.body.classList.remove(itemClass);
            })
        }
        this.closePopupSections();
    }

    openPopupSection(id_section) {
        this.closePopupSections();
        document.getElementById(id_section).classList.add('active');
    }

    closePopupSections() {
        let sections = this.popup.querySelectorAll('article.active');

        sections.forEach(section => {
           section.classList.remove('active');
        });
    }

    addListeners() {
        let openModalElements = document.getElementsByClassName('open-modal'),
            closeModalElements = document.getElementsByClassName('close-modal');

        let popup = this;

        Array.from(openModalElements).forEach(function(element) {
            element.addEventListener('click', function() {
                popup.openPopup(this.getAttribute('data-modal-id'));
            });
        });

        Array.from(closeModalElements).forEach(function(element) {
            element.addEventListener('click', function (e) {
                e.preventDefault();
                popup.closePopup();
            });
        });
    }
}
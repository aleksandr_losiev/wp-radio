/**
 * class Routing
 *
 * custom routing helper for generate pages and templates by URL
 *
 */
class Routing {

    constructor() {
        this._activeRoute = null;

        this.routes = [
            {
                name: 'Stations',
                slug: 'stations',
                template: 'Stations'
            },
            {
                name: 'Radio Station',
                slug: 'radio',
                template: 'RadioTaxonomy',
                params: [
                    {
                        type: 'slug'
                    }
                ]
            },
            {
                name: 'Home',
                slug: '/'
            }
        ];

        // this.routeChangeListener();
        this.setActiveUrlData();
        // this.pageLoad();

    }

    /**
     /**
     * Callback when changed URL
     */
    routeChangeListener() {
        window.addEventListener('popstate', () => {
            this.setActiveUrlData();
        });
    }

    /**
     * Save in this.activeRoute new URL data
     */
    setActiveUrlData() {
        let path = window.location.pathname.split('/');

        this._activeRoute = path.filter(element => element !== '');
        // console.log(this._activeRoute);

        if (!this._activeRoute.length) {
            this._activeRoute = ['/']
        }

        // this.setScripts();
    }

    setScripts() {

        switch (this._activeRoute[0]) {
            case 'stations':

                break;
        }
    }


    /**
     * Change Url and save this Url in history and this.activeRoute
     *
     * @param data
     * @param title
     * @param url
     */
    setNewUrl(data, title, url) {
        window.history.pushState(data, title, url);

        this.setActiveUrlData();
    }

    pageLoad() {
        switch (this._activeRoute.newValue[0]) {
            case 'radio':
                console.log(document.getElementById('station').getAttribute('data-station-id'));
                audio.openStationPopup(document.getElementById('station').getAttribute('data-station-id'));

                break;
        }
    }

    /**
     * Call this method when changed URL
     */
    routeListener() {
        // console.log(this._activeRoute.newValue[0]);
        //When page Load


        // When changed URL

        if (!this._activeRoute.oldValue.length) return;

        let route = null,
            url = this._activeRoute.newValue;

        if (url.length) {
            route = this.routes.find(route => route.slug === url[0]);
        } else if (url.length === 0) {
            route = this.routes.find(route => route.slug === '');
        }

        if (route === undefined) route = null;

        if (route && route.template !== undefined) {
            this.innerTemplate(route.template);
        }

    }

    innerTemplate(template) {
        // ajax[`getTemplate${template}`]().then(response => {
        //     console.log(response);
        // });
        let body = document.getElementsByTagName('body')[0];

        if (template === 'Stations') {
            ajax.getTemplate(template).then(response => {
                body.querySelector('main').remove();
                body.querySelector('header').insertAdjacentHTML('afterEnd', response);
            });
        } else if (template === 'RadioTaxonomy') {
            ajax.getTemplate(template, {slug: this._activeRoute.newValue[1]}).then(response => {
                body.querySelector('main').remove();
                body.querySelector('header').insertAdjacentHTML('afterEnd', response);
            });
        }

        // ajax.getTemplateStations().then(response => {
        //     console.log(response);
        // });
        console.log(template);
        //ajax.getTemplateStations()
    }
}
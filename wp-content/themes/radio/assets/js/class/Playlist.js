/**
 * Playlist helper and methods
 */

class Playlist {
    constructor() {
        this.savePlaylist();
        this.editPlaylist();
        this.deletePlaylist();
        this.addToPlaylist();
    }

    /**
     * Added playlist to user
     */
    savePlaylist() {
        let that = this;
        let buttons = document.querySelectorAll('button.save-playlist');

        buttons.forEach(button => {
            button.addEventListener('click', function (e) {
                e.preventDefault();

                let form = this.closest('form');

                let formData = {
                    title: form.querySelector('.playlist-title').value.trim(),
                    description: form.querySelector('.playlist-description').value.trim(),
                    playlistID: form.querySelector('input[name=playlist-id]') ? form.querySelector('input[name=playlist-id]').value : null
                };

                let validateFields = {
                    playlistTitle: formData.title
                };

                let errors = helper.validateField(validateFields, ['playlistTitle']);

                if (formData.title.length < 4) {
                    helper.notifications.add('error', 'Add Playlist', 'Sorry, but title very small. Min length 4.');
                } else if (errors) {
                    for (let index in errors) {
                        helper.notifications.add('error', 'Edit Profile', errors[index][0]);
                    }
                } else {
                    if (formData.playlistID) {
                        ajax.updateUserPlaylist(formData.title, formData.playlistID, formData.description).then(response => {
                            response = JSON.parse(response);

                            if (response.status) {
                                helper.notifications.add('success', 'Edit Playlist', response.message);

                                let playlist = document.querySelector(`article.playlist[data-playlist-id="${response.data.term_id}"]`);

                                if (playlist) {
                                    playlist.querySelector('.data a').innerHTML = formData.title;
                                    playlist.querySelector('.data a').setAttribute('href', response.data.url);
                                }

                                SinglePlaylist.setPlaylistData(formData.playlistID, formData.title, formData.description);

                                popup.closePopup();
                            } else {
                                helper.notifications.add('error', 'Edit Playlist', response.message);
                            }
                        })
                    } else {
                        ajax.setUserPlaylist(formData.title, formData.description).then(response => {
                            response = JSON.parse(response);

                            if (response.status) {
                                let addForm = document.getElementById('add-playlist'),
                                    container = document.querySelector('section.playlists-container'),
                                    tooltip = document.querySelector('.song-data button.tooltip');

                                addForm.querySelector('input').value = '';
                                addForm.querySelector('textarea').value = '';

                                helper.notifications.add('success', 'Add Playlist', response.message);

                                if (container) {
                                    container.insertAdjacentHTML('afterBegin', that.getPlaylistTemplate(response.data));
                                }
                                if (tooltip) {
                                    tooltip.querySelector('.tooltip-content').insertAdjacentHTML('beforeEnd', that.getSingleListTemplate(response.data));

                                    let newList = document.querySelector(`a.add-to-playlist[data-playlist-id="${response.data.term_id}"]`);
                                    that.addToPlayListListener(newList);
                                }

                                popup.closePopup();
                            } else {
                                helper.notifications.add('error', 'Add Playlist', response.message);
                            }
                        });
                    }
                }
            })
        });
    }

    /**
     * Edit User playlist
     */
    editPlaylist() {
        let buttons = document.querySelectorAll('.edit-playlist');

        buttons.forEach(button => {
            button.addEventListener('click', function (e) {
                e.preventDefault();

                let form = document.getElementById('edit-playlist');

                form.querySelector('.playlist-title').value = this.getAttribute('data-playlist-name');
                form.querySelector('.playlist-description').value = this.getAttribute('data-playlist-description');
                form.querySelector('input[name=playlist-id]').value = this.getAttribute('data-playlist-id');

                popup.openPopup('playlist-edit');
            })
        });
    }

    /**
     * Delete user playlist
     */
    deletePlaylist() {
        let buttons = document.querySelectorAll('.delete-playlist'),
            buttonStep2 = document.querySelector('button.remove-playlist');

        buttons.forEach(button => {
           button.addEventListener('click', function (e) {
               e.preventDefault();

               let form = document.getElementById('playlist-delete');

               form.querySelector('h5').innerHTML = `Are you sure you want to delete "${this.getAttribute('data-playlist-name')}" Playlist`;
               form.querySelector('input[name=playlist-id]').value = this.getAttribute('data-playlist-id');
               popup.openPopup('playlist-delete');
           })
        });

        if (buttons.length) {
            buttonStep2.addEventListener('click', function (e) {
                e.preventDefault();

                let termID = document.querySelector('#playlist-delete input[name=playlist-id]').value;

                ajax.deleteUserPlaylist(termID).then(response => {
                    response = JSON.parse(response);

                    if (response.status) {
                        helper.notifications.add('success', 'Delete playlist', response.message);

                        document.querySelector(`article.playlist[data-playlist-id="${response.data}"]`).remove();

                        popup.closePopup();
                    }
                });

            })
        }
    }

    addToPlaylist() {
        let  buttons = document.querySelectorAll('a.add-to-playlist');

        if (buttons) {
            buttons.forEach(button => {
                this.addToPlayListListener(button);
            });
        }
    }

    addToPlayListListener(element) {
        element.addEventListener('click', function() {
            let button = this.closest('.tooltip'),
                trackID = 0;

            console.log(button);

            if (button.hasAttribute('data-song-id')) {
                trackID = Number(button.getAttribute('data-song-id'));
            } else if (document.getElementById('player')) {
                trackID = Number(document.querySelector('#player .description').getAttribute('data-active-track-id'));
            }

            let termID = Number(this.getAttribute('data-playlist-id')),
                taxonomy = this.getAttribute('data-taxonomy');
            if (termID === 0 || trackID === 0) {
                helper.notifications.add('error', 'Add Track', 'Not added track to playlist. Not id track or playlist');
                return;
            }
            ajax.setTrackToPlaylist(trackID, termID, taxonomy).then(response => {
                response = JSON.parse(response);

                console.log(response);
                if (response.status) {
                    helper.notifications.add('success', 'Add track', response.message);
                }
            });
        });
    }

    // Templates

    getPlaylistTemplate(data) {
        let result  = `<article class="playlist" data-playlist-id="${data.term_id}">`;
            result +=   `<a href="${data.url}">`;
            result +=       '<div class="playlists-songs"></div>';
            result +=   `</a>`;
            result +=   '<div class="data">';
            result +=       '<h6>';
            result +=           `<a href="${data.url}">${data.name}</a>`;
            result +=           '<button class="more-tooltip tooltip button__clear">';
            result +=               '<i class="fa fa-ellipsis-v" aria-hidden="true"></i>';
            result +=               '<span class="tooltip-content tooltip-content__bottom">';
            result +=                   `<a class="edit-playlist" data-playlist-id="${data.term_id}" data-playlist-name="${data.name}" data-playlist-description="${data.description}">Edit Playlist</a>`;
            result +=                   `<a class="delete-playlist" data-playlist-id="${data.term_id}" data-playlist-name="${data.name}">Delete</a>`;
            result +=               '</span>';
            result +=           '</button>';
            result +=       '</h6>';
            result +=       `<p>${data.count} tracks</p>`;
            result +=   '</div>';
            result += '</article>';

        return result;
    }

    getSingleListTemplate(data) {
        return `<a class="add-to-playlist" data-playlist-id="${data.term_id}">${data.name}</a>`
    }
}

/**
 * Script for playlist page (single page)
 *
 */
class SinglePlaylist {
    constructor() {
        this.singlePlaylist = document.getElementById('single-playlist');
        this.title = this.singlePlaylist.querySelector('.description h2 span');
        this.description = this.singlePlaylist.querySelector('.description .data');
        this.ID = this.singlePlaylist.getAttribute('data-playlist-id');

        this.editListener();
        this.deleteListener();
        this.deleteTrack();
    }

    /**
     *  Listener for button "Edit"
     *
     *  Open Popup for edit title & description playlist
     */
    editListener() {
        let button = this.singlePlaylist.querySelector('button.edit');

        if (button) {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                let form = document.getElementById('edit-playlist');

                form.querySelector('.playlist-title').value = this.title.innerHTML;
                form.querySelector('.playlist-description').value = this.description.innerHTML;
                form.querySelector('input[name=playlist-id]').value = this.ID;

                popup.openPopup('playlist-edit');
            });
        }
    }

    /**
     * Delete playlist & redirect to page with all playlists
     */
    deleteListener() {
        let button = this.singlePlaylist.querySelector('button.delete'),
            buttonConfirm = document.querySelector('body.tax-playlist button.remove-playlist');

        if (button) {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                let form = document.getElementById('playlist-delete');

                form.querySelector('h5').innerHTML = `Are you sure you want to delete "${this.title.innerHTML}" Playlist`;
                form.querySelector('input[name=playlist-id]').value = this.ID;
                popup.openPopup('playlist-delete');

            });

            buttonConfirm.addEventListener('click', (e) => {
                e.preventDefault();

                ajax.deleteUserPlaylist(this.ID).then(response => {
                    response = JSON.parse(response);

                    if (response.status) {
                        helper.notifications.add('success', 'Delete playlist', response.message);

                        setTimeout(() => {
                            location.href = '/my-list/';
                        }, 1000);
                    }
                });

            })
        }
    }

    /**
     * Delete track from tracks list on Playlist page
     */
    deleteTrack() {
        let that = this,
            buttonConfirm = '',
            taxonomy = document.querySelector('body').classList.contains('tax-playlist') ? 'playlist' : 'favorites';

        let buttons = this.singlePlaylist.querySelectorAll('.delete-track');

        if (taxonomy === 'playlist') {
            buttonConfirm = document.querySelector('body.tax-playlist button.remove-track');
        } else if (taxonomy === 'favorites'){
            buttonConfirm = document.querySelector('body.page-template-favorites button.remove-track');
        } else {
            // buttonConfirm = document.querySelector('button.remove-track');
            console.error('Track location not find!')
        }

        buttons.forEach((button) => {
           button.addEventListener('click', function (e) {
               e.preventDefault();

               let trackID = this.getAttribute('data-song-id');
               let track = that.singlePlaylist.querySelector(`figure[data-track-id="${trackID}"]`);
               let section = document.getElementById('track-delete');

               let artist = track.querySelector('.title-container h5').innerHTML,
                   trackName = track.querySelector('audio').getAttribute('data-track-name');

               section.querySelector('h6').innerHTML = `Are you sure you want to delete "${artist} - ${trackName}"?`;
               section.querySelector('input[name=track-id]').value = trackID;
               section.querySelector('input[name=playlist-id]').value = that.ID;

               popup.openPopup('track-delete');
           })
        });

        console.log(buttonConfirm);
        if (buttons.length && buttonConfirm) {
            buttonConfirm.addEventListener('click', (e) => {
               e.preventDefault();

                let form = document.getElementById('delete-track');
                let trackID = form.querySelector('input[name="track-id"]').value,
                    termID = form.querySelector('input[name="playlist-id"]').value;

                ajax.deleteTrackFromPlaylist(trackID, termID, taxonomy).then(response => {
                    response = JSON.parse(response);

                    if (response.status) {
                        helper.notifications.add('success', 'Delete track', 'Track has been deleted');

                        document.querySelector(`figure[data-track-id="${trackID}"]`).remove();
                        let tracksCounter = this.singlePlaylist.querySelector('.type span');

                        if (taxonomy === 'playlist' && tracksCounter) {
                            tracksCounter.innerHTML = Number(tracksCounter.innerHTML) - 1;
                        }
                        popup.closePopup();
                    }
                });
            });
        }
    }

    /**
     * Change playlist data on page
     *
     * @param ID
     * @param title
     * @param description
     */
    static setPlaylistData(ID, title, description) {
        let playList = document.getElementById('single-playlist');
        if (playList) {
            playList.setAttribute('data-playlist-id', ID);
            playList.querySelector('.description h2 span').innerHTML = title;
            playList.querySelector('.description .data').innerHTML = description;
        }

    }
}

let router = new Routing(),
    popup = new Popup(),
    helper = new Helper(),
    ajax = new Ajax(),
    playlist = new Playlist();
    // audio = new AudioData(),
    // station = null;
    // station = new StationPlayer(),

switch (router._activeRoute[0]) {
    case 'radio':
        new Station();
        break;
    case 'playlist':
    case 'favorites':
        new SinglePlaylist();
        new AudioPlayer();
        break;
}
jQuery(function($){
    $('.table-dashboard').footable();
});
jQuery(document).ready(function ($) {

    $(".carousel-main-page").owlCarousel({
        loop: false,
        autoWidth: true,
        margin: 10,
        items: 7,
        nav: false,
        dots: false
    });

    // $('.table-dashboard').footable();
});

document.addEventListener('DOMContentLoaded', function () {

    // window.addEventListener('popstate', function(e){console.log('url changed')});

    helper.init();
    // audio.init();

    helper.setPreloaderStatus(false);
    let maskData = helper.paymentFormMask(),
        wpSubmit = document.getElementById('wp-submit'),
        registerSubmit = document.getElementById('register-submit'),
        registerSubmit2 = document.getElementById('register-submit-2'),
        paymentSubmit = document.getElementById('payment-submit');


    /*
     * Validate Sign In form
     */
    if (wpSubmit) {
        wpSubmit.addEventListener('click', function (e) {
            e.preventDefault();

            this.classList.add('_button_ajax_load');

            let email = document.getElementById('user_login').value,
                pass = document.getElementById('user_pass').value;

            if (!email || !pass) {
                ajax.buttonError(this);
                helper.notifications.add('error', 'Login', 'Email or Password empty!');
            } else {
                ajax.wpAuthenticate(email, pass).then(result => {
                    result = JSON.parse(result);
                    if (result.status) {
                        ajax.buttonSuccess(this);
                        helper.notifications.add('success', 'Login', result.message);
                        setTimeout(() => {
                            document.getElementById('loginform').submit();
                        }, 1500);
                    } else {
                        helper.notifications.add('error', 'Login', result.message);
                        ajax.buttonError(this);
                    }
                });
            }

        });
    }

    /*
     * Validate registration form
     */
    if (registerSubmit) {
        registerSubmit.addEventListener('click', function (e) {
            e.preventDefault();

            let signUpForm = document.getElementById('sign-up');

            let formData = {
                email: document.getElementById('register_login').value,
                password: document.getElementById('register_pass').value,
                confirmPassword: document.getElementById('register_pass_again').value,
            };

            let errors = helper.validateField(formData),
                errorBlock = signUpForm.getElementsByClassName('errors')[0];

            // if (false) {
            if (errors !== undefined) {
                errorBlock.innerHTML = '';

                console.log(errors);

                for (let index in errors) {
                    let errorData = document.createElement('p');
                    errorData.innerHTML = `<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>${errors[index]}`;
                    errorBlock.appendChild(errorData);
                }

            } else {

                ajax.getUserDataBy('email', formData.email).then(result => {
                    // console.log(JSON.parse(result));

                    errorBlock.innerHTML = '';
                    if (!JSON.parse(result)) {
                        popup.openPopupSection('sign-up-2');

                        /*
                         * Add Listeners to second step
                         */
                        Array.from(document.getElementById('sign-up-2').querySelectorAll('input[name=level]')).forEach(function (element) {
                            element.addEventListener('click', () => {
                                document.getElementById('register-submit-2').classList.remove('_button__disabled');
                            });
                        });
                    } else {
                        let errorData = document.createElement('p');
                        errorData.innerHTML = '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>This Email used.';
                        errorBlock.appendChild(errorData);
                    }
                });
            }

        });
    }

    /*
     * Validate form with price (Sign Up form - step 2)
     */
    if (registerSubmit2) {
        registerSubmit2.addEventListener('click', function (e) {
            e.preventDefault();

            let signUpForm2 = document.getElementById('sign-up-2');
            let levelChecked = Array.from(signUpForm2.querySelectorAll('input[name=level]')).find(item => item.checked);

            if (levelChecked !== undefined) {
                popup.openPopupSection('sign-up-3');

                let paymentForm = document.getElementById('sign-up-3'),
                    registerPaymentForm = document.getElementById('register-payment'),
                    levelName = levelChecked.getAttribute('data-name'),
                    levelPrice = levelChecked.getAttribute('data-price');

                registerPaymentForm.querySelector('input[name=level-id]').value = levelChecked.value;
                registerPaymentForm.querySelector('input[name=level-name]').value = levelName;
                registerPaymentForm.querySelector('input[name=level-price]').value = levelPrice;

                paymentForm.querySelector('.plan .name span').innerHTML = levelName;
                paymentForm.querySelector('.plan .value').innerHTML = levelChecked.getAttribute('data-value');

                if (Number(levelPrice) === 0) {
                    paymentForm.querySelector('.tab.active').classList.remove('active');
                    paymentForm.querySelector('.tab-data.active').classList.remove('active');
                    paymentForm.querySelectorAll('.pay-type .tab').forEach(item => {
                        item.classList.add('disabled');
                    })
                }
            }

        });
    }

    /*
     * Validate Payment form
     */
    if (paymentSubmit) {
        paymentSubmit.addEventListener('click', function (e) {
            e.preventDefault();

            let required = {
                    base: ['username', 'bemail', 'password'],
                    paid: ['username', 'bemail', 'password', 'AccountNumber', 'ExpirationMonth', 'ExpirationYear', 'CVV']
                },
                status = true;

            let signUpForm = document.getElementById('sign-up'),
                signUpForm3 = document.getElementById('payment-form'),
                paymentForm = document.getElementById('register-payment');

            let email = signUpForm.querySelector('input[name=register_login]').value,
                pass = signUpForm.querySelector('input[name=pass]').value,
                level = paymentForm.querySelector('input[name=level-id]').value,
                isFreeAccount = !(paymentForm.querySelector('input[name=level-price]').value > 0);

            paymentForm.querySelector('input[name=username]').value = email.split('@')[0];
            paymentForm.querySelector('input[name=bemail]').value = email;
            paymentForm.querySelector('input[name=bconfirmemail]').value = email;
            paymentForm.querySelector('input[name=password]').value = pass;
            paymentForm.querySelector('input[name=password2]').value = pass;

            if (!isFreeAccount) {
                paymentForm.querySelector('input[name=AccountNumber]').value = maskData.card.unmaskedValue;
                if (maskData.date.unmaskedValue.length === 4) {
                    paymentForm.querySelector('input[name=ExpirationMonth]').value = maskData.date.unmaskedValue.substring(0, 2);
                    paymentForm.querySelector('input[name=ExpirationYear]').value = Number('20' + maskData.date.unmaskedValue.substring(2));
                }
                if (Number(maskData.cvv.unmaskedValue) > 99) {
                    paymentForm.querySelector('input[name=CVV]').value = Number(maskData.cvv.unmaskedValue);
                }
                if (maskData.card.unmaskedValue.length === 12) {
                    paymentForm.querySelector('input[name=AccountNumber]').value = maskData.card.unmaskedValue;
                }
            }

            paymentForm.setAttribute('action', paymentForm.querySelector('input[name=url]').value + `=${level}`);

            let paymentFormData = helper.serializeArray(paymentForm);

            paymentFormData.forEach(item => {
                let req = isFreeAccount ? required.base : required.paid;

                status = req.includes(item.name) && item.value === '' ? false : status;
            });

            if (status) {
                paymentForm.submit();
            }

        });
    }

    /*
    * Reset Password
    */
    let changePassButton = document.querySelector('#change-password > button');

    if (changePassButton) {
        changePassButton.addEventListener('click', function () {
            this.classList.add('_button_ajax_load');

            let formData = {
                password: document.getElementById('new-pass').value,
                confirmPassword: document.getElementById('new-pass-2').value,
            };

            let errors = helper.validateField(formData, ['password', 'confirmPassword']);

            if (errors !== undefined) {
                for (let index in errors) {
                    helper.notifications.add('error', 'Change Password', errors[index][0]);
                    ajax.buttonError(this);
                }
            } else {
                ajax.setNewPassword(formData.password).then(response => {
                    if (response) {
                        ajax.buttonSuccess(this);
                        helper.notifications.add('success', 'Change Password', 'New password successfully stored! <br>Soon you will be redirected to the main page.');
                        setTimeout(() => {
                            window.location.replace('/');
                        }, 5000)
                    } else {
                        ajax.buttonError(this);
                        helper.notifications.add('error', 'Change Password', 'Error on save new Password');
                    }
                });
            }
        });
    }

    /*
     * Edit Profile Form
     */
    let editProfileButton = document.querySelector('#edit-profile > button');
    let editAvatar = document.getElementById('edit-avatar');

    if (editAvatar) {
        editAvatar.addEventListener('change', function() {

            if (helper.sendFileValidate(this.files[0])) {
                let filesData = new FormData();

                filesData.append('file', this.files[0]);
                filesData.append('action', 'saveUserAvatar');

                ajax.saveFiles(filesData).then(response => {
                    let iconDone = document.querySelector('label[for="edit-avatar"] i');

                    document.querySelector('.sidebar .logo img').setAttribute('src', response);
                    document.querySelector('.user-container .user-avatar').setAttribute('src', response);
                    iconDone.classList.remove('hidden');

                    helper.notifications.add('success', 'Change avatar', 'Your avatar has been successfully updated!');

                    setTimeout(() => {
                        iconDone.classList.add('hidden');
                    }, 3000)
                });
            }
        });
    }

    // document.getElementById('edit-banner').addEventListener('change', function () {
    //     if (helper.sendFileValidate(this.files[0])) {
    //         let filesData = new FormData();
    //
    //         filesData.append('file', this.files[0]);
    //         filesData.append('action', 'saveUserBanner');
    //
    //     }
    // });

    if (editProfileButton) {

        editProfileButton.addEventListener('click', function () {
            this.classList.add('_button_ajax_load');

            let formData = {
                email: document.getElementById('edit-email').value,
                nickname: document.getElementById('edit-username').value,
                date: document.getElementById('edit-date').value,
                month: document.getElementById('edit-month').value,
                year: document.getElementById('edit-year').value
            };

            console.log(formData);
            return;

            let errors = helper.validateField(formData, ['email', 'nickname', 'date', 'month', 'year']);

            if (errors !== undefined) {
                for (let index in errors) {
                    helper.notifications.add('error', 'Edit Profile', errors[index][0]);
                    ajax.buttonError(this);
                }
            } else {
                formData.gender = document.getElementById('edit-gender').value;

                console.log(formData);

                if (formData.email === document.getElementById('edit-email').getAttribute('data-default-value')) {
                    delete formData.email;

                    ajax.setUserData(formData).then(response => {
                        response = JSON.parse(response);
                        if (response.status) {
                            ajax.buttonSuccess(this);
                            helper.notifications.add('success', 'Edit Profile', response.message);
                        } else {
                            ajax.buttonError(this);
                            helper.notifications.add('error', 'Edit Profile', response.message);
                        }
                    })
                } else {

                    ajax.getUserDataBy('email', formData.email).then(result => {
                        if (!JSON.parse(result)) {
                            console.log(result);
                            ajax.setUserData(formData).then(response => {
                                response = JSON.parse(response);
                                if (response.status) {
                                    ajax.buttonSuccess(this);
                                    helper.notifications.add('success', 'Edit Profile', response.message);
                                } else {
                                    ajax.buttonError(this);
                                    helper.notifications.add('error', 'Edit Profile', response.message);
                                }
                            })
                        } else {
                            ajax.buttonError(this);
                            helper.notifications.add('error', 'Edit Profile', 'Sorry, but this Email used.');
                        }
                    });
                }

            }
        })
    }

    let button = document.getElementById('set-user-avatar');

    if (button) {
        button.addEventListener('click', () => {
            let input = document.querySelector('#change-avatar input[name="change-avatar"]:checked');
            console.log(input.value);
            ajax.setUserAvatar(input.value).then(response => {
                response = JSON.parse(response);
                if (response.status) {
                    helper.notifications.add('success', 'Change avatar', response.message);
                    popup.closePopup();

                    console.log(input.nextElementSibling);

                    let imageUrl = input.nextElementSibling.querySelector('img').getAttribute('src');
                    document.querySelector('.sidebar .logo img').setAttribute('src', imageUrl);
                    document.querySelector('header .user-container .user-avatar').setAttribute('src', imageUrl);
                }
            })
        })
    }

    let changePlanPopup = document.getElementById('popup-change-plan');

    if (changePlanPopup) {
        let inputs = changePlanPopup.querySelectorAll('input[name="plan"]'),
            button = changePlanPopup.querySelector('input[type="submit"]'),
            newPlan = 0;

        inputs.forEach(input => {
            input.addEventListener('change', function () {
                console.log(this.value);
                if (this.value !== this.getAttribute('data-plan-id')) {
                    button.classList.remove('_button__disabled');
                    newPlan = this.value;
                } else {
                    newPlan = 0;
                    button.classList.add('_button__disabled');
                }
            });
        });

        button.addEventListener('click', function (e) {
            e.preventDefault();

            if (!button.classList.contains('_button__disabled') && newPlan) {
                ajax.updateUserPlan(newPlan).then(response => {
                    response = JSON.parse(response);
                    console.log(response)
                });
            }
        })
    }

});
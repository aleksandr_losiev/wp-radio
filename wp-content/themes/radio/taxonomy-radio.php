<?php
/**
 * The template for displaying custom taxonomies
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#custom-taxonomies
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();

$station = get_queried_object();

$term_key = "term_$station->term_id";
$songs = get_field('songs', $term_key) ?: [];
$id_station = $station->term_id;

include get_template_directory() . '/template-parts/main/main-taxonomy-radio.php';
include get_template_directory() . '/template-parts/content/station-single-popup.php';

get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

$tracks_data = get_option('load_tracks_time', []);

$tracks_data = $tracks_data ? json_decode($tracks_data) : $tracks_data;

?>

<footer>
    <div class="_container">
        <?php if (is_user_logged_in()) {
            get_template_part('template-parts/footer/footer', 'auth');
        } else {
            get_template_part('template-parts/footer/footer', 'default');
        } ?>
    </div>
</footer>

<?php get_template_part('template-parts/popup/popup'); ?>
<?php get_template_part('template-parts/components/notifications'); ?>

<?php if (count($tracks_data)): ?>
    <div class="tracks-data-helper">
        <?php foreach ($tracks_data as $track): ?>
            <?php
            switch (get_field('song_type', $track)) {
                case 1:
                    $songURL = get_field('song_url', $track);
                    break;
                case 2:
                    $songURL = get_field('song_file', $track)['url'];
            }
            ?>
            <?php if (isset($songURL) && $track): ?>
                <?php var_dump(get_post_meta($track, 'track_time', true)); ?>
                <span data-track-id="<?= $track; ?>" data-track-url="<?= $songURL ?>"></span>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

</div>

<?php wp_footer(); ?>

</body>
</html>

<?php

/**
 * Class Endpoints
 *
 * Custom endpoints
 *
 * Namespace: /wp-json/radio-api/v2
 *
 * Stations:
 * GET /stations - get data all stations
 * GET /stations/sort - get sorted stations
 * GET /stations/{id} - get data station by ID
 * GET /stations/{id}/songs - get song by station ID
 * PUT /stations/{id}/views/{user_id} - add new view to station
 * GET /stations/by-category-id/{id} - get stations by category ID
 *
 * GET /search/?s={query}- search station/s by name
 * GET /search-global/?s={query}&type={query2}- search in project and return data by types
 *
 * GET /playlists - get all playlist by user ID
 * GET /playlist/{id} - get playlist data by ID
 * POST /playlist/create - add new playlist to user
 * PUT /playlist/ - edit playlist
 * DELETE /playlist/ - delete playlist
 *
 * GET /track/{track_id}/playlist/{playlist_id} - Adding track to playlist
 * DELETE /track/{track_id}/playlist/{playlist_id} - Delete track in playlist
 *
 * POST /login - login user by log and pass
 * POST /register - register user by log and pass
 * POST /user/{id}/update- update user profile
 *
 * GET /membership - get all data about memberships level
 * PUT /membership/user/{user_id}/level/{level_id} - update user level
 *
 * GET /categories - get all categories of channels
 */

class Endpoints
{

    public $namespace = 'radio-api/v2';

    public function __construct()
    {
        add_action('rest_api_init', function () {
            $this->apiRoute('/stations', [$this, 'getStations']);
            $this->apiRoute('/stations/sort', [$this, 'getStationsSort']);
            $this->apiRoute('/stations/(?P<id>\d+)', [$this, 'getStationByID']);
            $this->apiRoute('/stations/(?P<id>\d+)/songs', [$this, 'getStationSongs']);
            $this->apiRoute('/stations/(?P<id>\d+)/views/(?P<user_id>\d+)', [$this, 'setStationView'], 'PUT');
            $this->apiRoute('/stations/by-category-id/(?P<id>\d+)', [$this, 'getStationsByCategoryID']);

            $this->apiRoute('/search/', [$this, 'searchStation']);
            $this->apiRoute('/search-global/', [$this, 'searchGlobal']);

            $this->apiRoute('/playlists', [$this, 'getPlaylists']);
            $this->apiRoute('/playlist/create', [$this, 'createPlaylist'], 'POST');
            $this->apiRoute('/playlist/(?P<id>\d+)', [$this, 'getPlaylist']);
            $this->apiRoute('/playlist/(?P<id>\d+)', [$this, 'updatePlaylist'], 'PUT');
            $this->apiRoute('/playlist/(?P<id>\d+)', [$this, 'deletePlaylist'], 'DELETE');

            $this->apiRoute('/track/(?P<track_id>\d+)/playlist/(?P<playlist_id>\d+)', [$this, 'setTrackToPlaylist']);
            $this->apiRoute('/track/(?P<track_id>\d+)/playlist/(?P<playlist_id>\d+)', [$this, 'deleteTrackInPlaylist'], 'DELETE');

            $this->apiRoute('/login', [$this, 'login'], 'POST');
            $this->apiRoute('/register', [$this, 'registration'], 'POST');
            $this->apiRoute('/user/(?P<id>\d+)/update', [$this, 'updateUser'], 'POST');
            $this->apiRoute('/user/(?P<id>\d+)/update/avatar', [$this, 'updateUserAvatar'], 'POST');

            $this->apiRoute('/membership', [$this, 'getMembershipData']);
            $this->apiRoute('/membership/user/(?P<id>\d+)/level/(?P<level_id>\d+)', [$this, 'putUserMembershipLevel'], 'PUT');

            $this->apiRoute('/categories', [$this, 'getCategoriesOfStations']);
        });
    }

    /**
     * Action for generate new request
     *
     * @param $url
     * @param null $callback
     * @param string $type
     */
    public function apiRoute($url, $callback = null, $type = 'GET')
    {
        register_rest_route($this->namespace, $url, array(
            'methods' => $type,
            'callback' => $callback,
        ));
    }

    /**
     * Get stations data
     *
     * @return mixed
     */
    public function getStations()
    {
        $stations = get_terms([
            'taxonomy' => 'radio',
            'hide_empty' => false,
        ]);

        foreach ($stations as $station) {

            $term_key = "term_$station->term_id";
            $radioType = get_field('radio_type', $term_key);

            $station->thumbnail = get_field('thumbnail', $term_key);
            $station->screensaver = get_field('screensaver', $term_key);
            $station->viewsData = get_term_meta($station->term_id, 'views_data', true);
            $station->category = RadioHelper::getStationCategory($radioType, true);
        }

        return $stations;
    }

    /**
     * Get station data by station ID
     *
     * @param $data
     * @return array
     */
    public function getStationByID($data)
    {
        $stations = $this->getStations();

        foreach ($stations as $station) {
            if ($station->term_id == $data['id']) {

                $station->time = [
                    'current' => time(),
                    'last_update' => get_term_meta($data['id'], 'update_timestamp', true) ?: time()
                ];

                return $station;
            }
        }

        return [];
    }

    /**
     * Get Songs by station ID
     *
     * @param $data
     * @return array
     */
    public function getStationSongs($data)
    {
        $result = [];
        $songs = get_field('songs', "term_" . $data['id']);

        foreach ($songs as $song) {

            $songURL = null;

            switch (get_field('song_type', $song->ID)) {
                case 1:
                    $songURL = get_field('song_url', $song->ID);
                    break;
                case 2:
                    $songURL = get_field('song_file', $song->ID)['url'];
            }

            $data = [
                'ID' => $song->ID,
                'title' => get_field('title', $song->ID),
                'artist' => get_field('artist', $song->ID),
                'year' => get_field('year', $song->ID),
                'thumbnail' => get_the_post_thumbnail_url($song->ID),
                'src' => $songURL
            ];

            array_push($result, $data);
        }

        return $result;
    }

    /**
     * Search station by name
     *
     * @param $data
     * @return array|int|WP_Error
     */
    public function searchStation($data)
    {
        $args = array(
            'taxonomy' => array('radio'), // taxonomy name
            'orderby' => 'id',
            'order' => 'ASC',
            'hide_empty' => false,
            'name__like' => $data->get_param('s')
        );

        return get_terms($args);
    }

    /**
     * Method can search by stations, playlist, artist & songs
     *
     * @param $data
     * @return array
     */
    public function searchGlobal($data)
    {
        $params = $data->get_params();
        $types = $params['types'] ? explode(',', $params['types']) : ['stations', 'playlists', 'artists', 'songs'];

        return RadioHelper::searchByProject($params['s'], $types);
    }


    /**
     * Update user Profile
     *
     * @param $data
     *   • param $data->nickname
     *   • param $data->email
     *   • param $data->gender
     *   • param $data->date
     *   • param $data->month
     *   • param $data->year
     *
     * @return array|int|WP_Error
     */
    public function updateUser($data)
    {
        $userKey = "user_" . $data['id'];
        $parameters = $data->get_params();
        $userData = [
            'ID' => $data['id'],
        ];

        if (isset($parameters['nickname'])) {
            $userData['nickname'] = $parameters['nickname'];
        }

        if (isset($parameters['email'])) {
            $userData['user_email'] = $parameters['email'];
        }

        if (isset($parameters['nickname']) || isset($parameters['email'])) {
            wp_update_user($userData);
        }

        isset($parameters['gender']) && update_field('user_gender', $parameters['gender'], $userKey);
        isset($parameters['date']) && update_field('user_birthday_day', $parameters['date'], $userKey);
        isset($parameters['month']) && update_field('user_birthday_month', $parameters['month'], $userKey);
        isset($parameters['year']) && update_field('user_birthday_year', $parameters['year'], $userKey);

        return $parameters;
    }

    /**
     * Update user avatar
     *
     * @param $data
     * @return string|array
     */
    public function updateUserAvatar($data)
    {
        if (isset($_FILES)) {
            $userKey = "user_" . $data['id'];

            $fileData = RadioHelper::saveFiles($_FILES);

            update_field('user_avatar', $fileData['id'], $userKey);

            return $fileData['url'];
        }

        return $this->notAllData();
    }

    /**
     * Login user by Login & Pass
     *
     * @param $data
     *      • $data->login (required)
     *      • $data->pass (required)
     *      • $data->remember
     *
     * @return string|void|WP_Error|WP_User
     */
    public function login($data)
    {
        $params = $data->get_params();

        if (isset($params['login']) && isset($params['pass'])) {

            $remember = isset($params['remember']) ? $params['remember'] : false;
            $user = wp_signon([
                'user_login' => $params['login'],
                'user_password' => $params['pass'],
                'remember' => $remember
            ], false);

            return $user;
        }

        return $this->notAllData();
    }

    /**
     * Create new User
     *
     * if user exist - return error
     *
     * @param $data
     *      • $data->username (required)
     *      • $data->password (required)
     *      • $data->email (required)
     *
     * @return array|int|WP_Error
     */
    public function registration($data)
    {
        $params = $data->get_params();

        if (isset($params['username']) && isset($params['password']) && isset($params['email'])) {
            $userID = wp_create_user($params['username'], $params['password'], $params['email']);

            return $userID;
        }

        return $this->notAllData();
    }

    /**
     * Get all playlists by User ID
     *
     * @param $data
     *          • $data->user_ID (required)
     *
     * @return array
     */
    public function getPlaylists($data)
    {
        $user_ID = (int)$data->get_param('user_ID');

        if ($user_ID) {
            return RadioHelper::getUserPlaylists($user_ID);
        }

        return $this->notAllData();
    }

    /**
     * Get playlist data & songs by ID
     *
     * @param $data
     * @return array
     */
    public function getPlaylist($data)
    {
        if (is_numeric($data['id'])) {
            $playlist = get_term($data['id']);

            $songs = get_posts(
                array(
                    'posts_per_page' => -1,
                    'post_type' => 'song',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'playlist',
                            'field' => 'term_id',
                            'terms' => $playlist->term_id,
                        )
                    )
                )
            );

            $songsData = [];

            foreach ($songs as $song) {
                if (get_field('song_type', $song->ID) == 1) {
                    $songData = get_field('song_url', $song->ID);
                } else {
                    $songData = get_field('song_file', $song->ID)['url'];
                }
                $result = [
                    'ID' => $song->ID,
                    'post_author' => $song->post_author,
                    'post_date' => $song->post_date,
                    'post_title' => $song->post_title,
                    'song_url' => $songData,
                    'song_thumbnail' => get_the_post_thumbnail_url($song->ID),
                    'song_year' => get_field('year', $song->ID),
                    'song_artist' => get_field('artist', $song->ID),
                    'song_title' => get_field('title', $song->ID)
                ];

                $songsData[] = $result;
            }

            $playlist->songs = $songsData;

            return $playlist;
        }

        return $this->notAllData();
    }

    /**
     * Create new playlist
     *
     * If not parent directory, then created parent directory
     *
     * @param $data
     *      • $data->user_ID (required)
     *      • $data->title (required)
     *      • $data->description
     *
     * @return array|object
     */
    public function createPlaylist($data)
    {
        $params = $data->get_params();

        if (isset($params['user_ID']) && isset($params['title'])) {
            $termID = RadioHelper::getUserPlaylistTermID($params['user_ID']);

            $term = wp_insert_term($params['title'], 'playlist', array(
                'description' => $params['description'] ?: $params['title'],
                'parent' => $termID,
            ));

            if ($term) {
                return get_term($term['term_id'], 'playlist');
            }
        }

        return $this->notAllData();
    }

    /**
     * Update user playlist
     *
     * @param $data
     *      • $data->title
     *      • $data->description
     *
     * @return array|object
     */
    public function updatePlaylist($data)
    {
        $params = $data->get_params();

        if (isset($params['title']) || isset($params['description'])) {
            $changes = [];

            if (isset($params['title'])) {
                $changes['name'] = $params['title'];
                $changes['slug'] = '';
            }

            if (isset($params['description'])) {
                $changes['description'] = $params['description'];
            }

            return wp_update_term($data['id'], 'playlist', $changes);
        }

        return $this->notAllData();
    }

    /**
     * Delete user playlist
     *
     * @param $data
     *
     * @return array|bool
     */
    public function deletePlaylist($data)
    {
        if (isset($data['id'])) {
            return wp_delete_term($data['id'], 'playlist');
        }

        return $this->notAllData();
    }

    /**
     * Adding track to playlist
     *
     * @param $data
     *
     * @return array|int (playlist_id)
     */
    public function setTrackToPlaylist($data)
    {
        if ($data['track_id'] && $data['playlist_id']) {
            return wp_set_post_terms($data['track_id'], $data['playlist_id'], 'playlist', true);
        }

        return $this->notAllData();
    }

    /**
     * Delete track from playlist
     *
     * @param $data
     *
     * @return array|bool
     */
    public function deleteTrackInPlaylist($data)
    {
        if ($data['track_id'] && $data['playlist_id']) {
            return wp_remove_object_terms($data['track_id'], $data['playlist_id'], 'playlist');
        }

        return $this->notAllData();
    }

    /**
     * Set view to station
     *
     * @param $data
     * @return array|bool
     */
    public function setStationView($data)
    {
        if ($data['id'] && $data['user_id']) {
            return RadioHelper::updateStationView($data['id'], $data['user_id']);
        }

        return $this->notAllData();
    }

    /**
     * Get sorted stations by params
     *
     * @param $data
     * @return array|int|WP_Error
     */
    public function getStationsSort($data)
    {
        $count = intval($data->get_param('count')) ?: 10;
        $sortBy = $data->get_param('sort_by') ?: 'popular';

        return RadioHelper::getSortStationsBy($count, $sortBy);
    }

    /**
     * Get membership levels description
     *
     * @return array
     */
    public function getMembershipData()
    {
        $levels = [];
        $pmpro_levels = pmpro_getAllLevels(false, true);

        foreach ($pmpro_levels as $level) {
            $levels[] = [
                'id' => $level->id,
                'name' => $level->name,
                'description' => $level->description,
                'price' => $level->initial_payment,
                'format_price' => pmpro_formatPrice( $level->initial_payment ),
            ];
        }

        return $levels;
    }

    /**
     * Update user level in membership
     *
     * @param $data
     *
     * @return bool
     */
    public function putUserMembershipLevel($data)
    {
        return pmpro_changeMembershipLevel( $data['id'], $data['level_id'] );

    }

    /**
     * Get all categories & data
     *
     * @return array
     */
    public function getCategoriesOfStations()
    {
        $categories = get_terms( [
            'taxonomy' => 'radio_type',
            'hide_empty' => false,
        ] );

        foreach ($categories as $category) {
            $category->count = count(RadioHelper::getFilteredStationByCategory($category->term_id));
        }

        return $categories;
    }

    /**
     * Get stations by category ID
     *
     * @param $data
     * @return array
     */
    public function getStationsByCategoryID($data)
    {
        $stations = RadioHelper::getFilteredStationByCategory($data->get_param('id'));

        foreach ($stations as $station) {
            $station->count = count($songs = get_field('songs', "term_" . $station->term_id));
        }

        return $stations;
    }

    /**
     * If not send all data, then return this scope
     *
     * @return array
     */
    public function notAllData()
    {
        return ['status' => false, 'error' => __('Not all data sending', 'radio')];
    }

}

new Endpoints();
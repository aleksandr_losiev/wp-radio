<?php

/**
 * Class Ajax
 *
 * Register ajax methods and response
 */
class Ajax
{

    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'ajaxUrl'], 99);

        add_action('wp_ajax_nopriv_getUserDataBy', [$this, 'getUserDataBy']);
        add_action('wp_ajax_getUserDataBy', [$this, 'getUserDataBy']);
        // TODO: Remove this nopriv on Production
        add_action('wp_ajax_nopriv_getStationPopupTemplate', [$this, 'getStationPopupTemplate']);
        add_action('wp_ajax_getStationPopupTemplate', [$this, 'getStationPopupTemplate']);

        add_action('wp_ajax_setNewPassword', [$this, 'setNewPassword']);

        add_action('wp_ajax_nopriv_getNotificationTemplate', [$this, 'getNotificationTemplate']);
        add_action('wp_ajax_getNotificationTemplate', [$this, 'getNotificationTemplate']);

        add_action('wp_ajax_setUserData', [$this, 'setUserData']);

        add_action('wp_ajax_nopriv_wpAuthenticate', [$this, 'wpAuthenticate']);
        add_action('wp_ajax_wpAuthenticate', [$this, 'wpAuthenticate']);

        add_action('wp_ajax_getSearchData', [$this, 'getSearchData']);
        add_action('wp_ajax_nopriv_getSearchData', [$this, 'getSearchData']);

        add_action('wp_ajax_setUserPlaylist', [$this, 'setUserPlaylist']);

        add_action('wp_ajax_updateUserPlaylist', [$this, 'updateUserPlaylist']);

        add_action('wp_ajax_deleteUserPlaylist', [$this, 'deleteUserPlaylist']);

        add_action('wp_ajax_getTemplateRadioTaxonomy', [$this, 'getTemplateRadioTaxonomy']);

        add_action('wp_ajax_getTemplateStations', [$this, 'getTemplateStations']);

        add_action('wp_ajax_setTrackToPlaylist', [$this, 'setTrackToPlaylist']);

        add_action('wp_ajax_deleteTrackFromPlaylist', [$this, 'deleteTrackFromPlaylist']);

        add_action('wp_ajax_saveUserAvatar', [$this, 'saveUserAvatar']);

        add_action('wp_ajax_nopriv_setTracksTime', [$this, 'setTracksTime']);
        add_action('wp_ajax_setTracksTime', [$this, 'setTracksTime']);

        add_action('wp_ajax_setUserAvatar', [$this, 'setUserAvatar']);

        add_action('wp_ajax_updateUserPlan', [$this, 'updateUserPlan']);

        add_action('wp_ajax_nopriv_incrementViewToType', [$this, 'incrementViewToType']);
        add_action('wp_ajax_incrementViewToType', [$this, 'incrementViewToType']);
    }

    /**
     *  Add Ajax url to JS
     */
    public function ajaxUrl()
    {
        wp_localize_script('ajax', 'ajaxUrl',
            array(
                'url' => admin_url('admin-ajax.php')
            )
        );
    }

    /**
     * Get User by email on nickname
     *
     * • param $_POST['field'] {string} required
     * • param $_POST['value'] {string} required
     */
    public function getUserDataBy()
    {

        if ($_POST['field'] && $_POST['value']) {
            wp_die(json_encode(get_user_by($_POST['field'], $_POST['value'])));
        }

        wp_die(__('Not all data sending.', 'radio'));
    }

    /**
     *  Get Station data and template
     * (opened station on full-screen)
     *
     * • param $_POST['stationID'] {string||number} required
     */
    public function getStationPopupTemplate()
    {
        if ($_POST['stationID']) {
            if (get_term($_POST['stationID'])) {
                ob_start();
                $id_station = $_POST['stationID'];
                include get_template_directory() . '/template-parts/content/station-single-popup.php';
                $content = ob_get_clean();
                wp_die($content);
            } else {
                wp_die(__('Station not Found.', 'radio'));
            }
        }
        wp_die(__('Not station ID.', 'radio'));
    }


    /**
     *  Save new pass for current User
     *
     * • param $_POST['pass'] {string} required
     */
    public function setNewPassword()
    {
        if ($_POST['pass']) {
            wp_set_password($_POST['pass'], get_current_user_id());
            wp_die(1);
        }
        wp_die(__('New Password not sending.', 'radio'));
    }

    /**
     *  Get notification template
     *
     * • param $_POST['title'] {string} required
     * • param $_POST['type'] {string} required
     * • param $_POST['message'] {string} required
     * • param $_POST['key'] {string}
     */
    public function getNotificationTemplate()
    {
        $title = $_POST['title'];
        $type = $_POST['type'];
        $message = $_POST['message'];
        $key = $_POST['key'];

        if ($title && $type && $message) {
            ob_start();
            include get_template_directory() . '/template-parts/content/notification-single.php';
            $content = ob_get_clean();
            wp_die($content);
        }

        wp_die(__('Not all data sending.', 'radio'));
    }

    /**
     *  Get taxonomy template - Radio
     *
     * • param $_POST['stationID'] {number} required
     */
    public function getTemplateRadioTaxonomy()
    {
        if ($_POST['params']) {
            $station = get_term_by('slug', $_POST['params']['slug'], 'radio');
            $songs = get_posts(
                array(
                    'posts_per_page' => -1,
                    'post_type' => 'song',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'radio',
                            'field' => 'term_id',
                            'terms' => $station->term_id,
                        )
                    )
                )
            );

            ob_start();
            include get_template_directory() . '/template-parts/main/main-taxonomy-radio.php';
            $content = ob_get_clean();
            wp_die($content);
        }

        wp_die(__('Not sending station ID.', 'radio'));
    }

    /**
     * Get Station template
     */
    public function getTemplateStations()
    {
        $stations = get_terms([
            'taxonomy' => 'radio',
            'hide_empty' => false,
        ]);

        ob_start();
        include get_template_directory() . '/template-parts/main/main-stations.php';
        $content = ob_get_clean();
        wp_die($content);
    }

    /**
     * Update user metadata
     *
     * • param $_POST['data'] {array}
     * • return {json}
     */
    public function setUserData()
    {
        if ($_POST['data']) {
            $userID = get_current_user_id();
            $userKey = "user_$userID";

            $userData = [
                'ID' => $userID,
                'user_nicename' => $_POST['data']['nickname'],
            ];
            if (in_array('email', $_POST['data'])) {
                $userData['user_email'] = $_POST['data']['email'];
            }

            update_field('user_gender', $_POST['data']['gender'], $userKey);
            update_field('user_birthday_day', $_POST['data']['date'], $userKey);
            update_field('user_birthday_month', $_POST['data']['month'], $userKey);
            update_field('user_birthday_year', $_POST['data']['year'], $userKey);

            wp_update_user($userData);

            wp_die(json_encode([
                'status' => 1,
                'message' => __('New user data has been changed successfully!', 'radio'),
            ]));
        }

        $this->notAllData();
    }

    /**
     * Check user login and password
     *
     * • param $_POST['username'] {string}
     * • param $_POST['password'] {string}
     * • return {json}
     */
    public function wpAuthenticate()
    {
        if ($_POST['username'] && $_POST['password']) {
            $auth = wp_authenticate($_POST['username'], $_POST['password']);
            if (is_wp_error($auth)) {
                if (count($auth->errors)) {
                    wp_die(json_encode([
                        'status' => 0,
                        'message' => __('Invalid email address or email.', 'radio')
                    ]));
                }
            }
            wp_die(json_encode([
                'status' => 1,
                'message' => __('Login was successful.', 'radio')
            ]));
        }
        $this->notAllData();
    }

    /**
     * Get data by value
     *
     * • param $_POST['value'] {string}
     * • param $_POST['type'] {array}
     */
    public function getSearchData()
    {
        if ($_POST['value']) {

            wp_die(json_encode([
                'status' => 1,
                'data' => RadioHelper::searchByProject($_POST['value'], $_POST['type'])
            ]));
        }

        $this->notAllData();
    }

    /**
     * Save new playlist for User
     *
     * • param $_POST['title'] {string} required
     */
    public function setUserPlaylist()
    {
        if ($_POST['title']) {
            $termID = RadioHelper::getUserPlaylistTermID();

            $term = wp_insert_term($_POST['title'], 'playlist', array(
                'description' => $_POST['description'] ?: $_POST['title'],
                'parent' => $termID,
            ));

            if ($term) {
                $term = get_term($term['term_id'], 'playlist');
                $term->url = get_term_link($term->term_id, 'playlist');

                wp_die(json_encode([
                    'status' => 1,
                    'data' => $term,
                    'message' => __('Playlist was created.', 'radio')
                ]));
            }
            wp_die(json_encode([
                'status' => 0,
                'message' => __('Playlist not created!', 'radio'),
            ]));
        }

        $this->notAllData();

    }

    /**
     * Update playlist by term_id
     */
    public function updateUserPlaylist()
    {
        if ($_POST['title'] && $_POST['term_id']) {
            $term = wp_update_term($_POST['term_id'], 'playlist', [
                'name' => $_POST['title'],
                'description' => $_POST['description'],
                'slug' => ''
            ]);

            if ($term) {
                $term['url'] = get_term_link($term['term_id'], 'playlist');

                wp_die(json_encode([
                    'status' => 1,
                    'data' => $term,
                    'message' => __('Playlist was updated.', 'radio')
                ]));
            }
            wp_die(json_encode([
                'status' => 0,
                'message' => __('Playlist not updated!', 'radio'),
            ]));
        }

        $this->notAllData();
    }

    public function deleteUserPlaylist()
    {
        if ($_POST['term_id']) {
            $term = wp_delete_term($_POST['term_id'], 'playlist');

            if ($term) {
                $term = $_POST['term_id'];
                wp_die(json_encode([
                    'status' => 1,
                    'data' => $term,
                    'message' => __('Playlist was been deleted.', 'radio')
                ]));
            }
            wp_die(json_encode([
                'status' => 0,
                'message' => __('Playlist not deleted!', 'radio'),
            ]));
        }
        $this->notAllData();
    }

    /**
     * Add track to playlist
     *
     * • param $_POST['term_id'] {number} required
     * • param $_POST['track_id'] {number} required
     */
    public function setTrackToPlaylist()
    {
        if ($_POST['term_id'] && $_POST['track_id'] && $_POST['taxonomy']) {
            $result = wp_set_post_terms($_POST['track_id'], $_POST['term_id'], $_POST['taxonomy'], true);
            wp_die(json_encode([
                'status' => 1,
                'data' => $result,
                'message' => __('Track was been added.', 'radio')
            ]));
        }
        $this->notAllData();
    }

    /**
     * Delete track from user playlist
     *
     * • param $_POST['term_id'] {number} required
     * • param $_POST['track_id'] {number} required
     */
    public function deleteTrackFromPlaylist()
    {
        if ($_POST['term_id'] && $_POST['track_id'] && $_POST['taxonomy']) {
            $result = wp_remove_object_terms((int)$_POST['track_id'], (int)$_POST['term_id'], $_POST['taxonomy']);
            wp_die(json_encode([
                'status' => 1,
                'data' => $result,
                'message' => __('Track was been deleted.', 'radio')
            ]));
        }
        $this->notAllData();
    }

    /**
     * Save new avatar for User
     *
     * • param $_FILES required
     */
    public function saveUserAvatar()
    {
        if (isset($_FILES)) {
            $userID = get_current_user_id();
            $userKey = "user_$userID";

            $fileData = RadioHelper::saveFiles($_FILES);

            update_field('user_avatar', $fileData['id'], $userKey);

            wp_die($fileData['url']);
        }

        $this->notAllData();
    }

    /**
     * Set to tracks duration time
     */
    public function setTracksTime()
    {
        if (isset($_POST['posts'])) {
            $posts_id = json_decode(get_option('load_tracks_time'));

            foreach ($_POST['posts'] as $track) {
                if ($track['isHaveKey'] == 'true') {
                    if (($key = array_search($track['track_id'], $posts_id)) !== false) {
                        unset($posts_id[$key]);
                    }
                }

                update_post_meta( $track['track_id'], 'track_time', $track['track_time']);
            }

            update_option('load_tracks_time', json_encode($posts_id));

            wp_die(1);
        }

        $this->notAllData();
    }

    public function setUserAvatar()
    {
        if (isset($_POST['image_id'])) {
            $result = update_field('user_avatar', (int)$_POST['image_id'], "user_" . get_current_user_id());

            wp_die(json_encode([
                'status' => 1,
                'data' => $result,
                'message' => __('Avatar has been updated!', 'radio')
            ]));
        }

        $this->notAllData();
    }

//    public function updateUserPlan()
//    {
//        if (isset($_POST['plan_id'])) {
//            var_dump(get_user_meta(get_current_user_id()));
//            wp_die();
//        }
//
//        $this->notAllData();
//    }

    public function incrementViewToType()
    {
        if (isset($_POST['id'])) {
            $result = RadioHelper::incrementViewToType($_POST['id'], $_POST['type']);

            wp_die(json_encode([
                'status' => 1,
                'data' => $result,
                'message' => __('Updated views for', 'radio') . ' ' . $_POST['type']
            ]));
        }

        $this->notAllData();
    }

    /**
     * If not all data
     *
     * @param $message string
     *
     * return this callback
     */
    public function notAllData($message = null)
    {
        wp_die(json_encode([
            'status' => 0,
            'message' => $message ?: __('Not Send all data.', 'radio'),
        ]));
    }
}

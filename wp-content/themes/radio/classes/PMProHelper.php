<?php
/**
 *
 */

class PMProHelper {
    public static function init()
    {
        remove_filter('login_redirect','pmpro_login_redirect', 10, 3);
    }

    /**
     * Actions for register form
     */
    public static function payActions()
    {
        do_action("pmpro_checkout_after_level_cost");
        do_action('pmpro_checkout_after_pricing_fields');
        do_action('pmpro_checkout_after_username');
        do_action('pmpro_checkout_after_password');
        do_action('pmpro_checkout_after_email');
        do_action('pmpro_checkout_after_captcha');
        do_action('pmpro_checkout_after_user_fields');
        do_action('pmpro_checkout_boxes');
        do_action("pmpro_checkout_after_billing_fields");
        do_action('pmpro_checkout_after_payment_information_fields');
        do_action("pmpro_checkout_after_tos_fields");
        do_action("pmpro_checkout_before_submit_button");
        do_action('pmpro_checkout_after_form');
    }
}
<?php

class RadioHelper
{

    /**
     * Get social links with icons from WP menu
     * Generate this links in list
     *
     * @param string $custom_class
     * @return string
     */
    public static function getSocialList(string $custom_class = '')
    {
        $data = "<ul class='social-navigation-list $custom_class'>";

        $menu = wp_get_nav_menu_object(get_nav_menu_locations()['social']);
        $menu_items = wp_get_nav_menu_items($menu);

        foreach ($menu_items as $item) {
            $icon_classes = implode(' ', $item->classes);
            $data .= "<li><a href='$item->url' target='_blank'><i class='$icon_classes' aria-hidden='true'></i></a></li>";
        }

        $data .= '</ul>';

        return $data;
    }

    public static function getUserBirthdayDate($user_id = 0)
    {
        $user_id = $user_id ?: get_current_user_id();

        $day = get_field('user_birthday_day', 'user_' . $user_id);
        $month = get_field('user_birthday_month', 'user_' . $user_id);
        $year = get_field('user_birthday_year', 'user_' . $user_id);

        if ($day && $month && $year) {
            return $day . '/' . $month . '/' . $year;
        } else {
            return __('Date of Birthday not selected', 'radio');
        }

    }

    public static function getFilteredStationByCategory($cat_id = 0)
    {
        $result = [];

        $stations = get_terms([
            'taxonomy' => 'radio',
            'hide_empty' => false,
        ]);

        foreach ($stations as $station) {
            $categories = get_field('radio_type', "term_$station->term_id");

            foreach ($categories as $category) {
                if ($cat_id == $category->term_id) {
                    array_push($result, $station);
                }
            }
        }

        return $result;
    }

    /**
     * Get parent term_id playlist by User
     *
     * @param $user_ID
     *
     * @return mixed
     */
    public static function getUserPlaylistTermID($user_ID = null)
    {
        $userID = $user_ID ?: get_current_user_id();
        $term_slug = 'playlist_user_' . $userID;

        $term = get_term_by('slug', $term_slug, 'playlist');

        if (!$term) {
            $term = self::addUserMainPlaylist($term_slug, $userID);
        }

        return $term->term_id;
    }

    /**
     * Generate parent term_id for playlist by User
     *
     * @param string $term_slug
     * @param int $userID
     * @return mixed
     */
    public static function addUserMainPlaylist($term_slug = '', $userID = 0)
    {
        $userID = $userID ?: get_current_user_id();
        $term_slug = $term_slug ?: 'playlist_user_' . $userID;

        $term = wp_insert_term('User ID: ' . $userID . ' - All Playlists', 'playlist', array(
            'description' => 'All playlist for user ' . $userID,
            'parent' => 0,
            'slug' => $term_slug,
        ));

        return $term;
    }

    /**
     * Get all playlists by User ID
     *
     * If not send User id, then paste current User ID
     *
     * @param null $user_ID
     * @return array|int|WP_Error
     */
    public static function getUserPlaylists($user_ID = null)
    {
        $parent_termID = RadioHelper::getUserPlaylistTermID($user_ID);

        $playlists = get_terms(array(
            'hide_empty' => 0,
            'orderby' => 'id',
            'order' => 'DESC',
            'taxonomy' => 'playlist',
            'parent' => $parent_termID
        ));

        foreach ($playlists as $playlist) {
            $tracks = self::getTracksByPlaylist($playlist->term_id);

            $playlist->songs_thumbnails = [];

            foreach ($tracks as $track) {
                $playlist->songs_thumbnails[] = (object)[
                    'ID' => $track->ID,
                    'title' => $track->title,
                    'thumbnail' => get_the_post_thumbnail_url($track->ID)
                ];
            }
        }

        return $playlists;
    }

    public static function getTracksByPlaylist($term_id)
    {
        $args = array(
            'posts_per_page' => 4,
            'post_type' => 'song',
            'tax_query' => array(
                array(
                    'taxonomy' => 'playlist',
                    'field' => 'id',
                    'terms' => $term_id
                )
            )
        );

        $query = new WP_Query($args);

        return $query->posts;
    }

    /**
     * Get station category data
     *
     * Return in array or in HTML template
     *
     * @param $data
     * @param bool $isArray
     * @return array|string
     */
    public static function getStationCategory($data, $isArray = false)
    {
        $result = $isArray ? [] : '<div class="station-categories">';

        foreach ($data as $category) {
            $termLink = get_term_link($category->term_id, $category->taxonomy);
            if ($isArray) {
                $data = [
                    'term_id' => $category->term_id,
                    'taxonomy' => $category->taxonomy,
                    'name' => $category->name,
                    'slug' => $category->slug
                ];
                array_push($result, $data);
            } else {
                $result .= "<a href='$termLink'>$category->name</a>";
            }
        }

        if (!$isArray) {
            $result .= '</div>';
        }

        return $result;
    }

    /**
     * Get station views by ID
     *
     * @param $termID
     *
     * @return int
     */
    public static function getStationViews($termID)
    {
        $data = get_term_meta($termID, 'views_data', true);
        $counter = 0;

        if ($data) {
            foreach ($data as $item) {
                $counter += count($item);
            }
        } else {
            $counter = 0;
        }

        return $counter;
    }

    /**
     * Get count station views by current month
     *
     * @param $termID
     *
     * @return int
     */
    public static function getStationViewsByMonth($termID)
    {
        $date = new DateTime();
        $currentDate = $date->format('m-Y');
        $data = get_term_meta($termID, 'views_data', true);
        $counter = 0;

        if ($data) {
            foreach ($data as $key=>$views) {
                if (strpos($key, $currentDate)) {
                    $counter += count($views);
                }
            }
        }

        return $counter;
    }

    /**
     * Add view to Station
     *
     * @param $termID
     * @param null $userID
     *
     * @return int|bool|WP_Error
     */
    public static function updateStationView($termID, $userID = null)
    {
        $date = new DateTime();
        $currentDate = $date->format('d-m-Y');
        $userID = $userID ?: get_current_user_id();

        $data = get_term_meta($termID, 'views_data', true);
        if (isset($data[$currentDate])) {
            if (!in_array($userID, $data[$currentDate])) {
                array_push($data[$currentDate], (int)$userID);
                return update_term_meta( $termID, 'views_data', $data);
            }
        } else {
            $data[$currentDate] = [(int)$userID];
            return update_term_meta( $termID, 'views_data', $data);
        }

        return false;
    }

    /**
     * Save file in uploads directory
     *
     * @param $files
     * @return null|array
     */
    public static function saveFiles($files)
    {
        if (isset($files)) {
            $uploadDir = wp_upload_dir();

            $fileName = md5(rand(0, 9999) . $files["file"]["name"] . time()) . '_' . $files["file"]["name"];
            wp_upload_bits($fileName, null, file_get_contents($files["file"]["tmp_name"]));

            $filetype = wp_check_filetype( $fileName, null );

            $attachment = [
                'guid'           => $uploadDir['url'] . '/' . $fileName,
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', $fileName ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            ];

            $attach_id = wp_insert_attachment( $attachment, $uploadDir['path'] . '/' . $fileName, 0 );

            require_once( ABSPATH . 'wp-admin/includes/image.php' );

            $attach_data = wp_generate_attachment_metadata( $attach_id, $uploadDir['path'] . '/' . $fileName );

            wp_update_attachment_metadata( $attach_id, $attach_data );

            return [
                'id' => $attach_id,
                'url' => $uploadDir['url'] . '/' . $fileName
            ];
        }

        return null;
    }

    /**
     * Method can search by stations, playlist, artist & songs
     *
     * @param $value
     * @param array $types
     * @return array
     */
    public static function searchByProject($value, $types = ['stations', 'playlists', 'artists', 'songs'])
    {
        $result = [];

        if ($value) {
            foreach ($types as $type) {
                switch ($type) {
                    case 'stations':
                        $args = array(
                            'taxonomy' => array('radio'),
                            'orderby' => 'id',
                            'order' => 'ASC',
                            'hide_empty' => false,
                            'name__like' => $value
                        );

                        $result['stations'] = [];

                        foreach ( get_terms($args) as $station) {
                            $term_key = "term_$station->term_id";
//                            $radioType = get_field('radio_type', $term_key);

                            $station->url = get_term_link($station->term_id, $station->taxonomy);
                            $station->thumbnail = get_field('thumbnail', $term_key) ?: get_field('default_station_image', 'options');
                            $station->screensaver = get_field('screensaver', $term_key);
//                            $station->viewsData = get_term_meta($station->term_id, 'views_data', true);
//                            $station->category = RadioHelper::getStationCategory($radioType, true);

                            $result['stations'][] = $station;
                        }

                        break;
                    case 'playlists':
                        $args = array(
                            'taxonomy' => array('playlist'),
                            'orderby' => 'id',
                            'order' => 'ASC',
                            'hide_empty' => true,
                            'name__like' => $value
                        );

                        $playlists = [];

                        foreach (get_terms($args) as $playlist) {
                            $term_key = "term_$playlist->term_id";
//                            $radioType = get_field('radio_type', $term_key);

                            $playlist->url = get_term_link($playlist->term_id, $playlist->taxonomy);
                            $playlist->thumbnail = get_field('default_station_image', 'options');

                            if ($playlist->count && $playlist->parent) {
                                $playlists[] = $playlist;
                            }
                        }

                        $result['playlists'] = $playlists;

                        break;
                    case 'artists':
                        $args = array(
                            'posts_per_page' => -1,
                            'post_type' => 'song',
                            'meta_query' => array(
                                array(
                                    'key'     => 'artist',
                                    'value'   => $value,
                                    'compare' => 'LIKE',
                                ),
                            ),
                        );
                        $query = new WP_Query;
                        $songsByArtist = $query->query($args);
                        $result['artists'] = [];

                        foreach ($songsByArtist as $song) {
                            if (get_field('song_type', $song->ID) == 1) {
                                $songData = get_field('song_url', $song->ID);
                            } else {
                                $songData = get_field('song_file', $song->ID)['url'];
                            }

                            $temp = [
                                'ID' => $song->ID,
                                'post_author' => $song->post_author,
                                'post_date' => $song->post_date,
                                'post_title' => $song->post_title,
                                'song_url' => $songData,
                                'song_thumbnail' => get_the_post_thumbnail_url($song->ID),
                                'song_year' => get_field('year', $song->ID),
                                'song_artist' => get_field('artist', $song->ID),
                                'song_title' => get_field('title', $song->ID)
                            ];
                            $result['artists'][] = $temp;
                        }

                        break;
                    case 'songs':
                        $args = array(
                            'posts_per_page' => -1,
                            'post_type' => 'song',
                            's' => $value
                        );
                        $query = new WP_Query;
                        $songs = $query->query($args);

                        $result['songs'] = [];

                        foreach ($songs as $song) {
                            if (get_field('song_type', $song->ID) == 1) {
                                $songData = get_field('song_url', $song->ID);
                            } else {
                                $songData = get_field('song_file', $song->ID)['url'];
                            }

                            $temp = [
                                'ID' => $song->ID,
                                'post_author' => $song->post_author,
                                'post_date' => $song->post_date,
                                'post_title' => $song->post_title,
                                'song_url' => $songData,
                                'song_thumbnail' => get_the_post_thumbnail_url($song->ID),
                                'song_year' => get_field('year', $song->ID),
                                'song_artist' => get_field('artist', $song->ID),
                                'song_title' => get_field('title', $song->ID)
                            ];
                            $result['songs'][] = $temp;
                        }

                        break;
                }
            }
        }

        return $result;
    }

    /**
     * Get popular Stations by all time
     * and sort 10->1
     *
     * @param int $count
     * @param string $sortBy
     *      popular
     *      trending
     * @return array|int|WP_Error
     */
    public static function getSortStationsBy($count = 10 , $sortBy = 'popular')
    {
        $stations = get_terms( [
            'taxonomy' => 'radio',
            'hide_empty' => false,
        ] );

        foreach ($stations as $station) {
            $term_key = "term_$station->term_id";

            switch ($sortBy){
                case 'trending':
                    $station->views = self::getStationViewsByMonth($station->term_id);
                    break;
                case 'popular':
                default:
                    $station->views = self::getStationViews($station->term_id);
                    break;

            }

            $station->thumbnail = get_field('thumbnail', $term_key) ?: get_field('default_station_image', 'option');
        }

        usort($stations, function ($a, $b) {
            return $b->views - $a->views;
        });

        return $count ? array_slice($stations, 0, $count) : $stations;
    }

    /**
     * Get format in minutes & seconds MM:SS
     *
     * @param $seconds
     * @return string
     */
    public static function getMinutesBySeconds($seconds)
    {
        $minutes = floor($seconds/60);
        $minutes = $minutes > 10 ? $minutes : '0' . $minutes;

        $seconds = $seconds % 60;
        $seconds = $seconds > 10 ? $seconds : '0' . $seconds;

        return $minutes . ':' . $seconds;
    }

    /**
     * Generate meta key for terms
     *
     * @param $meta_key
     * @param $terms
     * @param int $default
     */
    public static function setMetaToTerm($meta_key, $terms, $default = 0)
    {
        foreach ($terms as $term) {
            $value = get_term_meta($term->term_id, $meta_key, true);

            if (!$value) {
                update_term_meta( $term->term_id, $meta_key, $default );
            }
        }
    }

    /**
     * Get Favorite playlist term_id
     *
     * @param null $user_ID
     * @return int
     */
    public static function getFavoritePlaylistID($user_ID = null)
    {
        $userID = $user_ID ?: get_current_user_id();
        $term_slug = 'favorite_playlist_' . $userID;

        $term = get_term_by('slug', $term_slug, 'favorites');

        if (!$term) {
            $term = self::addFavoritePlaylistToUser($term_slug, $userID);
        }

        return $term->term_id;
    }

    /**
     * Generate for User Favorite playlist
     *
     * @param $term_slug
     * @param $userID
     * @return array|WP_Error
     */
    public static function addFavoritePlaylistToUser($term_slug, $userID)
    {
        $userID = $userID ?: get_current_user_id();
        $term_slug = $term_slug ?: ' favorite_playlist_' . $userID;

        $term = wp_insert_term('User ID: ' . $userID . ' - Favorite playlist', 'favorites', array(
            'description' => 'All Favorites tracks for user ' . $userID,
            'parent' => 0,
            'slug' => $term_slug,
        ));

        return $term;
    }

    /**
     *
     */
    public static function getUserStations($count = 10)
    {
        $result = [];
        $playlists = get_terms( [
            'taxonomy' => 'playlist',
            'hide_empty' => true,
        ] );

        foreach ($playlists as $playlist) {
            if ($playlist->count && $playlist->parent) {
                $playlist->views = get_term_meta( $playlist->term_id, 'views_by_month', true);
                $playlist->user_id = self::getAuthorByTermID($playlist->parent);
                $result[] = $playlist;
            }

            if ($count !== -1 && (count($result) >= $count)) break;
        }

        return $result;
    }

    public static function incrementViewToType($id, $type = 'taxonomy')
    {
        $status = false;
        $key = 'views_by_month';
        $current_date = new DateTime();
        $date_format = $current_date->format('d-m-y');
        $date_format_month = $current_date->format('m-y');

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        switch ($type) {
            case 'post':
                $views_data = get_post_meta( $id, $key, true);
                break;
            case 'taxonomy':
                $views_data = get_term_meta( $id, $key, true);
                break;
            default:
                $views_data = false;
        }

        if ($views_data !== false) {
            $user_data = [
                'ip' => $ip,
                'date' => $date_format,
                'user_id' => get_current_user_id()
            ];

            !$views_data && $views_data = [];
            $views_data[$date_format_month][] = $user_data;

            switch ($type) {
                case 'post':
                    $status = update_post_meta( $id, $key, $views_data );
                    break;
                case 'taxonomy':
                    $status = update_term_meta( $id, $key, $views_data);
                    break;
            }
        }

        return $status;
    }

    public static function getAuthorByTermID($term_id, $is_only_id = true) {
        $term = get_term($term_id);
        $term_slug_explode = explode('_', $term->slug);
        $user_id = $term_slug_explode[2];

        return $is_only_id ? $user_id : get_userdata($user_id);
    }
}

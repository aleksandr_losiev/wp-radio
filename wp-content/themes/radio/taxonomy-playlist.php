<?php
/**
 * The template for displaying custom taxonomies
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#custom-taxonomies
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();

$playlist = get_queried_object();

$songs = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'song',
        'tax_query' => array(
            array(
                'taxonomy' => 'playlist',
                'field' => 'term_id',
                'terms' => $playlist->term_id,
            )
        )
    )
);

RadioHelper::incrementViewToType($playlist->term_id);

?>

    <main id="single-playlist" class="station-page" data-playlist-id="<?= $playlist->term_id ?>">

        <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
        <section class="station-container _clearfix">
            <div class="station-data">
                <div class="thumbnail">
                    <?php if (count($songs)): ?>
                        <?php for($i = 0; $i < 4 && $i < count($songs); $i++): ?>
                            <img src="<?= get_the_post_thumbnail_url($songs[$i]); ?>">
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
                <div class="description">
                    <h2><span><?= $playlist->name; ?></span><button class="_button__play"><i class="fa fa-play" aria-hidden="true"></i></button></h2>
                    <p class="type"><span><?= count($songs); ?></span> <?= count($songs) != 1 ? __('tracks', 'radio') : __('track', 'radio'); ?></p>
                    <div class="buttons">
                        <button class="button__clear edit"><i class="fa fa-pencil" aria-hidden="true"></i> <?= __('Edit', 'radio'); ?></button>
                        <button class="button__clear delete"><i class="fa fa-trash-o" aria-hidden="true"></i> <?= __('Delete', 'radio'); ?></button>
                    </div>
                    <p class="data"><?= $playlist->description; ?></p>
                </div>
            </div>
            <div class="station-songs audio-block">
                <?php foreach ($songs as $key => $song):
                    if (get_field('song_type', $song->ID) == 1) {
                        $songData = get_field('song_url', $song->ID);
                    } else {
                        $songData = get_field('song_file', $song->ID)['url'];
                    }

                    ?>
                    <figure data-track-id="<?= $song->ID ?>">
                        <div class="description">
                            <div class="index"><?= $key +1; ?></div>
                            <img class="thumbnail" src="<?= get_the_post_thumbnail_url($song->ID); ?>">
                            <div class="title-container">
                                <h5><?= get_field('artist', $song->ID); ?></h5>
                                <h6><span><?= get_field('year', $song->ID); ?></span><?= get_field('title', $song->ID); ?></h6>
                            </div>
                            <div class="song-data">
                                <audio
                                        src="<?= $songData; ?>"
                                        data-index="<?= $key; ?>"
                                        data-title="<?= $song->post_title; ?>"
                                        data-artist="<?= get_field('artist', $song->ID); ?>"
                                        data-track-name="<?= get_field('title', $song->ID); ?>"
                                        data-track-id="<?= $song->ID ?>"
                                        data-track-thumbnail="<?= get_the_post_thumbnail_url($song->ID); ?>"
                                ></audio>
                                <div class="audio-time">00:00</div>
                            </div>
                        </div>
                        <div class="song-actions">
                            <div class="play-stop">
                                <button class="play-track button__clear" data-index="<?= $key; ?>"><i class="fa fa-play" aria-hidden="true"></i></button>
                            </div>
                            <button class="add-to-playlist button__clear tooltip"
                                    data-song-id="<?= $song->ID; ?>"
                                    data-taxonomy="playlist"
                                    >
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                <?php get_template_part('template-parts/components/tooltips/add-to-playlist'); ?>
                            </button>
                            <button class="delete-track button__clear" data-song-id="<?= $song->ID; ?>" data-type="playlist">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    </figure>

                <?php endforeach; ?>

            </div>
        </section>

        <?php get_template_part('template-parts/components/share-buttons'); ?>
        <?php get_template_part('template-parts/components/audio-player'); ?>

    </main><!-- .site-main -->

<?php
get_footer();

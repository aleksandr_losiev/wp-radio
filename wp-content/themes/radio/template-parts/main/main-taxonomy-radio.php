<?php
/**
 * Part template for custom taxonomy
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#custom-taxonomies
 *
 * @var $station
 * @var $songs
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php RadioHelper::updateStationView($station->term_id); ?>

<main id="station" class="station-page" data-station-id="<?= $station->term_id; ?>">
        <section class="station-container _clearfix">
            <div class="station-data">
                <div class="thumbnail">
                    <img src="<?= get_field('thumbnail', "term_$station->term_id") ?: get_field('default_station_image', 'option'); ?>" alt="<?= $station->name; ?>">
                </div>
                <div class="description">
                    <h2><?= $station->name; ?></h2>
                    <p class="type"><?= __('Radio Station', 'radio'); ?></p>
                    <p class="data"><?= $station->description; ?></p>
                </div>
            </div>
            <div class="station-songs">
                <h4><?= __('Listen Again', 'radio'); ?></h4>
                <?php foreach ($songs as $song):
                    $song_data = get_field('song_file', $song->ID);

                    ?>
                    <figure class="hidden" data-track-id="<?= $song->ID; ?>">
                        <div class="description">
                            <div class="title-container">
                                <h5><?= get_field('artist', $song->ID); ?></h5>
                                <h6><span><?= get_field('year', $song->ID); ?></span><?= get_field('title', $song->ID); ?></h6>
                            </div>
                            <div class="song-data">
                                <div class="audio-data" data-track-id="<?= $song->ID; ?>" data-track-url="<?= $song_data['url']; ?>" data-time="<?= get_post_meta('$song->ID', 'track_time', true) ?: 0; ?>"></div>
                                <div class="audio-time"><?= RadioHelper::getMinutesBySeconds(get_post_meta($song->ID, 'track_time', true)) ?: '00:00'; ?></div>
                            </div>
                        </div>
                        <div class="song-actions">
                            <div class="play-stop">
                                <button class="add-to-playlist button__clear"><i class="fa fa-play" aria-hidden="true"></i></button>
                            </div>
                            <button class="add-to-playlist button__clear tooltip"
                                    data-song-id="<?= $song->ID; ?>"
                                    data-taxonomy="playlist"
                                    >
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <?php get_template_part('template-parts/components/tooltips/add-to-playlist'); ?>
                            </button>
                        </div>
                    </figure>

                 <?php endforeach; ?>

            </div>
        </section>
    <?php get_template_part('template-parts/components/share-buttons'); ?>
    </main><!-- .site-main -->

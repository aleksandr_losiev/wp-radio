<?php
/**
 * Part template for Stations page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#custom-taxonomies
 *
 * @var $stations
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

?>

<main id="stations-page-content" class="stations-page _clearfix">

    <h2><?php _e('Channels', 'radio'); ?></h2>
    <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
    <section class="stations-container _clearfix">
        <?php
        foreach ($stations as $station) {
            include get_template_directory() . '/template-parts/content/station-single.php';
        }
        ?>
    </section>

</main>

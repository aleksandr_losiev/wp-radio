<?php
/**
 * Sidebar with radio type list
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php

$radio_types = get_terms([
    'taxonomy' => 'radio_type',
    'hide_empty' => false,
]);

RadioHelper::setMetaToTerm('views', $radio_types);

$sidebar_menu = wp_get_nav_menu_object('Music Genres');
$sort_by_popular = get_field('music_genres', 'options');
$category = get_queried_object();
$music_genres_data = [];

if ($sort_by_popular) {

    $radio_types = get_terms([
        'taxonomy' => 'radio_type',
        'hide_empty' => false,
        'order' => 'DESC',
        'orderby' => 'meta_value_num',
        'meta_key' => 'views'
    ]);

    foreach ($radio_types as $menu_item) {
        $music_genres_data[] = [
            'title' => $menu_item->name,
            'url' => get_term_link($menu_item->term_id),
            'term_id' => $menu_item->term_id,
            'is_active' => isset($category->term_id) && ($category->term_id == $menu_item->term_id)
        ];
    }

} else if ($sidebar_menu) {
    foreach (wp_get_nav_menu_items($sidebar_menu->term_id) as $menu_item) {
        $term_data = get_term_by('name', $menu_item->title, $menu_item->object);
        $music_genres_data[] = [
            'title' => $menu_item->title,
            'url' => $menu_item->url,
            'term_id' => $term_data->term_id,
            'is_active' => isset($category->term_id) && ($category->term_id == $term_data->term_id)
        ];
    }
} else {
    foreach ($radio_types as $menu_item) {
        $music_genres_data[] = [
            'title' => $menu_item->name,
            'url' => get_term_link($menu_item->term_id),
            'term_id' => $menu_item->term_id,
            'is_active' => isset($category->term_id) && ($category->term_id == $menu_item->term_id)
        ];
    }
}
?>

<aside class="sidebar sidebar-radio-type">
    <h4><?= __('MUSIC GENRES', 'radio') ?></h4>
    <ul class="types-list">
        <?php foreach ($music_genres_data as $menu_item): ?>
            <li data-views="<?= get_term_meta($menu_item['term_id'], 'views', true); ?>">
                <a href="<?= $menu_item['url']; ?>" class="<?= $menu_item['is_active'] ? 'active' : ''; ?>">
                    <?= $menu_item['title']; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</aside>

<?php
/**
 * Display the footer for page if user not auth
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<div class="footer-home">
    <section class="footer-data">
        <div class="footer-navigation _col _col__1-3">
            <?php wp_nav_menu( [
                'container' => 'nav',
                'container_class' => 'footer-menu-navigation',
                'theme_location'  => 'footer'
            ] ); ?>
        </div>

        <aside class="footer-widget-1 _col _col__1-3 _text__center">
            <?php
            if ( is_active_sidebar( 'footer-1' ) ) {
                dynamic_sidebar( 'footer-1' );
            }
            ?>
        </aside>

        <div class="social-navigation _col _col__1-3">
            <div class="follow"><?= __('Follow us', 'radio'); ?></div>
            <?= RadioHelper::getSocialList(); ?>
        </div>
    </section>
</div>

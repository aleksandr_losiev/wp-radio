<?php
/**
 * Display the footer for page if user auth
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<div class="footer-default">
    <section class="footer-data">
        <aside class="footer-widget-1 _col _col__1-3">
            <?php
            if ( is_active_sidebar( 'footer-1' ) ) {
                dynamic_sidebar( 'footer-1' );
            }
            ?>
        </aside>

        <div class="footer-navigation _col _col__1-3">
            <?php wp_nav_menu( [
                'container' => 'nav',
                'container_class' => 'footer-menu-navigation',
                'theme_location'  => 'footer'
            ] ); ?>
        </div>

        <div class="social-navigation _col _col__1-3">
            <div class="follow"><?= __('Follow us', 'radio'); ?></div>
            <?= RadioHelper::getSocialList(); ?>
        </div>
    </section>
    <section class="copyright _text__center">
        <?php
        if ( is_active_sidebar( 'footer-2' ) ) {
            dynamic_sidebar( 'footer-2' );
        }
        ?>
    </section>
</div>

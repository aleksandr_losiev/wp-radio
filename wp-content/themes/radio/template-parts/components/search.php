<?php
/**
 * Search by site
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php
    $search = isset($_GET['search']) ? $_GET['search'] : ''
?>

<div id="search">
    <input type="text" placeholder="Search" class="search-sidebar" value="<?= $search; ?>" />
    <div class="search-result">
        <ul class="search-result-data"></ul>
    </div>
</div>

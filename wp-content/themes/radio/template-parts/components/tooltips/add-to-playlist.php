<?php
/**
 * Tooltip - Add to Playlist
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 *
 *  */

?>

<?php
$taxData = get_queried_object();
?>

<span class="tooltip-content tooltip-content__medium tooltip-content__bottom">
    <a
        class="edit-playlist open-modal"
        data-modal-id="playlist-add"
    >
        <?= __('Create New Playlist', 'radio'); ?>
        <i class="fa fa-plus-circle" aria-hidden="true"></i>
    </a>
    <?php if (is_tax() && false): ?>
        <a
                class="add-to-playlist"
                data-taxonomy="favorites"
                data-playlist-id="<?= RadioHelper::getFavoritePlaylistID() ?>"
        >
        <?= __('Add to favorites', 'radio'); ?>
            <i class="fa fa-star" aria-hidden="true"></i>
    </a>
    <?php endif; ?>
    <?php foreach (RadioHelper::getUserPlaylists() as $playlist): ?>
        <?php if ($taxData->taxonomy === 'playlist' && $taxData->term_id === $playlist->term_id): ?>
        <?php else: ?>
            <a
                    class="add-to-playlist"
                    data-taxonomy="playlist"
                    data-playlist-id="<?= $playlist->term_id; ?>"
            >
                <?= $playlist->name; ?>
            </a>
        <?php endif; ?>
    <?php endforeach; ?>
</span>

<?php
/**
 * Share buttons
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php

    if (is_tax()) {
        $playlist = get_queried_object();
        $url = urlencode(get_term_link($playlist->term_id, $playlist->taxonomy));
        $title = urlencode(html_entity_decode($playlist->name, ENT_COMPAT, 'UTF-8'));
        $media = urlencode(get_field('thumbnail', "term_$playlist->term_id") ?: get_field('default_station_image', 'option'));
    } else {
        $url = urlencode(get_the_permalink());
        $title = urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));
        $media = urlencode(get_the_post_thumbnail_url(get_the_ID(), 'full'));
    }

    $socials = [
        [
            'class' => 'share-twitter',
            'url'   => "https://twitter.com/intent/tweet?text=$title&amp;url=$url&amp;via=WPCrumbs",
            'icon'  => 'fa-twitter',
        ],
        [
            'class' => 'share-facebook',
            'url'   => "https://www.facebook.com/sharer/sharer.php?u=$url",
            'icon'  => 'fa-facebook',
        ],
        [
            'class' => 'share-googleplus',
            'url'   => "https://plus.google.com/share?url=$url",
            'icon'  => 'fa-google',
        ],
        [
            'class' => 'share-pinterest',
            'url'   => "http://pinterest.com/pin/create/button/?url=$url&amp;media=$media&amp;description=$title",
            'icon'  => 'fa-pinterest',
        ],
        [
            'class' => 'share-whatsapp',
            'url'   => "whatsapp://send?text=$title $url",
            'icon'  => 'fa-whatsapp',
        ]
    ]
?>

<section id="share-buttons">
    <ul class="share-buttons">
        <?php foreach ($socials as $social): ?>
            <li>
                <a class="<?= $social['class']; ?>" href="<?= $social['url']; ?>" target="_blank">
                    <i class="fa <?= $social['icon'] ?>" aria-hidden="true"></i>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</section>

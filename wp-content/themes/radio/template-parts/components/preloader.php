<?php
/**
 * Preloader
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<section id="preloader">
    <div class="caption">
        <div class="cube-loader">
            <div class="cube loader-1"></div>
            <div class="cube loader-2"></div>
            <div class="cube loader-4"></div>
            <div class="cube loader-3"></div>
        </div>
    </div>
</section>
<?php
/**
 * Audio Player
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

?>

<section id="audio-player" class="hidden">
     <div class="description">
         <img class="thumbnail" src="<?= get_field('default_station_image', 'option'); ?>">
         <div class="song-data">
             <h5></h5>
             <h6></h6>
         </div>
     </div>
     <div class="buttons-main" data-status-hidden="pause">
         <button class="button__clear" id="repeat-track"><i class="fa fa-repeat" aria-hidden="true"></i></button>
         <button class="button__clear" id="prev-track"><i class="fa fa-step-backward" aria-hidden="true"></i></button>
         <button class="button__clear active" id="play-track"><i class="fa fa-play" aria-hidden="true"></i></button>
         <button class="button__clear active" id="pause-track"><i class="fa fa-pause" aria-hidden="true"></i></button>
         <button class="button__clear" id="next-track"><i class="fa fa-step-forward" aria-hidden="true"></i></button>
         <button class="button__clear" id="random-track"><i class="fa fa-random" aria-hidden="true"></i></button>
     </div>
     <div class="time-line">
         <input
                 type="range"
                 min="0"
                 max="300"
                 value="0"
                 step="1"
                 class="audio-duration"
         >
         <div class="audio-time">
             <span class="current"></span>
             <div class="end"></div>
         </div>
     </div>
     <div class="helper-buttons">
         <button class="player-volume button__clear">
             <i class="fa fa-volume-up" aria-hidden="true"></i>
             <input type="range"
                    min="0"
                    max="100"
                    value="80"
                    step="1"
                    class="player-volume-range"
             >
         </button>
         <button class="button__clear tooltip add-to-playlist" data-song-id="0">
             <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
             <?php get_template_part('template-parts/components/tooltips/add-to-playlist'); ?>
         </button>
     </div>
</section>
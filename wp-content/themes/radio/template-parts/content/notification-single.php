<?php
/**
 * Single Notifications
 *
 *This notification may be as successful, attention and with an error.
 *
 * @var $type (string)
 * @var $message (string)
 * @var $title (string)
 * @var $key (sting)
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article class="single-notification <?= $type; ?>" <?= $key ? 'data-key=' . $key : ''; ?>>
    <h6><?= $title; ?></h6>
    <p><?= $message; ?></p>
    <button class="close-notification"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
</article>

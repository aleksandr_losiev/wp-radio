<?php
/**
 * Single station template for archive
 *
 * @var $station (object)
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php
    $term_key = "term_$station->term_id";
    $radioType =  get_field('radio_type', $term_key);
    $songs = get_field('songs', $term_key) ?: [];
    $artists = [];

    foreach ($songs as $song) {
        $artist = get_field('artist', $song->ID);
        !in_array($artist, $artists) && array_push($artists, $artist);
    }

?>

<article class="single-station" data-name="<?= $station->name; ?>" data-id="<?= $station->term_id; ?>" data-artists="<?= implode(', ', $artists); ?>">
    <div data-station_id="<?= $station->term_id; ?>" class="station-data-container" data-link="<?= get_term_link($station->term_id); ?>">
        <div class="image-block">
            <a href="<?= get_term_link($station->term_id); ?>">
                <img src="<?= get_field('thumbnail', $term_key) ?: get_field('default_station_image', 'option'); ?>" alt="<?= $station->name ?>">
            </a>
        </div>
        <a href="<?= get_term_link($station->term_id); ?>"><h5><?= $station->name; ?></h5></a>
        <?= RadioHelper::getStationCategory($radioType); ?>
    </div>
</article>

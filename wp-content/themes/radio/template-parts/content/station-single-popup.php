<?php
/**
 * Single station popup template
 *
 * @var $id_station (number)
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php
    $songs = get_field('songs', "term_$id_station");
    $time = [
        'current' => time(),
        'last_update' => get_term_meta( $id_station, 'update_timestamp', true ) ?: time()
    ];

    RadioHelper::incrementViewToType($id_station);
?>

<section class="station-popup compress">
    <div class="main-image">
        <img src="<?= get_field('screensaver', "term_$id_station") ?: get_field('default_screensaver', 'option'); ?>">
    </div>
    <div class="song-data" id="player">
        <div class="data">
            <div class="thumbnail">
                <img src="<?= get_field('thumbnail', "term_$id_station") ?: get_field('default_station_image', 'option'); ?>">
            </div>
            <div class="description" data-active-track-id="0">
                <h5></h5>
                <h6></h6>
            </div>
        </div>
        <div class="buttons-main" data-status="stop">
            <button class="player-stop-radio"><i class="fa fa-stop" aria-hidden="true"></i></button>
            <button class="player-play-radio"><i class="fa fa-play" aria-hidden="true"></i></button>
            <button class="button__clear" id="next-track"><i class="fa fa-step-forward" aria-hidden="true"></i></button>
        </div>
        <div class="duration">
            <div class="audio-block">
                <?php
                    foreach ($songs as $song):

                        switch (get_field('song_type', $song->ID)) {
                            case 1:
                                $songURL = get_field('song_url', $song->ID);
                                break;
                            case 2:
                                $songURL = get_field('song_file', $song->ID)['url'];
                        }
                ?>
                    <?php if (isset($songURL) && $song->ID): ?>
                        <div class="audio-track"
                                data-title="<?= $song->post_title; ?>"
                                src="<?= $songURL; ?>"
                                data-artist="<?= get_field('artist', $song->ID); ?>"
                                data-track-name="<?= get_field('title', $song->ID); ?>"
                                data-track-id="<?= $song->ID ?>"
                                data-track-thumbnail="<?= get_the_post_thumbnail_url($song->ID); ?>"
                                data-track-duration="<?= get_post_meta($song->ID, 'track_time', true) ?>"
                        ></div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="audio-status">
                <div class="title">Live</div>
                <div
                        class="audio-duration"
                        data-time-update="<?= $time['last_update']; ?>"
                        data-time-current="<?= $time['current']; ?>"
                ></div>
            </div>
        </div>
        <div class="buttons-helper" data-compress="expand">
            <button class="player-volume">
                <i class="fa fa-volume-up" aria-hidden="true"></i>
                <input type="range"
                       min="0"
                       max="100"
                       value="80"
                       step="1"
                       class="player-volume-range"
                >
            </button>
            <button class="tooltip">
                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                <?php get_template_part('template-parts/components/tooltips/add-to-playlist'); ?>
            </button>
<!--            <button class="compress-station-popup"><i class="fa fa-compress" aria-hidden="true"></i></button>-->
<!--            <button class="expand-station-popup"><i class="fa fa-expand" aria-hidden="true"></i></button>-->
            <button class="close-station-popup"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
        </div>
    </div>

</section>

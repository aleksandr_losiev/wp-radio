<?php
/**
 * Popup Sign In
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="sign-in" class="small bg__default">
    <div class="popup-data">
        <h4><?= __('Sign in', 'radio'); ?></h4>
        <form name="loginform" id="loginform" action="/wp-login.php" method="post">

            <p class="login-username">
                <label for="user_login"><?= __('Email or Phone Number', 'radio'); ?></label>

                <input type="text" name="log" id="user_login" class="input" value="" placeholder="your.email@gmail.com" />
            </p>
            <p class="login-password">
                <label for="user_pass"><?= __('Password', 'radio'); ?></label>
                <input type="password" name="pwd" id="user_pass" class="input" value="" placeholder="<?= __('Password', 'radio'); ?>" />
            </p>

            <p class="forgot-password"><a href="/"><?= __('Forgot Password?', 'radio'); ?></a></p>

            <p class="login-submit">
                <button type="submit" name="wp-submit" id="wp-submit" class="_button _button__green _button__normal _button__center _button__370 _button_ajax"><?= __('Sign in', 'radio') ?></button>
                <input type="hidden" name="redirect_to" value="/" />
            </p>

            <div class="remember-help">
                <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /><?= __('Remember me', 'radio'); ?></label></p>
                <p class="need-help"><a href="/"><?= __('Need Help?', 'radio'); ?>  </a></p>
            </div>

        </form>
        <p class="sign-up">
            <?= __('New to Cherry Pop Radio?', 'radio'); ?>
            <button class="_button__link open-modal" data-modal-id="sign-up"><?= __('Sign Up', 'radio'); ?></button>
        </p>
    </div>
    <?php get_template_part('template-parts/popup/popup-parts/popup', 'close'); ?>
</article>

<?php
/**
 * Popup Playlist
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="playlist-add" class="small bg__content">
    <div class="popup-data">
        <h5><?= __('Add playlist', 'radio'); ?></h5>
        <form id="add-playlist">
            <div class="form-field">
                <label><?= __('Playlist name', 'radio'); ?></label>
                <input type="text" class="playlist-title" placeholder="<?= __('My playlist', 'radio'); ?>"/>
            </div>
            <div class="form-field">
                <label><?= __('Description', 'radio'); ?></label>
                <textarea class="playlist-description" placeholder="<?= __('Playlist Description', 'radio'); ?>"></textarea>
            </div>
            <div class="buttons">
                <button class="button__cancel close-modal"><?= __('Cancel', 'radio'); ?></button>
                <button class="button__save save-playlist"><?= __('Save', 'radio'); ?></button>
            </div>
        </form>
    </div>
</article>

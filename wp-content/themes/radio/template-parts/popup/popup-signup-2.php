<?php
/**
 * Popup Sign Up - list
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<?php
global $wpdb, $pmpro_msg, $pmpro_msgt, $current_user;

$pmpro_levels = pmpro_getAllLevels(false, true);
$pmpro_level_order = pmpro_getOption('level_order');

?>

<article id="sign-up-2" class="max bg__default">
    <div class="popup-data">
        <h4><?= __('Sign Up', 'radio'); ?><span class="step">2/<i>3</i></span></h4>
        <form name="list-price" id="list-price" method="post">
            <?php foreach ($pmpro_levels as $level): ?>
                <div class="price-data">
                    <div class="title">
                        <div class="input">
                            <input
                                    type="radio"
                                    id="level-<?= $level->id; ?>"
                                    name="level"
                                    value="<?= $level->id; ?>"
                                    data-name="<?= $level->name ?>"
                                    data-price="<?= $level->initial_payment; ?>"
                                    data-value="<?= pmpro_formatPrice( $level->initial_payment ); ?>"
                            >
                        </div>
                        <div class="title-data">
                            <h5><?= $level->name; ?></h5>
                            <?php if ($price = $level->initial_payment): ?>
                                <p class="price"><?= pmpro_formatPrice( $price ); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="description"><?= $level->description ?: '' ; ?></div>
                </div>
            <?php endforeach; ?>
            <p class="register-submit">
                <input type="submit" name="register-submit-2" id="register-submit-2" class="_button _button__green _button__normal _button__center _button__370 _button__disabled" value="<?= __('Choose plan', 'radio') ?>" />
            </p>
        </form>
    </div>
    <?php get_template_part('template-parts/popup/popup-parts/popup', 'close'); ?>
</article>

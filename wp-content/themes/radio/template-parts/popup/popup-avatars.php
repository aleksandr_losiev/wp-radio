<?php
/**
 * Popup Avatars
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="popup-avatars" class="max bg__content">
    <div class="popup-data">
        <h5><?= __('Change avatar', 'radio'); ?></h5>
        <form id="change-avatar">
            <?php if (get_field('user_avatars', 'options')): ?>
                <?php foreach (get_field('user_avatars', 'options') as $avatar): ?>
                    <div class="form-field">
                        <input id="avatar-id-<?= $avatar['ID']; ?>" type="radio" name="change-avatar" value="<?= $avatar['ID']; ?>">
                        <label for="avatar-id-<?= $avatar['ID']; ?>">
                            <span><i class="fa fa-check-circle-o" aria-hidden="true"></i></span>
                            <img src="<?= $avatar['url']; ?>">
                        </label>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </form>
        <div class="buttons">
            <button class="button__cancel close-modal"><?= __('Cancel', 'radio'); ?></button>
            <button id="set-user-avatar" class="button__save"><?= __('Save', 'radio'); ?></button>
        </div>
    </div>
</article>
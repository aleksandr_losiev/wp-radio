<?php
/**
 * Popup Sign Up
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="sign-up" class="small bg__default">
    <div class="popup-data">
        <h4><?= __('Sign Up', 'radio'); ?><span class="step">1/<i>3</i></span></h4>
        <form name="register-form" id="register-form" method="post">

            <p class="register-username form-row">
                <label for="register_login"><?= __('Email or Phone Number', 'radio'); ?></label>

                <input type="text" name="register_login" id="register_login" class="input" value="" placeholder="your.email@gmail.com" />
            </p>

            <p class="register-password form-row">
                <label for="register_pass"><?= __('Password', 'radio'); ?></label>
                <input type="password" name="pass" id="register_pass" class="input" value="" placeholder="<?= __('Password', 'radio'); ?>" />
            </p>

            <p class="register-password-again form-row">
                <label for="register_pass_again"><?= __('Password Again', 'radio'); ?></label>
                <input type="password" name="pass-again" id="register_pass_again" class="input" value="" placeholder="<?= __('Password', 'radio'); ?>" />
            </p>

            <p class="register-submit">
                <input type="submit" name="register-submit" id="register-submit" class="_button _button__green _button__normal _button__center _button__370" value="<?= __('See the plans', 'radio') ?>" />
            </p>
        </form>
        <div class="register-description">
            <p><i class="fa fa-check" aria-hidden="true"></i><?= __('We\'ll remind you three days before your trial ends', 'radio'); ?></p>
            <p><i class="fa fa-check" aria-hidden="true"></i><?= __('No commitments, cancel anytime', 'radio'); ?></p>
        </div>
        <div class="errors"></div>
    </div>
    <?php get_template_part('template-parts/popup/popup-parts/popup', 'close'); ?>
</article>

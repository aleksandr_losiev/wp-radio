<?php
/**
 * Popup Playlist
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="playlist-delete" class="small bg__content">
    <div class="popup-data">
        <h5></h5>
        <form id="delete-playlist">
            <input type="hidden" name="playlist-id">
            <div class="buttons">
                <button class="button__save remove-playlist"><?= __('Yes', 'radio'); ?></button>
                <button class="button__cancel close-modal"><?= __('No', 'radio'); ?></button>
            </div>
        </form>
    </div>
</article>

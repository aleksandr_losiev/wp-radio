<?php
/**
 * Popup - main template for popups
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<section id="popup" class="popup">
    <div class="popup-container">
        <?php
            if (!is_user_logged_in()) {
                get_template_part('template-parts/popup/popup', 'signin');
                get_template_part('template-parts/popup/popup', 'signup');
                get_template_part('template-parts/popup/popup', 'signup-2');
            } else {
                if (is_page_template('templates/acount.php')) {
                    get_template_part('template-parts/popup/popup', 'avatars');
                    get_template_part('template-parts/popup/popup', 'change-plan');
                }
                get_template_part('template-parts/popup/popup', 'search');
            }
        ?>
        <?php  ?>
        <?php  ?>
        <?php ?>
        <?php get_template_part('template-parts/popup/popup', 'signup-3'); ?>
        <?php get_template_part('template-parts/popup/popup', 'playlist'); ?>
        <?php get_template_part('template-parts/popup/popup', 'playlist-edit'); ?>
        <?php get_template_part('template-parts/popup/popup', 'playlist-delete'); ?>
        <?php get_template_part('template-parts/popup/popup', 'track-delete'); ?>
        <?php
//            var_dump(get_page_template_slug( get_the_ID() ));
        ?>
    </div>
</section>

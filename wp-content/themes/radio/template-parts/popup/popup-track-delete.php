<?php
/**
 * Popup for delete track in Playlist page
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="track-delete" class="small bg__content">
    <div class="popup-data">
        <h6></h6>
        <form id="delete-track">
            <input type="hidden" name="track-id">
            <input type="hidden" name="playlist-id">
            <div class="buttons">
                <button class="button__save remove-track"><?= __('Yes', 'radio'); ?></button>
                <button class="button__cancel close-modal"><?= __('No', 'radio'); ?></button>
            </div>
        </form>
    </div>
</article>

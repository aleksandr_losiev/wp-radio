<?php
/**
 * Popup parts - Close popup
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<div class="close close-modal"><i class="fa fa-times" aria-hidden="true"></i></div>
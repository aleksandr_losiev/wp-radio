<?php
/**
 * Popup Search
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

<article id="popup-search">
    <div class="popup-data">
        <div class="channels block">
            <h5><?= __('Channels', 'radio'); ?></h5>
            <div class="data-list"></div>
            <div class="more" data-class="channels"><?= __('More', 'radio'); ?></div>
        </div>
        <div class="playlists block">
            <h5><?= __('Playlists', 'radio'); ?></h5>
            <div class="data-list"></div>
            <div class="more" data-class="playlists"><?= __('More', 'radio'); ?></div>
        </div>
        <div class="artists block">
            <h5><?= __('Songs by Artist', 'radio'); ?></h5>
            <div class="data-list"></div>
            <div class="more" data-class="artists"><?= __('More', 'radio'); ?></div>
        </div>
        <div class="songs block">
            <h5><?= __('Songs', 'radio'); ?></h5>
            <div class="data-list"></div>
            <div class="more" data-class="songs"><?= __('More', 'radio'); ?></div>
        </div>
    </div>
</article>
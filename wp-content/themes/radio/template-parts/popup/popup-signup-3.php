<?php
/**
 * Popup Sign Up - Payment (step 3)
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */
?>

 <?php
	global $gateway, $pmpro_paypal_token, $wpdb, $current_user, $pmpro_level, $pmpro_levels, $pmpro_error_fields;
	global $bemail, $bconfirmemail, $username, $password, $password2, $CardType, $AccountNumber, $ExpirationMonth,$ExpirationYear;

//    do_action('pmpro_checkout_boxes');
?>

<article id="sign-up-3" class="medium bg__default">
    <div class="popup-data">
        <h4><?= __('Payment', 'radio'); ?><span class="step">3/<i>3</i></span></h4>
        <form name="payment-form" id="payment-form" method="post" class="tabs-container">

            <div class="pay-type">
                <div class="tab active" data-tab="debit-credit">
                    <div class="icon"><img src="<?= ASSETS . '/img/icons/credit_card.svg'; ?>"></div>
                    <div class="title"><?= __('DEBIT / CREDIT CARD', 'radio'); ?></div>
                </div>
                <div class="tab" data-tab="pay-pal">
                    <div class="icon"><img src="<?= ASSETS . '/img/icons/pay_pal.svg'; ?>"></div>
                    <div class="title"><?= __('PayPal', 'radio'); ?></div>
                </div>
            </div>

            <div class="pay-data">
                <div class="tab-data active" data-tab-name="debit-credit">
                    <div class="form-field card-icon">
                        <label for="payment-card"><?= __('Credit Card Number', 'radio'); ?></label>
                        <input type="text" id="payment-card" name="payment-card" placeholder="XXXX XXXX XXXX XXXX">
                    </div>
                    <div class="form-field">
                        <label for="payment-name"><?= __('Name on Card', 'radio'); ?></label>
                        <input type="text" id="payment-name" name="payment-name" placeholder="Your name">
                    </div>
                    <div class="row">
                        <div class="_col__1-2">
                            <div class="form-field">
                                <label for="payment-date"><?= __('Expiry Date', 'radio'); ?></label>
                                <input type="text" id="payment-date" name="payment-date" placeholder="MM / YY">
                            </div>
                        </div>
                        <div class="_col__1-2">
                            <div class="form-field card-icon">
                                <label for="payment-ccv"><?= __('CCV Code', 'radio'); ?></label>
                                <input type="text" id="payment-ccv" name="payment-ccv" placeholder="CVV">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="plan">
                <div class="name"><span>Standart</span>&nbsp;<?= __('Plan', 'radio'); ?></div>
                <div class="value">$0.99</div>
            </div>
        </form>
        <form id="register-payment" action="" method="post">
            <input type="hidden" name="username" value="">
            <input type="hidden" name="password" value="">
            <input type="hidden" name="password2" value="">
            <input type="hidden" name="bemail" value="">
            <input type="hidden" name="bconfirmemail" value="">

            <input type="hidden" name="AccountNumber" value="">
            <input type="hidden" name="ExpirationMonth" value="">
            <input type="hidden" name="ExpirationYear" value="">
            <input type="hidden" name="CVV" value="">

            <input type="hidden" name="level-name" value="">
            <input type="hidden" name="level-price" value="">
            <input type="hidden" name="level-id" value="">
            <input type="hidden" name="url" value="<?= pmpro_url("checkout", "?level="); ?>">

            <?php PMProHelper::payActions(); ?>
            <p class="register-submit">
                <input type="submit" name="payment-submit" id="payment-submit" class="_button _button__green _button__normal _button__center _button__370" value="<?= __('Pay', 'radio') ?>" />
            </p>
        </form>
    </div>
    <?php get_template_part('template-parts/popup/popup-parts/popup', 'close'); ?>
</article>

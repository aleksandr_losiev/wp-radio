<?php
/**
 * Home page for user authorized
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */
?>

<?php
    $stations_count_sliders = get_field('count_stations_in_slider') ?: 10;
    $stations_trending = RadioHelper::getSortStationsBy($stations_count_sliders, 'trending');
    $stations_popular = RadioHelper::getSortStationsBy($stations_count_sliders);
    $user_stations = RadioHelper::getUserStations($stations_count_sliders);
    $background_img = get_field('main_screen_thumbnail');
    $background_video_src = null;
?>

<section class="main-screen _d__flex _a-i__center" <?= $background_img ? "style='background-image: url($background_img)'" : '' ?>>
    <?php if (have_rows('background_video', 'option')): ?>
        <video autoplay muted loop id="myVideo" poster="<?= get_field('baground_video_image', 'option') ?>">
            <?php while( have_rows('background_video', 'option') ): the_row(); ?>
                <source src="<?php the_sub_field('file'); ?>" type="<?php the_sub_field('type'); ?>">
            <?php endwhile; ?>
        </video>
    <?php endif; ?>
    <div class="_container">
        <div class="box">
            <h2><?= get_field('main_screen_title'); ?></h2>
            <h4><?= get_field('main_screen_subtitle'); ?></h4>
        </div>
        <div class="box">
            <?= get_field('main_screen_description'); ?>
        </div>
    </div>
    <div class="mouse"><img src="<?= ASSETS . '/img/icons/mouse.svg'; ?>" alt="mouse-icon"></div>
</section>

<section class="stations-trending stations-list _clearfix">
    <div class="_container">
        <h4>
            <?php _e('Trending now on', 'radio'); ?>&nbsp;<?php bloginfo('name'); ?>
            <a href="/" class="_button__more"><?= __('View all', 'radio'); ?></a>
        </h4>
    </div>
    <div class="carousel-main-page owl-slider owl-loaded">
        <?php foreach ($stations_trending as $station): ?>
            <div class="owl-station">
                <?php include get_template_directory() . '/template-parts/content/station-single.php'; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="stations-popular stations-list _clearfix">
    <div class="_container">
        <h4>
            <?php _e('Popular on', 'radio'); ?>&nbsp;<?php bloginfo('name'); ?>
            <a href="/" class="_button__more"><?= __('View all', 'radio'); ?></a>
        </h4>
    </div>
    <div class="carousel-main-page owl-slider owl-loaded">
        <?php foreach ($stations_popular as $station): ?>
            <div class="owl-station">
                <?php include get_template_directory() . '/template-parts/content/station-single.php'; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="stations-popular stations-list _clearfix">
    <div class="_container">
        <h4>
            <?php _e('Member Vibes Channels', 'radio'); ?>
            <a href="/" class="_button__more"><?= __('View all', 'radio'); ?></a>
        </h4>
    </div>
    <div class="carousel-main-page owl-slider owl-loaded">
        <?php foreach ($user_stations as $station): ?>

            <div class="owl-station">
                <?php include get_template_directory() . '/template-parts/content/station-single.php'; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

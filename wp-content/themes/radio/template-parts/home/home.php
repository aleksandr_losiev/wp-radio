<?php
/**
 * Home page for user not authorized
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

?>

<section class="home-container">
    <h3 class="_text__center"><?= __('Need music?', 'radio'); ?></h3>
    <p class="_text__center"><?= __('We got you!', 'radio'); ?></p>
    <form id="main-search-form">
        <div class="field _d__flex">
            <input id="main-search" type="text" placeholder="<?= __('Search for music, genre, stations...'); ?>">
            <label for="main-search" class="_d__flex _j-c__center _a-i__center"><i class="fa fa-search" aria-hidden="true"></i></label>
        </div>
    </form>
    <div class="sign-up-data">
        <p><?= __('Listen anywhere', 'radio'); ?></p>
        <button class="_button _button__green _button__normal open-modal" data-modal-id="sign-up"><?= __('Sign up', 'radio'); ?></button>
        <p><?= __('for free', 'radio'); ?></p>
    </div>
</section>

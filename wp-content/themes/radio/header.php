<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php get_template_part('template-parts/components/preloader'); ?>
<div id="page" class="site">

		<header>
            <div class="_container">
                <div class="header-container">
                    <div class="navigation">
                        <a class="logo">
                            <img src="<?= wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0]; ?>">
                        </a>
                        <?php wp_nav_menu( [
                            'container' => 'nav',
                            'container_class' => 'menu-header',
                            'theme_location'  => 'main'
                        ] ); ?>
                        <?php get_template_part('template-parts/components/search'); ?>
                    </div>
                    <div class="user-container">
                        <?php if(is_user_logged_in()): ?>
                            <div class="user-menu hidden">
                                <img class="user-avatar" src="<?= get_field('user_avatar', 'user_' . get_current_user_id()) ?: get_field('default_user_avatar', 'option'); ?>">
                                <ul class="menu-list">
                                    <?php $user_menu = wp_get_nav_menu_object(140 ); ?>
                                    <?php if ($user_menu): ?>
                                        <?php foreach (wp_get_nav_menu_items($user_menu->term_id ) as $menu_item): ?>
                                            <li><a href="<?= $menu_item->url; ?>"><?= $menu_item->title; ?></a></li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                   <li><a href="<?= wp_logout_url( home_url() ); ?>"><?= __('Log Out', 'radio'); ?></a></li>
                                </ul>
                            </div>
                        <?php else: ?>
                            <button class="_button _button__green _button__small open-modal" data-modal-id="sign-in"><?php _e('Sign in', 'radio') ?></button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
		</header>

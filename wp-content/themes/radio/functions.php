<?php
/**
 * Radio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

define('ASSETS', get_template_directory_uri() . '/assets');

if (!function_exists('radio_setup')) {
function radio_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         */
        load_theme_textdomain('radio', get_template_directory() . '/languages');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(600, 600);

        /*
         * Add default posts and comments RSS feed links to head.
         */
        add_theme_support('automatic-feed-links');

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(
            array(
                'main' => __('Primary', 'radio'),
                'footer'  => __('Footer Menu', 'radio'),
                'social'  => __('Social Links Menu', 'radio'),
                'user'  => __('User Menu', 'radio'),
                'sidebar' => __('Music Genres'),
                'dashboard' => __('Dashboard', 'radio'),
            )
        );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /*
         * Add support for core custom logo.
         */
        add_theme_support(
            'custom-logo',
            array(
                'height' => 90,
                'width' => 105,
                'flex-width' => false,
                'flex-height' => false,
            )
        );
    }

    add_action('after_setup_theme', 'radio_setup');
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function radio_widgets_init()
{

    register_sidebar(
        array(
            'name' => __('Footer', 'radio'),
            'id' => 'footer-1',
            'description' => __('Add widgets here to appear in your footer.', 'radio'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        )
    );

    register_sidebar(
        array(
            'name' => __('Footer Copyright', 'radio'),
            'id' => 'footer-2',
            'description' => __('Add widgets here to appear after your footer.', 'radio'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h4 class="widget-title">',
            'after_title' => '</h4>',
        )
    );

}

add_action('widgets_init', 'radio_widgets_init');


/**
 * Enqueue scripts and styles.
 */
function radio_scripts()
{
    wp_enqueue_style('radio-description', get_stylesheet_uri(), array(), wp_get_theme()->get('Version'));
    wp_enqueue_style('radio-style', ASSETS . '/css/style.css', array(), wp_get_theme()->get('Version'));
    wp_enqueue_style('radio-style', ASSETS . '/css/owl.carousel.min.css', array(), '2.3.4');
    wp_enqueue_style('radio-style', ASSETS . '/css/owl.theme.default.min.css', array(), '2.3.4');
    wp_enqueue_style('font-awesome', ASSETS . '/css/font-awesome.css', array(), '4.7');
    wp_enqueue_style('table', ASSETS . '/css/footable.standalone.min.css', array(), '1.0');

    wp_enqueue_script('owl', get_theme_file_uri('/assets/js/libs/owl.carousel.min.js'), array('jquery'), '2.3.4', true);
    wp_enqueue_script('validate', get_theme_file_uri('/assets/js/libs/validate.min.js'), array(), '0.13.1', true);
    wp_enqueue_script('imask', get_theme_file_uri('/assets/js/libs/imask.js'), array(), '1.1.1', true);
    wp_enqueue_script('popup', get_theme_file_uri('/assets/js/class/Popup.js'), array(), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('playlist', get_theme_file_uri('/assets/js/class/Playlist.js'), array(), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('audio', get_theme_file_uri('/assets/js/class/Audio.js'), array(), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('routing', get_theme_file_uri('/assets/js/class/Routing.js'), array(), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('helper', get_theme_file_uri('/assets/js/class/Helper.js'), array('validate'), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('ajax', get_theme_file_uri('/assets/js/class/Ajax.js'), array('jquery'), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('table', get_theme_file_uri('/assets/js/libs/footable.js'), array('jquery'), wp_get_theme()->get('Version'), true);
    wp_enqueue_script('script', get_theme_file_uri('/assets/js/script.js'), array('owl', 'popup', 'table'), wp_get_theme()->get('Version'), true);

}

add_action('wp_enqueue_scripts', 'radio_scripts');

function import_songs()
{
	echo "----------";
	$station_id=$_POST['station_id'];
	$station_name=$_POST['station_name'];

	global $wpdb;
	$sql = "SELECT * FROM tbl_songs WHERE station='$station_name'";//1=1 LIMIT 0, 10";//UPDATE tablename SET column1='testdata' WHERE id=1";
	$results = $wpdb->get_results($sql);

	$songs = [];
	foreach ($results as $row) {
		$post_arr = array(
                'post_title'   => $row->title,
                'post_status'  => 'publish',
                'post_type'    => 'song',
                'meta_input'   => array(
                    'title' => $row->title,
                    'artist' => $row->author,
                    'year' => $row->year,
                    'song_type'   => 1,
                    'song_url'  => $row->url
                ),
            );
		var_dump($post_arr);
		$id = wp_insert_post($post_arr);
		$songs[] = $id;
	}

	update_term_meta($station_id, 'songs', $songs);

	$sql = "DELETE FROM tbl_songs WHERE station='$station_name'";//TRUNCATE tbl_songs";
	$wpdb->get_results($sql);
	die;
}

add_action('wp_ajax_nopriv_import_songs', 'import_songs');


require get_template_directory() . '/classes/RadioHelper.php';
require get_template_directory() . '/classes/PMProHelper.php';
require get_template_directory() . '/classes/Ajax.php';
require get_template_directory() . '/classes/Endpoints.php';
require get_template_directory() . '/inc/post_types.php';
require get_template_directory() . '/inc/actions.php';

class_exists('PMProHelper') && PMProHelper::init();

$ajax_class = new Ajax();

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'position'      => 115,
        'icon_url'      => 'dashicons-admin-tools'
    ));
}

///**
// * SVG Icons class.
// */
//require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';
//
///**
// * Custom Comment Walker template.
// */
//require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';
//
///**
// * Enhance the theme by hooking into WordPress.
// */
//require get_template_directory() . '/inc/template-functions.php';
//
///**
// * SVG Icons related functions.
// */
//require get_template_directory() . '/inc/icon-functions.php';
//
///**
// * Custom template tags for the theme.
// */
//require get_template_directory() . '/inc/template-tags.php';
//
///**
// * Customizer additions.
// */
//require get_template_directory() . '/inc/customizer.php';

<?php
/**
 * Filters and hooks
 */

add_action('edited_terms', 'add_timestamp_to_term', 10, 2);
add_action('created_term', 'add_timestamp_to_term', 10, 2);
add_action('after_setup_theme', 'remove_admin_bar');
add_action('save_post_song', 'setLoadTracksTime', 10, 2);
add_action('edit_post_song', 'setLoadTracksTime', 10, 2);

// Actions
function add_timestamp_to_term($term_id, $taxonomy)
{
    update_term_meta($term_id, 'update_timestamp', time());
}

function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function setLoadTracksTime($post_ID, $post, $update)
{
    $posts_id = get_option('load_tracks_time', []);

    $posts_id = is_array($posts_id) ? $posts_id : json_decode($posts_id);

    if (!in_array($post_ID, $posts_id)) {
        array_push($posts_id, $post_ID);

        update_option('load_tracks_time', json_encode($posts_id));
    }
}
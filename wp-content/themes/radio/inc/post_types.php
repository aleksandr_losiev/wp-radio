<?php
/**
 * Radio custom post types
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

add_action('init', 'register_songs_type');
function register_songs_type()
{
    register_taxonomy('radio_type', array('song'), array(
        'label'                 => __('Category', 'radio'),
        'labels'                => array(
            'name'              => __('Category', 'Radio'),
            'singular_name'     => __('Single Category', 'radio'),
            'search_items'      => __('Search Category', 'radio'),
            'all_items'         => __('All Category', 'radio'),
            'parent_item'       => __('Parent Category', 'radio'),
            'parent_item_colon' => __('Parent Category:', 'radio'),
            'edit_item'         => __('EditCategory', 'radio'),
            'update_item'       => __('Update Category', 'radio'),
            'add_new_item'      => __('Add new Category', 'radio'),
            'new_item_name'     => __('New Category', 'radio'),
            'menu_name'         => __('Category', 'radio'),
        ),
        'description'           => __('Category for radio', 'radio'),
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'show_admin_column'     => true,
    ) );

    register_taxonomy('radio', array('song'), array(
        'label'                 => __('Station', 'radio'),
        'labels'                => array(
            'name'              => __('Station', 'Radio'),
            'singular_name'     => __('Single Station', 'radio'),
            'search_items'      => __('Search Station', 'radio'),
            'all_items'         => __('All Stations', 'radio'),
            'parent_item'       => __('Parent Station', 'radio'),
            'parent_item_colon' => __('Parent Station:', 'radio'),
            'edit_item'         => __('Edit Station', 'radio'),
            'update_item'       => __('Update Station', 'radio'),
            'add_new_item'      => __('Add new Station', 'radio'),
            'new_item_name'     => __('New Station', 'radio'),
            'menu_name'         => __('Stations', 'radio'),
        ),
        'description'           => __('All live Station', 'radio'),
        'rewrite'               =>  array('slug' => 'radio', 'with_front' => false),
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'show_admin_column'     => true,
    ) );

    register_taxonomy('playlist', array('song'), array(
        'label'                 => __('Playlist', 'radio'),
        'labels'                => array(
            'name'              => __('Playlists', 'Radio'),
            'singular_name'     => __('Playlist', 'radio'),
            'search_items'      => __('Search Playlist', 'radio'),
            'all_items'         => __('All Playlists', 'radio'),
            'parent_item'       => __('Parent Playlist', 'radio'),
            'parent_item_colon' => __('Parent Playlist:', 'radio'),
            'edit_item'         => __('Edit Playlist', 'radio'),
            'update_item'       => __('Update Playlist', 'radio'),
            'add_new_item'      => __('Add new Playlist', 'radio'),
            'new_item_name'     => __('New Playlist', 'radio'),
            'menu_name'         => __('Playlists', 'radio'),
        ),
        'description'           => __('Custom Users Playlists', 'radio'),
        'public'                => true,
        'show_in_nav_menus'     => false,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'show_admin_column'     => true,
    ) );

    register_taxonomy('favorites', array('song'), array(
        'label'                 => __('Favorites', 'radio'),
        'labels'                => array(
            'name'              => __('Favorite playlist', 'Radio'),
            'singular_name'     => __('Favorite', 'radio'),
            'search_items'      => __('Search favorite playlist', 'radio'),
            'all_items'         => __('All favorites playlists', 'radio'),
            'parent_item'       => __('Parent Playlist', 'radio'),
            'parent_item_colon' => __('Parent Playlist:', 'radio'),
            'edit_item'         => __('Edit favorite playlist', 'radio'),
            'update_item'       => __('Update favorite playlist', 'radio'),
            'add_new_item'      => __('Add new favorite playlist', 'radio'),
            'new_item_name'     => __('New favorite playlist', 'radio'),
            'menu_name'         => __('Favorite Playlist', 'radio'),
        ),
        'description'           => __('User favorite Playlist', 'radio'),
        'public'                => true,
        'show_in_nav_menus'     => false,
        'show_ui'               => true,
        'show_tagcloud'         => false,
        'hierarchical'          => true,
        'show_admin_column'     => true,
    ) );

    register_post_type('song', array(
        'labels' => array(
            'name'               => __('Songs', 'radio'),
            'singular_name'      => __('Song', 'radio'),
            'add_new'            => __('Add new song', 'radio'),
            'add_new_item'       => __('Add new song', 'radio'),
            'edit_item'          => __('Edit song', 'radio'),
            'new_item'           => __('New song', 'radio'),
            'view_item'          => __('View song', 'radio'),
            'search_items'       => __('Search songs', 'radio'),
            'not_found'          => __('No songs found', 'radio'),
            'not_found_in_trash' => __('No songs found in the trash', 'radio'),
            'parent_item_colon'  => '',
            'menu_name'          => __('Songs', 'radio')
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_position'      => 110,
        'menu_icon'          => 'dashicons-format-audio',
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => 'songs',
        'hierarchical'       => false,
        'supports'           => array('title', 'thumbnail'),
        'taxonomies' => array('radio', 'playlist', 'favorites'),
    ));


}

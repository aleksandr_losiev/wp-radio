<?php
/**
 * Template Name: My Playlist
 * Archive page with user playlists
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();

$playlists = RadioHelper::getUserPlaylists();
?>

<main id="my-playlists" class="playlist-page _clearfix">

    <h2><?php the_title(); ?></h2>
    <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
    <section class="playlists-container _clearfix">
        <?php foreach ($playlists as $playlist): ?>
        <?php
            $link = get_term_link($playlist->term_id);
            ?>
            <article class="playlist" data-playlist-id="<?= $playlist->term_id; ?>">
                <a href="<?= $link; ?>">
                    <div class="playlists-songs">
                        <?php if($playlist->count): ?>
                            <?php foreach(RadioHelper::getTracksByPlaylist($playlist->term_id) as $track): ?>
                                <img src="<?= get_the_post_thumbnail_url($track->ID); ?>">
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </a>
                <div class="data">
                    <h6>
                        <a href="<?= $link; ?>"><?= $playlist->name; ?></a>
                        <button class="more-tooltip tooltip button__clear">
                            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                            <span class="tooltip-content tooltip-content__bottom">
                                <a class="edit-playlist"
                                   data-playlist-id="<?= $playlist->term_id; ?>"
                                   data-playlist-name="<?= $playlist->name; ?>"
                                   data-playlist-description="<?= $playlist->description; ?>"
                                ><?= __('Edit Playlist', 'radio'); ?></a>
                                <a
                                    class="delete-playlist"
                                    data-playlist-id="<?= $playlist->term_id; ?>"
                                    data-playlist-name="<?= $playlist->name; ?>"
                                ><?= __('Delete'); ?></a>
                            </span>
                        </button>
                    </h6>
                    <p><?= $playlist->count; ?>&nbsp;<?= __('tracks', 'radio'); ?></p>
                </div>
            </article>
        <?php endforeach; ?>
        <article class="playlist">
            <div class="add-new-playlist">
                <button class="open-modal" data-modal-id="playlist-add"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
            </div>
        </article>
    </section>

</main>

<?php get_footer(); ?>
<?php
/**
 * Template Name: Home page
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$is_user_login = is_user_logged_in();

?>
    <main id="home-page-content" class="home-page <?= $is_user_login ? 'auth' : 'not-auth' ?>">
        <?php
        if ( have_posts() ) {

            // Load posts loop.
            while ( have_posts() ) {
                the_post();

                $is_user_login ? get_template_part('template-parts/home/home', 'auth') : get_template_part('template-parts/home/home');

            }

        }
        ?>
    </main>

<?php
get_footer();

<?php
/**
 * Template Name: Dashboard Songs
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$query = [
    'post_type' => 'song',
    'posts_per_page' => '-1',
];

$tracks = new WP_Query($query);

$total_views = [
    'this_month' => [],
    'this_month_unique' => [],
    'total' => [],
    'total_unique' => [],
]

?>
    <main class="dashboard dashboard__songs">
        <?php  get_template_part('templates/dashboard-parts/sidebar'); ?>
        <div class="dashboard__data">
            <h2><?php the_title(); ?></h2>
            <div class="dashboard__container">
                <div class="dashboard__songs__table">
                    <table class="table-dashboard" data-paging="true" data-paging-size="20" data-sorting="true">
                        <thead>
                        <tr>
                            <th data-breakpoints="xs" data-type="number">ID</th>
                            <th data-breakpoints="xs"><?= __('Tittle', 'radio'); ?></th>
                            <th data-breakpoints="xs"><?= __('Artist', 'radio'); ?></th>
                            <th data-type="number"><?= __('Views', 'radio'); ?> <span>(<?= __('This Month', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Unique views', 'radio'); ?> <span>(<?= __('This Month', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Views', 'radio'); ?> <span>(<?= __('Total', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Unique views', 'radio'); ?> <span>(<?= __('Total', 'radio'); ?>)</span></th>
                        </tr>
                        </thead>
                        <tbody>
                                            <?php foreach ($tracks->posts as $track): ?>
<!--                        --><?php //foreach (get_field('songs', "term_10") as $track): ?>
                            <?php
//                    var_dump($track);
                            $current_date = new DateTime();
                            $date_format_month = $current_date->format('m-y');
                            $track->views =  get_post_meta( $track->ID, 'views_by_month', true);

                            $views = [
                                'this_month' => 0,
                                'this_month_unique' => 0,
                                'total' => 0,
                                'total_unique' => 0,
                            ];

                            foreach ($track->views as $month => $data) {
                                $views['total'] += count($data);
                                $views['total_unique'] += count(array_unique($data));

                                if ($date_format_month == $month) {
                                    $views['this_month'] += count($data);
                                    $views['this_month_unique'] += count(array_unique($data));

                                    $total_views['this_month'][] = $data;
                                    $total_views['this_month_unique'][] = $data;
                                }

                                $total_views['total'][] = $data;
                                $total_views['total_unique'][] = $data;
                            }
                            ?>
                            <tr data-expanded="true">
                                <td><?= $track->ID ?></td>
                                <td><?= $track->post_title; ?></a></td>
                                <td><?= get_field('artist', $track->ID); ?></td>
                                <td><?= $views['this_month']; ?></td>
                                <td><?= $views['this_month_unique']; ?></td>
                                <td><?= $views['total']; ?></td>
                                <td><?= $views['total_unique']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="dashboard__songs__map">

                </div>
            </div>
        </div>
        <div class="dashboard-sidebar-statistic">
            <div class="dashboard-sidebar-statistic__container">
                <div class="dashboard-sidebar-statistic__data">
                    <div class="element__counter-block">
                        <h5><?= __('Auditions', 'radio'); ?><span><?= __('Total', 'radio') ?></span></h5>
                        <p><?= count(getListIpByViews($total_views['total'])); ?></p>
                    </div>
                    <div class="element__counter-block">
                        <h5><?= __('Auditions', 'radio'); ?><span><?= __('Total unique', 'radio') ?></span></h5>
                        <p><?= count(getListIpByViews($total_views['total_unique'], true)); ?></p>
                    </div>
                    <div class="element__counter-block">
                        <h5><?= __('Auditions', 'radio'); ?><span><?= __('This month', 'radio') ?></span></h5>
                        <p><?= count(getListIpByViews($total_views['this_month'])); ?></p>
                    </div>
                    <div class="element__counter-block">
                        <h5><?= __('Auditions', 'radio'); ?><span><?= __('This month unique', 'radio') ?></span></h5>
                        <p><?= count(getListIpByViews($total_views['this_month_unique'], true)); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php

function getListIpByViews($array, $is_unique = false) {
    $result = [];

    foreach ($array as $items) {
        foreach ($items as $view_data) {
            $result[] = $view_data['ip'];
        }
    }

    return $is_unique ? array_unique($result) : $result;
}

get_footer();

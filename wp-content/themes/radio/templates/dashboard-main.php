<?php
/**
 * Template Name: Dashboard Main
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

?>
    <main class="dashboard dashboard__main">
        <?php  get_template_part('templates/dashboard-parts/sidebar'); ?>
        <div class="dashboard__data">
            <h2><?php the_title(); ?></h2>
            <div class="dashboard__container">
                Test text
            </div>
        </div>
    </main>
<?php
get_footer();
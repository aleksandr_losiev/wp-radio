<?php
/**
 * Template Name: User Account
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$avatar = get_field('user_avatar', 'user_' . get_current_user_id()) ?: get_field('default_user_avatar', 'option');
$user = wp_get_current_user();
$userGender = get_field('user_gender', 'user_' . $user->ID) ? get_field('user_gender', 'user_' . $user->ID)['value'] : false;
$plan = pmpro_getMembershipLevelsForUser()[0];
$gender = get_field_object('field_5d25f27d748e2');
?>
    <main id="user-account">
        <div class="main-container">
            <div class="sidebar">
                <div class="logo">
                    <img src="<?= $avatar; ?>">
                </div>
                <ul class="menu">
                    <li class="active">
                        <a href="#account-overview" class="lazy-scroll">
                            <i class="fa fa-user-o" aria-hidden="true"></i><?= __('Account overview', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="#my-plan" class="lazy-scroll">
                            <i class="fa fa-credit-card" aria-hidden="true"></i><?= __('My Plan', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="#edit-profile" class="lazy-scroll">
                            <i class="fa fa-pencil" aria-hidden="true"></i><?= __('Edit Profile', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="#change-password" class="lazy-scroll">
                            <i class="fa fa-lock" aria-hidden="true"></i><?= __('Change Password', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="#notifications" class="lazy-scroll">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i><?= __('Notifications', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="#terms" class="lazy-scroll">
                            <i class="fa fa-file-text-o"
                               aria-hidden="true"></i><?= __('Terms And Services', 'radio'); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= wp_logout_url(home_url()); ?>">
                            <i class="fa fa-sign-out" aria-hidden="true"></i><?= __('Log Out', 'radio'); ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="content">
                <section id="user-banner">
                    <?php if (get_field('user_banner_default', 'option')): ?>
                        <img src="<?= get_field('user_banner_default', 'option'); ?>">
                    <?php endif; ?>
                </section>
                <section id="account-overview">
                    <h4><?= __('Account overview', 'radio'); ?></h4>
                    <div class="section-data">
                        <div>
                            <span>Username</span>
                            <span><?= $user->user_nicename ?></span>
                        </div>
                        <div>
                            <span>Email</span>
                            <span><?= $user->user_email; ?></span>
                        </div>
                        <div>
                            <span>Gender</span>
                            <span class="capitalize"><?= $userGender ?: __('Gender not selected', 'radio'); ?></span>
                        </div>
                        <div>
                            <span>Date of Birth</span>
                            <span><?= RadioHelper::getUserBirthdayDate($user->ID); ?></span>
                        </div>
                    </div>
                    <button class="_button _button__green _button__normal _button__310"
                            data-scroll-to="edit-profile"><?= __('Edit', 'radio'); ?></button>
                </section>
                <section id="my-plan">
                    <h4><?= __('Your Plan', 'radio'); ?></h4>
                    <div class="section-data">
                        <div class="plan-title">
                            <h6><?= $plan->name; ?></h6>
                            <div class="price"><?= pmpro_formatPrice($plan->initial_payment); ?></div>
                        </div>
                        <div class="plan-description">
                            <?= $plan->description; ?>
                        </div>
                    </div>
<!--                    <button class="_button _button__green _button__normal _button__310 open-modal" data-modal-id="popup-change-plan">--><?//= __('Change Plan', 'radio'); ?><!--</button>-->
                    <button class="_button _button__green _button__normal _button__310 open-modal" data-modal-id="sign-up-2"><?= __('Change Plan', 'radio'); ?></button>
                </section>
                <section id="edit-profile">
                    <h4><?= __('Edit Profile', 'radio'); ?></h4>
                    <form id="edit-profile">
                        <div class="form-field">
                            <label for="edit-username"><?= __('Username', 'radio'); ?></label>
                            <input type="text" id="edit-username" placeholder="your.name"
                                   value="<?= $user->user_nicename ?>">
                        </div>
                        <div class="form-field">
                            <label for="edit-email"><?= __('Email', 'radio'); ?></label>
                            <input type="email" id="edit-email" placeholder="your.name@gmail.com"
                                   value="<?= $user->user_email; ?>" data-default-value="<?= $user->user_email; ?>">
                        </div>
                        <div class="form-field">
                            <label for="edit-gender"><?= __('Gender', 'radio'); ?></label>
                            <select id="edit-gender">
                                <option value="" disabled><?= __('Choose gender'); ?></option>
                                <?php foreach ($gender['choices'] as $key => $value): ?>
                                    <option value="<?= $key; ?>" <?= $userGender === $key ? 'selected' : ''; ?>><?= $value; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-field">
                            <label for="edit-gender"><?= __('Date of birth', 'radio'); ?></label>
                            <div class="select-row">
                                <div class="col-1-3">
                                    <select id="edit-date">
                                        <?php foreach (get_field_object('field_5d25f30a748e3')['choices'] as $key => $value): ?>
                                            <option value="<?= $key; ?>" <?= get_field('user_birthday_day', 'user_' . $user->ID) == $key ? 'selected' : ''; ?>><?= $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-1-3">
                                    <select id="edit-month">
                                        <?php foreach (get_field_object('field_5d25f4d07d62e')['choices'] as $key => $value): ?>
                                            <option value="<?= $key; ?>" <?= get_field('user_birthday_month', 'user_' . $user->ID) == $key ? 'selected' : ''; ?>><?= $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-1-3">
                                    <select id="edit-year">
                                        <?php foreach (get_field_object('field_5d25f5267d62f')['choices'] as $key => $value): ?>
                                            <option value="<?= $key; ?>" <?= get_field('user_birthday_year', 'user_' . $user->ID) == $key ? 'selected' : ''; ?>><?= $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <button class="_button _button__green _button__normal _button__310 _button_ajax"><?= __('Save', 'radio'); ?></button>
                </section>
                <section id="change-password">
                    <h4><?= __('Change Password', 'radio'); ?></h4>
                    <form id="reset-pass">
                        <div class="form-field" data-pass-status="open">
                            <label for="new-pass"><?= __('New Password', 'radio'); ?></label>
                            <input type="password" id="new-pass" class="pass-open">
                            <button class="eye" data-status="open"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button class="eye" data-status="close"><i class="fa fa-eye-slash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="form-field" data-pass-status="open">
                            <label for="new-pass-2"><?= __('New Password Again', 'radio'); ?></label>
                            <input type="password" id="new-pass-2" class="pass-open">
                            <button class="eye" data-status="open"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button class="eye" data-status="close"><i class="fa fa-eye-slash" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                    <button class="_button _button__green _button__normal _button__310 _button_ajax"><?= __('Save', 'radio'); ?></button>
                </section>
                <section id="update-images">
                    <h4><?= __('Update user images', 'radio'); ?></h4>
                    <?php if (false): ?>
                        <form id="edit-user-images">
                            <div class="form-field">
                                <label for="edit-avatar" class="file-label">
                                    <span><?= __('Update user avatar', 'radio'); ?></span>
                                    <i class="fa fa-check-circle hidden" aria-hidden="true"></i>
                                </label>
                                <input type="file" id="edit-avatar" class="hidden">
                            </div>
    <!--                        <div class="form-field">-->
    <!--                            <label for="edit-banner" class="file-label">-->
    <!--                                <span>Update user banner</span>-->
    <!--                                <i class="fa fa-check-circle hidden" aria-hidden="true"></i>-->
    <!--                            </label>-->
    <!--                            <input type="file" id="edit-banner" class="hidden">-->
    <!--                        </div>-->
                        </form>
                    <?php endif; ?>
                    <div class="form-field">
                        <button class="_button _button__green _button__normal _button__370 open-modal" data-modal-id="popup-avatars"><?= __('Change avatar', 'radio'); ?></button>
                    </div>
                </section>
                <section id="notifications">
                    <h4><?= __('Notifications', 'radio'); ?></h4>
                    <div class="section-data"></div>
                </section>
                <section id="terms">
                    <h4><?= __('Terms And Services', 'radio'); ?></h4>
                    <div class="section-data">
                        <?php
                            $terms_data =  get_page_by_path( 'terms-of-use' );
                            echo $terms_data->post_content;
                        ?>
                    </div>
                </section>
            </div>
        </div>
    </main>

<?php
get_footer();

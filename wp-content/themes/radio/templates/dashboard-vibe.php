<?php
/**
 * Template Name: Dashboard Vibe
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$vibes = RadioHelper::getUserStations(-1);

?>
    <main class="dashboard dashboard__vibe">
        <?php  get_template_part('templates/dashboard-parts/sidebar'); ?>
        <div class="dashboard__data">
            <h2><?php the_title(); ?></h2>
            <div class="dashboard__container">
                <table class="table-dashboard" data-paging="true" data-paging-size="20" data-sorting="true">
                    <thead>
                        <tr>
                            <th data-breakpoints="xs" data-type="number">ID</th>
                            <th data-breakpoints="xs">Tittle</th>
                            <th data-breakpoints="xs">Author</th>
                            <th data-type="number"><?= __('Views', 'radio'); ?> <span>(<?= __('This Month', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Unique views', 'radio'); ?> <span>(<?= __('This Month', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Views', 'radio'); ?> <span>(<?= __('Total', 'radio'); ?>)</span></th>
                            <th data-type="number"><?= __('Unique views', 'radio'); ?> <span>(<?= __('Total', 'radio'); ?>)</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($vibes as $vibe): ?>
                        <?php
                            $current_date = new DateTime();
                            $date_format_month = $current_date->format('m-y');

                            $views = [
                                'this_month' => 0,
                                'this_month_unique' => 0,
                                'total' => 0,
                                'total_unique' => 0,
                            ];

                            foreach ($vibe->views as $month => $data) {
                                $views['total'] += count($data);
                                $views['total_unique'] += count(array_unique($data));

                                if ($date_format_month == $month) {
                                    $views['this_month'] += count($data);
                                    $views['this_month_unique'] += count(array_unique($data));
                                }
                            }
                        ?>
                            <tr data-expanded="true">
                                <td><?= $vibe->term_id ?></td>
                                <td><a href="<?= get_term_link( $vibe->term_id, $vibe->taxonomy); ?>"><?= $vibe->name; ?></a></td>
                                <td><?= get_the_author_meta('user_nicename', $vibe->user_id); ?></td>
                                <td><?= $views['this_month']; ?></td>
                                <td><?= $views['this_month_unique']; ?></td>
                                <td><?= $views['total']; ?></td>
                                <td><?= $views['total_unique']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
<?php
get_footer();

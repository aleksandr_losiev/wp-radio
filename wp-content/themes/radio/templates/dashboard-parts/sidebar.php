<?php
/**
 * Sidebar for Dashboard
 */
?>

<article class="dashboard-sidebar">
    <?php wp_nav_menu( [
        'container' => 'nav',
        'container_class' => 'dashboard__menu',
        'theme_location'  => 'dashboard'
    ] ); ?>
</article>

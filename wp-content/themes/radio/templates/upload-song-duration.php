<?php
/**
 * Template Name: Upload song duration
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$query = [
    'post_type' => 'song',
    'posts_per_page' => '-1',
    'meta_query' => [
        [
            'key' => 'track_time',
            'compare' => 'NOT EXISTS'
        ],
    ]
];

$tracks = new WP_Query($query);
$urlData[1] = isset($_GET['page']) ? $_GET['page'] : 1;
$postPerPage = 300;
$urlData = array_slice(explode('/', $_SERVER['REQUEST_URI']), 1, 2);

if (!$urlData[1]) {
    $urlData[1] = 1;
}

?>
    <main id="songs-duration-update">
        <h2><?= __('Need tracks to get duration:', 'radio'); ?> <?= count($tracks->posts); ?></h2>
        <?php foreach (array_slice($tracks->posts, ($urlData[1] -1) * $postPerPage, $postPerPage)as $track): ?>
            <?php
            switch (get_field('song_type', $track->ID)) {
                case 1:
                    $songURL = get_field('song_url', $track->ID);
                    break;
                case 2:
                    $songURL = get_field('song_file', $track->ID)['url'];
            }
            ?>
<!--            --><?php //var_dump($track) ?>
            <div
                data-track-id="<?= $track->ID; ?>"
                data-track-url="<?= $songURL ?>"
                class="track-data"><span>#<?= $track->ID;?></span> - <?= $track->post_title; ?></div>
        <?php endforeach; ?>
        <button class="_button__more _button_ajax"><?= __('Update', 'radio'); ?></button>
        <div class="pagination">
            <?php for($i = 0; $i < ceil(count($tracks->posts) / $postPerPage); $i++): ?>
                <a href="/<?= $urlData[0] ?>/<?= $i +1; ?>" class="<?= $urlData[1] == $i ? 'active' : '' ?>"><?= $i +1; ?></a>
            <?php endfor; ?>
        </div>
    </main>

<?php
get_footer();

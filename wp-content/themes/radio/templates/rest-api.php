<?php
/**
 * Template Name: REST API
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */

get_header();

$apiData = [
    'url' => $_SERVER['SERVER_NAME'],
    'namespace' => '/wp-json/radio-api/v2',
    'route' => [
        [
            'name' => __('Login', 'radio'),
            'description' => __('Authorizes a user to his account', 'radio'),
            'return_format' => 'json',
            'url' => '/login',
            'type' => 'POST',
            'class' => 'post',
            'params' => [
                [
                    'key' => 'login',
                    'required' => true
                ],
                [
                    'key' => 'pass',
                    'required' => true
                ],
                [
                    'key' => 'remember',
                    'required' => false
                ]
            ]
        ],
        [
            'name' => __('Register', 'radio'),
            'description' => __('Creates user account', 'radio'),
            'return_format' => 'json',
            'url' => '/register',
            'type' => 'POST',
            'class' => 'post',
            'params' => [
                [
                    'key' => 'username',
                    'required' => true
                ],
                [
                    'key' => 'password',
                    'required' => true
                ],
                [
                    'key' => 'email',
                    'required' => true
                ]
            ]
        ],
        [
            'name' => __('Get Stations', 'radio'),
            'description' => __('Get all station data and description', 'radio'),
            'return_format' => 'json',
            'url' => '/stations',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __('Get sorted stations', 'radio'),
            'description' => __('Get sorted stations data and description', 'radio'),
            'return_format' => 'json',
            'url' => '/stations/sort',
            'type' => 'GET',
            'class' => 'get',
            'params' => [
                [
                    'key' => 'count',
                    'required' => false,
                    'description' => __('Default: 10', 'radio'),
                ],
                [
                    'key' => 'sort_by',
                    'required' => false,
                    'description' => __('Default: popular; Param: trending|popular', 'radio')
                ],
            ]
        ],
        [
            'name' => __('Get Station', 'radio'),
            'description' => __('Get station data, description and songs by ID', 'radio'),
            'return_format' => 'json',
            'url' => '/stations/{id}',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __('Get Station songs', 'radio'),
            'description' => __('Get station only songs by station ID', 'radio'),
            'return_format' => 'json',
            'url' => '/stations/{id}/songs',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __('View station counter', 'radio'),
            'description' => __('Set view counter to station by ID', 'radio'),
            'return_format' => 'json',
            'url' => '/stations/{id}/views/{user_id}',
            'type' => 'PUT',
            'class' => 'put',
        ],
        [
            'name' => __('Get stations by category ID', 'radio'),
            'description' => __('Get all stations data and description by category ID', 'radio'),
            'return_format' => 'json',
            'url' => '/stations/by-category-id/{id}',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __('Search', 'radio'),
            'description' => __('Search station(s) by key word', 'radio'),
            'return_format' => 'json',
            'url' => '/search/?s={query}',
            'type' => 'GET',
            'class' => 'get',
            'params' => [
                [
                    'key' => 's',
                    'required' => true
                ],
            ]
        ],
        [
            'name' => __('Search Global', 'radio'),
            'description' => __('Search in project by types. Default all types (stations, playlists, artists & songs). You can select one or more types.', 'radio'),
            'return_format' => 'json',
            'url' => '/search/?s={query}&type={type1, type2}',
            'type' => 'GET',
            'class' => 'get',
            'params' => [
                [
                    'key' => 's',
                    'required' => true
                ],
                [
                    'key' => 'type',
                    'required' => false
                ],
            ]
        ],
        [
            'name' => __('User update', 'radio'),
            'description' => __('Update user profile', 'radio'),
            'return_format' => 'json',
            'url' => '/user/{id}/update',
            'type' => 'POST',
            'class' => 'post',
            'params' => [
                [
                    'key' => 'nickname',
                    'required' => false
                ],
                [
                    'key' => 'gender',
                    'required' => false
                ],
                [
                    'key' => 'date',
                    'required' => false
                ],
                [
                    'key' => 'month',
                    'required' => false
                ],
                [
                    'key' => 'year',
                    'required' => false
                ],
            ]
        ],
        [
            'name' => __('Update user avatar', 'radio'),
            'description' => __('Update user avatar', 'radio'),
            'return_format' => 'json',
            'url' => '/user/{id}/update/avatar',
            'type' => 'POST',
            'class' => 'post',
            'params' => [
                [
                    'key' => 'file',
                    'required' => true
                ],
            ]
        ],
        [
            'name' => __('All playlists', 'radio'),
            'description' => __('Get all user playlists', 'radio'),
            'return_format' => 'json',
            'url' => '/playlists/?user_ID={id}',
            'type' => 'GET',
            'class' => 'get',
            'params' => [
                [
                    'key' => 'user_ID',
                    'required' => true
                ],
            ]
        ],
        [
            'name' => __('Get playlist', 'radio'),
            'description' => __('Get user playlist by ID', 'radio'),
            'return_format' => 'json',
            'url' => '/playlist/{playlist_id}',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __('Create playlist', 'radio'),
            'description' => __('Create new playlist for User', 'radio'),
            'return_format' => 'json',
            'url' => '/playlist/create',
            'type' => 'POST',
            'class' => 'post',
            'params' => [
                [
                    'key' => 'user_ID',
                    'required' => true
                ],
                [
                    'key' => 'title',
                    'required' => true
                ],
                [
                    'key' => 'description',
                    'required' => false
                ]
            ]
        ],
        [
            'name' => __('Update playlist', 'radio'),
            'description' => __('Update playlist by playlist ID', 'radio'),
            'return_format' => 'json',
            'url' => '/playlist/{playlist_id}/?title={data}&description={data}',
            'type' => 'PUT',
            'class' => 'put',
            'params' => [
                [
                    'key' => 'title',
                    'required' => false
                ],
                [
                    'key' => 'description',
                    'required' => false
                ]
            ]
        ],
        [
            'name' => __('Delete playlist', 'radio'),
            'description' => __('Delete user playlist by playlist ID', 'radio'),
            'return_format' => 'json',
            'url' => '/playlist/{playlist_id}',
            'type' => 'DELETE',
            'class' => 'delete',
        ],
        [
            'name' => __("Added track", 'radio'),
            'description' => __('Added track to playlist', 'radio'),
            'return_format' => 'json',
            'url' => '/track/{track_id}/playlist/{playlist_id}',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __("Delete track", 'radio'),
            'description' => __('Delete current track from playlist', 'radio'),
            'return_format' => 'json',
            'url' => '/track/{track_id}/playlist/{playlist_id}',
            'type' => 'DELETE',
            'class' => 'delete',
        ],
        [
            'name' => __("Get memberships data", 'radio'),
            'description' => __('Get memberships description levels', 'radio'),
            'return_format' => 'json',
            'url' => '/membership',
            'type' => 'GET',
            'class' => 'get',
        ],
        [
            'name' => __("Update user level membership", 'radio'),
            'description' => __('Update user level membership', 'radio'),
            'return_format' => 'json',
            'url' => '/membership/user/{user_id}/level/{level_id}',
            'type' => 'PUT',
            'class' => 'put',
        ],
        [
            'name' => __('Get all categories for stations', 'radio'),
            'description' => __('Get all categories data and description for stations', 'radio'),
            'return_format' => 'json',
            'url' => '/categories',
            'type' => 'GET',
            'class' => 'get',
        ],
    ]
];

?>
    <main id="rest-api-data">
        <div class="sidebar">
            <ul>
                <?php foreach ($apiData['route'] as $key => $endpoint): ?>
                    <li><button class="button__clear" data-scroll-to="endpoint-index-<?= $key; ?>"><?= $endpoint['name']; ?></button></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="container">
            <h3><?php the_title(); ?></h3>
            <h4><?= __('Namespace:', 'radio')?> <span><?=  $apiData['namespace'] ?></span></h4>
            <div class="data">
                <?php foreach ($apiData['route'] as $key => $endpoint): ?>
                    <div class="endpoint <?= $endpoint['class']; ?>" id="endpoint-index-<?= $key; ?>">
                        <h5><?= $endpoint['name']; ?></h5>
                        <p><?= $endpoint['description']; ?></p>
                        <div class="main-data">
                            <div class="type"><?= $endpoint['type']; ?></div>
                            <div class="url"><?= $endpoint['url']; ?></div>
                        </div>
                        <div class="main-description">
                            <?php if (isset($endpoint['params'])): ?>
                                <div class="params-data data">
                                    <h6><?= __('Params:', 'radio'); ?></h6>
                                    <?php foreach ($endpoint['params'] as $param): ?>
                                        <p>
                                            <?= __('Key:', 'radio'); ?>&nbsp;<span><?= $param['key']; ?></span>
                                            <?php if ($param['required']): ?>
                                                (<?= __('Required', 'radio'); ?>)
                                            <?php endif; ?>
                                            <?php if (isset($param['description'])): ?>
                                                (<?= $param['description']; ?>)
                                            <?php endif; ?>
                                        </p>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                            <div class="example-data data">
                                <h6><?= __('Example url:', 'radio'); ?></h6>
                                <p><?= $apiData['url'] . $apiData['namespace'] . $endpoint['url']; ?></p>
                            </div>
                            <div class="return-data data">
                                <h6><?= __('Return format:', 'radio'); ?></h6>
                                <p><?= $endpoint['return_format']; ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </main>

<?php
get_footer();

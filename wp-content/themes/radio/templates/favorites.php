<?php
/**
 * Template Name: Favorites playlists
 * Archive page with all favorites playlist
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();

$favorite_term_id = RadioHelper::getFavoritePlaylistID();

$songs = get_posts(
    array(
        'posts_per_page' => -1,
        'post_type' => 'song',
        'tax_query' => array(
            array(
                'taxonomy' => 'favorites',
                'field' => 'term_id',
                'terms' => $favorite_term_id,
            )
        )
    )
);

?>

    <main id="single-playlist" class="station-page" data-playlist-id="<?= $favorite_term_id ?>">

        <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
        <section class="station-container _clearfix">
            <div class="station-songs audio-block">
                <?php foreach ($songs as $key => $song):
                    $song_data = get_field('song_file', $song->ID);

                    ?>
                    <figure data-track-id="<?= $song->ID ?>">
                        <div class="description">
                            <div class="index"><?= $key +1; ?></div>
                            <img class="thumbnail" src="<?= get_the_post_thumbnail_url($song->ID); ?>">
                            <div class="title-container">
                                <h5><?= get_field('artist', $song->ID); ?></h5>
                                <h6><span><?= get_field('year', $song->ID); ?></span><?= get_field('title', $song->ID); ?></h6>
                            </div>
                            <div class="song-data">
                                <audio
                                    src="<?= $song_data['url']; ?>"
                                    data-index="<?= $key; ?>"
                                    data-title="<?= $song->post_title; ?>"
                                    data-artist="<?= get_field('artist', $song->ID); ?>"
                                    data-track-name="<?= get_field('title', $song->ID); ?>"
                                    data-track-id="<?= $song->ID ?>"
                                    data-track-thumbnail="<?= get_the_post_thumbnail_url($song->ID); ?>"
                                ></audio>
                                <div class="audio-time"><?= RadioHelper::getMinutesBySeconds(get_post_meta($song->ID, 'track_time', true)) ?: '00:00'; ?></div>
                            </div>
                        </div>
                        <div class="song-actions">
                            <div class="play-stop">
                                <button class="play-track button__clear" data-index="<?= $key; ?>"><i class="fa fa-play" aria-hidden="true"></i></button>
                            </div>
                            <button class="add-to-playlist button__clear tooltip"
                                    data-song-id="<?= $song->ID; ?>"
                                    data-taxonomy="playlist"
                            >
                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                <?php get_template_part('template-parts/components/tooltips/add-to-playlist'); ?>
                            </button>
                            <button class="delete-track button__clear" data-song-id="<?= $song->ID; ?>" data-type="favorites">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </button>
                        </div>
                    </figure>

                <?php endforeach; ?>

            </div>
        </section>
        <?php get_template_part('template-parts/components/audio-player'); ?>

    </main><!-- .site-main -->

<?php
get_footer();

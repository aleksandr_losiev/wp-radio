<?php
/**
 * Template Name: Playlist All users
 * Archive page with all users playlists
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();

$playlists = get_terms( [
    'taxonomy' => 'playlist',
    'hide_empty' => true,
] );
?>

    <main id="all-playlists" class="playlist-page _clearfix">

        <h2><?php the_title(); ?></h2>
        <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
        <section class="playlists-container _clearfix">
            <?php foreach ($playlists as $playlist): ?>
                <?php if ($playlist->count && $playlist->parent): ?>
                        <?php $link = get_term_link($playlist->term_id); ?>
                        <article class="playlist" data-playlist-id="<?= $playlist->term_id; ?>">
                            <a href="<?= $link; ?>">
                                <div class="playlists-songs">
                                    <?php if($playlist->count): ?>
                                        <?php foreach(RadioHelper::getTracksByPlaylist($playlist->term_id) as $track): ?>
                                            <img src="<?= get_the_post_thumbnail_url($track->ID); ?>">
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </a>
                            <div class="data">
                                <h6>
                                    <a href="<?= $link; ?>"><?= $playlist->name; ?></a>
                                </h6>
                                <p><?= $playlist->count; ?>&nbsp;<?= __('tracks', 'radio'); ?></p>
                            </div>
                        </article>
                <?php endif; ?>
            <?php endforeach; ?>
        </section>

    </main>

<?php get_footer(); ?>
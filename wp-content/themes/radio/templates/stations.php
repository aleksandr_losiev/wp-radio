<?php
/**
 * Template Name: Stations
 *
 * @package WordPress
 * @subpackage Radio
 * @since Radio 1.0
 */
?>

<?php
$stations = get_terms( [
    'taxonomy' => 'radio',
    'hide_empty' => false,
] );

get_header();

include get_template_directory() . '/template-parts/main/main-stations.php';

get_footer();
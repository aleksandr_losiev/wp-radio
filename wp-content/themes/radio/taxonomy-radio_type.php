<?php
/**
 * The template for displaying custom taxonomies and filtered by category
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#custom-taxonomies
 *
 * @package WordPress
 * @subpackage Radio
 * @since 1.0.0
 */

get_header();
?>

<?php
$category = get_queried_object();
$stations = RadioHelper::getFilteredStationByCategory($category->term_id);

$views = (int)get_term_meta($category->term_id, 'views', true);
update_term_meta($category->term_id, 'views', $views + 1);
?>

    <main id="stations-page-content" class="stations-page _clearfix">

        <h2><?php _e('Channels', 'radio'); ?>&nbsp;<i class="fa fa-angle-right"
                                                      aria-hidden="true"></i>&nbsp;<?= $category->name; ?></h2>
        <?php get_template_part('template-parts/sidebar/sidebar', 'radio_type'); ?>
        <section class="stations-container _clearfix">
            <?php
            foreach ($stations as $station) {
                include get_template_directory() . '/template-parts/content/station-single.php';
            }
            ?>
        </section>

    </main>

<?php get_footer();
